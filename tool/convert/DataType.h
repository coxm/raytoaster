namespace tools {


enum class DataType: unsigned char {
  game,
  save,
};


} // namespace tools
