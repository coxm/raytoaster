
#include <sstream>

#include "./DataType.h"
#include "./cli.h"


namespace tools {


void
FormatReader::operator()(
  std::string const& name,
  std::string const& value,
  game::data::FileFormat& format
)
  const
{
  format = game::data::parseFormat(value);
  if (format == game::data::FileFormat::unknown) {
    throw args::ParseError("Invalid format");
  }
}


void
DataTypeReader::operator()(
  std::string const& name,
  std::string const& value,
  DataType& type
)
  const
{
  if (value == "game") {
    type = DataType::game;
  }
  else if (value == "save") {
    type = DataType::save;
  }
  else {
    throw args::ParseError("Invalid data type");
  }
}


void
SeparatedListReader::operator()(
  std::string const& name,
  std::string const& value,
  std::vector<std::string>& items
)
  const
{
  std::string item;
  std::stringstream input(value);
  while(std::getline(input, item, ';')) {
    items.emplace_back(std::move(item));
  }
}


} // namespace tools
