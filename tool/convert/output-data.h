#include <fstream>

#include <cereal/cereal.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>

#include <game/config.h>
#include <game/data/serialise.h>

#include "./ErrorCode.h"


namespace tools {


template <typename DataType>
int outputData(
  args::ValueFlag<std::string>& outputPathOption,
  DataType const& data,
  Format format,
  char const* description
) {
  auto const createArchive = [
    &data,
    format,
    description
  ](std::ostream& os) -> int {
    switch (format) {
      case Format::xml:
        cereal::XMLOutputArchive{os}(cereal::make_nvp(description, data));
        break;
      case Format::json:
        cereal::JSONOutputArchive{os}(cereal::make_nvp(description, data));
        break;
      case Format::binary:
        cereal::BinaryOutputArchive{os}(cereal::make_nvp(description, data));
        break;
      default:
        std::cerr << "Invalid format: " << int(format) << std::endl;
        return ERR_INVALID_FORMAT;
    }
    return 0;
  };

  if (outputPathOption) {
    auto const outputPath = args::get(outputPathOption);
    auto const mode = format == Format::binary
      ? std::ios_base::binary
      : std::ios_base::out;
    std::ofstream ofs(outputPath, mode);
    if (!ofs.good()) {
      std::cerr << "Unable to open file '" << outputPath << '\'' << std::endl;
      return ERR_UNABLE_TO_OPEN_OUTPUT_FILE;
    }
    return createArchive(ofs);
  }

  int const result = createArchive(std::cout);
  if (result == 0) {
    std::cout << std::endl;
  }
  return result;;
}


} // namespace tools
