/**
 * @file convert-data.cpp
 *
 * Small tool which converts game data between formats.
 *
 * Input files must be interpretable via Cereal.
 */
#include <string>
#include <fstream>
#include <iostream>
#include <filesystem>

#include <args.hxx>

#include <cereal/cereal.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>

#include <game/data/open-mode.h>
#include <game/data/Root.h>
#include <game/data/serialisation.h>
#include <game/data/archive-data.h>
#include <game/data/FileFormat.h>

#include "./cli.h"
#include "./DataType.h"
#include "./ErrorCode.h"


using FileFormat = game::data::FileFormat;


FileFormat getFormat(
  args::ValueFlag<FileFormat, tools::FormatReader>& formatOption,
  std::string_view const filepath
) {
  std::cout << "Deducing format for " << filepath << std::endl;
  return formatOption
    ? args::get(formatOption)
    : game::data::suggestFormat(filepath);
}


int main(int argc, char const* const* argv) {
  args::ArgumentParser parser(
    "convert-data",
    "Convert existing game data between formats, or print prototype data"
  );
  args::HelpFlag help(parser, "help", "Show this help menu", {'h', "help"});
  args::CompletionFlag completion(parser, {"complete"});

  args::Flag verboseOption(
    parser, "verbose", "Print verbose output",
    {'v', "verbose"}, args::Options::Single);
  args::Flag printPrototypeOption(
    parser, "prototype", "Print a prototype instead of translating input",
    {'p', "prototype"}, args::Options::Single);
  args::ValueFlag<std::string> inputPathOption(
    parser, "input-path", "The input file (defaults to stdin)",
    {'i', "input"}, args::Options::Single);
  args::ValueFlag<FileFormat, tools::FormatReader> inputFormatOption(
    parser, "json|xml|binary", "The input format",
    {'F', "input-format"}, args::Options::Single);

  args::ValueFlag<std::string> outputPathOption(
    parser, "output-path", "The output file (defaults to stdout)",
    {'o', "output"}, args::Options::Single);
  args::ValueFlag<FileFormat, tools::FormatReader> outputFormatOption(
    parser, "json|xml|binary", "The output format",
    {'f', "output-format"}, args::Options::Single);

  try {
    parser.ParseCLI(argc, argv);
  }
  catch (args::Help const&) {
    std::cout << parser;
    return 0;
  }
  catch (const args::Completion& err) {
    std::cout << err.what();
    return 0;
  }
  catch (args::ParseError const& err) {
    std::cerr << "Unable to parse CLI inputs: " << err.what()
      << std::endl << std::endl << parser;
    return tools::ERR_CLI_PARSE;
  }
  catch (args::ValidationError const& err) {
    std::cerr << "Invalid CLI inputs: " << err.what() << std::endl;
    return tools::ERR_CLI_INVALID_OPTIONS;
  }

  auto const verbose = bool(verboseOption);
  if (verbose) {
    for (int i = 0; i < argc; ++i) {
      std::cout << argv[i] << ' ';
    }
    std::cout << std::endl;
  }

  auto const printPrototype = bool(printPrototypeOption);
  if (printPrototype && (bool(inputPathOption) || bool(inputFormatOption))) {
    std::cerr << "--prototype cannot be used with "
                 "-i or --input" << std::endl;
    return tools::ERR_CLI_INVALID_OPTIONS;
  }
  auto const inputPath = args::get(inputPathOption);
  auto const outputPath = args::get(outputPathOption);
  if (verbose) {
    if (!inputPath.empty()) {
      std::cout << "Loading from " << inputPath << std::endl;
    }
    if (!outputPath.empty()) {
      std::cout << "Writing to " << outputPath << std::endl;
    }
  }

  auto const inputFormat = getFormat(inputFormatOption, inputPath);
  if (!printPrototype &&
      inputFormat == game::data::FileFormat::unknown) {
    std::cerr << "Unable to deduce input format" << std::endl;
    return tools::ERR_INVALID_INPUT_FORMAT;
  }
  auto const outputFormat = getFormat(outputFormatOption, outputPath);
  if (outputFormat == game::data::FileFormat::unknown) {
    std::cerr << "Unable to deduce output format" << std::endl;
    return tools::ERR_INVALID_OUTPUT_FORMAT;
  }

  auto const run = [
    &inputPath, &outputPath, inputFormat, outputFormat, printPrototype
  ] (auto& data) -> int {
    bool success = true;
    if (printPrototype) {
      // Do nothing: the data is already initialised.
    }
    else if (inputPath.empty()) {
      // Import data from stdin.
      success = game::data::archiveData(data, std::cin, inputFormat);
    }
    else {
      // Import data from a file.
      auto const mode =
        game::data::getOpenMode(inputFormat, std::ios_base::in);
      std::ifstream inputFile(inputPath, mode);
      if (!inputFile.good()) {
        std::cerr << "Unable to open input file: " << inputPath << std::endl;
        return tools::ERR_UNABLE_TO_OPEN_INPUT_FILE;
      }
      success = game::data::archiveData(data, inputFile, inputFormat);
    }
    if (!success) {
      std::cerr << "Error reading from input file " << inputPath << std::endl;
      return tools::ERR_UNABLE_TO_READ_INPUT;
    }

    if (outputPath.empty()) {
      success = game::data::archiveData(data, std::cout, outputFormat);
    }
    else {
      auto const mode =
        game::data::getOpenMode(inputFormat, std::ios_base::out);
      std::ofstream outputFile(outputPath, mode);
      if (!outputFile.good()) {
        return tools::ERR_UNABLE_TO_OPEN_OUTPUT_FILE;
      }
      success = game::data::archiveData(data, outputFile, outputFormat);
    }
    if (!success) {
      return tools::ERR_UNABLE_TO_WRITE_OUTPUT;
    }

    return 0;
  };

  {
    game::data::Root root;
    return run(root);
  }
}
