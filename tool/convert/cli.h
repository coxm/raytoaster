#pragma once
#include <vector>
#include <string>
#include <sstream>

#include <args.hxx>

#include <game/data/FileFormat.h>


namespace tools {


enum class DataType: unsigned char;


struct FormatReader {
  void
  operator()(
    std::string const& name,
    std::string const& value,
    game::data::FileFormat& format
  ) const;
};


struct DataTypeReader {
  void
  operator()(
    std::string const& name,
    std::string const& value,
    DataType& items
  ) const;
};


struct SeparatedListReader {
  void
  operator()(
    std::string const& name,
    std::string const& value,
    std::vector<std::string>& items
  ) const;
};


using SeparatedListArg = args::ValueFlag<
  std::vector<std::string>, SeparatedListReader>;


} // namespace tools
