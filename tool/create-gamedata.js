#!/usr/bin/env node
'use strict';
const fs = require('fs');
const path = require('path');
const util = require('util');
const execSync = require('child_process').execSync;
const colours = require('colors/safe');

const {program} = require('commander');


const readFile = util.promisify(fs.readFile);


program
  .version('0.0.0')
  .option('-o, --output <output>', 'output file')
  .option('-a, --atlas <atlas>', 'input sprite atlas file (JSON)')
  .option('-m, --tilemaps <tilemaps...>', 'input tilemap files')
  .option('-v, --verbose', 'verbose mode')
  .option('--effects <effects-program>', 'a program to print Effect indices')
  .parse(process.argv);


const ActionType = JSON.parse(execSync(program.effects));
const EffectId = parseEnum(
  path.resolve(__dirname, '../include/game/audio/EffectId.h'));
const ChannelId = parseEnum(
  path.resolve(__dirname, '../include/game/audio/ChannelId.h'));


(async () => {
  const spriteAtlas = generateSpriteAtlas(program.atlas);
  const gamedata = {
    root: {
      levels: await Promise.all(program.tilemaps.map(filepath => loadTilemap({
        spriteAtlas,
        filepath,
      }))),
      spriteAtlas,
    },
  };

  const serialised = JSON.stringify(gamedata, null, 2);
  if (program.output) {
    fs.writeFileSync(program.output, serialised);
    if (program.verbose) {
      console.log('Game data JSON written to', program.output);
    }
  }
  else {
    console.log(serialised);
  }
})().catch(err => {
  console.error('Encountered an error building game data:');
  console.error(colours.red(err.message));
  console.log(err.stack);
  process.exit(1);
});


function generateSpriteAtlas(inputPath) {
  const {meta, frames} = JSON.parse(fs.readFileSync(inputPath));
  const atlas = {
    frames: [],
    images: [],
    key: {
    },
    map: {
    },
  };

  let frameno = 0;
  for (const imgPath in frames) {
    const frame = frames[imgPath].frame;
    const name = imagePathToName(imgPath);
    frame.name = name;
    frame.index = frameno;
    atlas.map[name] = frame;
    atlas.frames.push(frame);
    atlas.images.push(imgPath);
    ++frameno;
  }

  for (const name of [
    'bullet',
    'pistol',
    'hudBackground',
    'machinegun',
  ]) {
    const index = atlas.frames.findIndex(frame => frame.name === name);
    if (index === -1) {
      throw new Error(`No frame found for name '${name}'`);
    }
    atlas.key[name] = index;
  }

  return atlas;
}


function imagePathToName(imgPath) {
  const match = /(^|\/)([-0-9a-zA-Z]+)\.png+$/.exec(imgPath)
  if (!match) {
    throw new Error(`Unable to deduce name from path ${imgPath}`);
  }
  return match[2].replace(/-[a-z]/g, c => c[1].toUpperCase());
}


const tilesets = new Map();
async function loadTileset(filepath) {
  let tileset = tilesets.get(filepath);
  if (!tileset) {
    tileset = JSON.parse(await readFile(filepath, 'utf8'));
    tileset.lookup = new Map(tileset.tiles.map(tile => [
      tile.id,
      imagePathToName(tile.image),
    ]));
    tilesets.set(filepath, tileset);
  }
  return tileset;
}


async function loadTilemap({spriteAtlas, filepath}) {
  const map = JSON.parse(await readFile(filepath, 'utf8'));
  if (map.tilesets.length !== 1) {
    throw new Error("Currently only one tileset supported");
  }
  const tspath = path.join(path.dirname(filepath), map.tilesets[0].source);
  const tileset = await loadTileset(tspath);

  // Flip tiles (Tiled lists tiles from top to bottom; we need bottom-top).
  const flipY = (cells, width) => {
    const flipped = [];
    for (let i = cells.length - width; i >= 0; i -= width) {
      flipped.push(...cells.slice(i, i + width));
    }
    return flipped;
  };

  // Find a frame index from a GID.
  const gidToFrameIndex = gid => {
    if (gid === 0) { // Nothing in tile; return -1.
      return -1;
    }
    --gid;
    const frameName = tileset.lookup.get(gid);
    if (frameName === undefined) {
      throw new Error(`No frame name for gid ${gid}`);
    }
    const frame = spriteAtlas.map[frameName];
    if (frame === undefined) {
      throw new Error(`No frame for name '${frameName}' (gid ${gid})`);
    }
    return frame.index;
  };

  const tileLayers = map.layers.reduce((dict, layer) => {
    if (layer.type === "tilelayer") {
      dict[layer.name] = layer;
      layer.data = flipY(layer.data, map.width);
      layer.frames = layer.data.map(gidToFrameIndex);
    }
    return dict;
  }, {});

  const cells = [];
  for (let y = 0, index = 0; y < map.height; ++y) {
    for (let x = 0; x < map.width; ++x, ++index) {
      const ceiling = tileLayers.ceiling.frames[index];
      const floor = tileLayers.floor.frames[index];
      const wall = tileLayers.walls.frames[index] + 1;
      cells.push({
        wall,
        floor,
        ceiling,
        x, y,
      });
    }
  }

  const objectLayers = map.layers.reduce((dict, layer) => {
    if (layer.type === 'objectgroup') {
      layer.actors = layer.objects.map(
        obj => reorientTilemapObject(obj, map));
      delete layer.objects; // Use actors[i].from if you need the original.
      dict[layer.name] = layer;
    }
    return dict;
  }, {});
  for (const key of ['actors', 'triggers']) {
    if (!objectLayers.hasOwnProperty(key)) {
      throw new Error(`Layer missing layer '${key}'`);
    }
  }

  const actors = objectLayers.actors;
  const player = actors.actors.find(obj => obj.name === 'Player');
  if (!player) {
    throw new Error("No player found");
  }

  const enemyTypes = {
    goat: {
      divide: {x: 2, y: 1},
      yOffset: 0,
    },
  };
  const enemies = actors.actors
    .filter(actor => actor.from.visible && !!enemyTypes[actor.type])
    .map(actor => {
      if (actor.gid === undefined) {
        throw new Error(`Enemy ${actor.id} has no gid`);
      }
      const frame = gidToFrameIndex(actor.gid);
      const spriteInfo = enemyTypes[actor.type];
      return {
        position: {x: actor.x, y: actor.y},
        frame,
        id: actor.from.id,
        ...spriteInfo,
      };
    });

  const stages = objectLayers.triggers.actors
    .filter(trigger => trigger.type === 'stage')
    .reduce((dict, stage) => {
      stage.fromId = stage.id;
      stage.propertyDict.stageId = stage.propertyDict.stageId | 0;
      dict[stage.propertyDict.stageId] = stage;
      return dict;
    }, {});

  const effects = [];
  const triggerDefaults = {
    trigger: [],
    stage: [],
    kill: [],
    fear: [],
  };
  for (const key in triggerDefaults) {
    Object.freeze(triggerDefaults[key]);
  }
  Object.freeze(triggerDefaults);

  const triggers = objectLayers.triggers.actors
    .map(trigger => {
      const defaultProperties = triggerDefaults[trigger.type];
      if (defaultProperties === undefined) {
        throw new Error(`Invalid trigger type: ${trigger.type}`);
      }
      const properties = new Map(defaultProperties.map(p => [p.name, p]));
      for (const prop of trigger.propertyList) {
        properties.set(prop.name, prop);
      }

      const effectBegin = effects.length;
      for (const property of trigger.propertyList) {
        if (['stageId', 'maxUseCount'].includes(property.name)) {
          continue;
        }

        const index = ActionType[property.name];
        if (index === undefined) {
          throw new Error(
            `Trigger ${trigger.id} has invalid property '${property.name}'`);
        }
        let data;
        switch (property.name) {
          case 'PlaySound': {
            // Formatted as `EffectId[:ChannelId=-1]`.
            const values = property.value.split(':');
            data = {
              effect: EffectId[values[0]],
              channel: -1,
            };
            if (values.length > 1) {
              data.channel = ChannelId[values[1]];
            }
            if (data.effect !== (data.effect | 0) || data.effect < 0) {
              throw new Error(
                `Trigger ${trigger.id} has invalid PlaySound value`);
            }
            if (data.channel !== (data.channel | 0)) {
              throw new Error(
                `Trigger ${trigger.id} has non-integer channel`);
            }
          } break;
          default:
            data = {value: property.value};
        }
        if (data === undefined) {
          throw new Error(`Object ${trigger.id}: empty prop ${property.name}`);
        }
        effects.push({
          action: {
            index,
            data,
            name: `${trigger.name}:${property.name}`,
          },
        });
      }
      const effectEnd = effects.length;

      const stageId = trigger.propertyDict.stageId;
      if (stageId === undefined) {
        throw new Error(`Trigger ${trigger.id} has no stage`);
      }
      if (!stages[stageId]) {
        throw new Error(`Trigger ${trigger.id} points to unknown stage`);
      }

      if (trigger.from.width !== trigger.from.height) {
        throw new Error(`Trigger ${trigger.id} is not circular`);
      }

      return {
        area: {
          x: trigger.x,
          y: trigger.y,
          r: trigger.from.width / (2 * map.tilewidth),
        },

        // Stages are always active, so have stageId -1.
        stageId: trigger.type === 'stage' ? -1 : stageId,
        effectBegin,
        effectEnd,
        maxUseCount: trigger.maxUseCount || 1,

        // These are unnecessary, but useful for development:
        triggerType: trigger.type,
        fromId: trigger.id,
      };
    });

  return {
    triggers,
    effects,
    world: {
      width: map.width,
      height: map.height,
      cells,
    },
    progress: {
      camera: player,
      atmosphere: {
        fog: {
          value: 0.16,
          min: 0.16,
          max: 0.8,
          increment: 0,
        },
        fogLimit: {
          value: 10,
          min: 0,
          max: 100,
          increment: 0,
        },
        fear: {
          value: 1,
          min: 1,
          max: 32,
          increment: 0,
        },
      },
      stage: {
        triggers: [],
        id: -1,
      },
      enemies,
    },
  };
}


function loadAABB(obj, map) {
  const width = obj.from.width / map.tilewidth;
  const height = obj.from.height / map.tileheight;
  const aabb = {
    min: {
      x: obj.x - 0.5 * width,
      y: obj.y - 0.5 * height,
    },
    max: {
      x: obj.x + 0.5 * width,
      y: obj.y + 0.5 * height,
    },
    width,
    height,
    x: obj.x,
    y: obj.x,
  };
}


const DEG_TO_RAD = Math.PI / 180;


function reorientTilemapObject(obj, map) {
  if (!obj.type) {
    throw new Error(`Object (ID ${obj.id}) has no type`);
  }

  const tileSide = map.tilewidth;
  const origX = (obj.x || 0) / tileSide;
  const origY = (obj.y || 0) / tileSide;
  const dx = 0.5 * (obj.width / tileSide);
  let dy = 0.5 * (obj.height / tileSide);
  if (obj.hasOwnProperty('gid')) {
    dy = -dy;
  }
  const radians = -DEG_TO_RAD * (obj.rotation || 0);
  const cos = Math.cos(radians);
  const sin = Math.sin(radians);
  const x = origX + dx * cos + dy * sin;
  const y = map.height - (origY - dx * sin + dy * cos);

  const propertyDict = obj.properties
    ? obj.properties.reduce((dict, prop) => {
        dict[prop.name] = prop.value;
        return dict;
      }, {})
    : {};

  return {
    x, y,
    id: obj.id,
    angle: radians,
    type: obj.type,
    name: obj.name,
    gid: obj.gid,
    from: obj,
    propertyList: obj.properties || [],
    propertyDict,
  };
}


function parseEnum(filename) {
  const content = fs.readFileSync(filename, 'utf8');
  const begin = content.indexOf('{', content.indexOf('enum ')) + 1;
  const close = content.indexOf('}', begin);

  let counter = 0;
  const dict = {};
  const lines = content.substring(begin, close).trim().split('\n');
  for (const line of lines) {
    const parts = line.split(/\s+=\s+/)
      .map(part => part.replace(/[\s,]/gm, ''));
    const name = parts[0];
    if (parts.length === 2) {
      counter = parseInt(parts[1]);
    }
    const value = counter++;
    dict[name] = value;
  }
  return dict;
}
