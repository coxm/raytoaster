#include <iostream>

#include <game/data/Effect.h>


int main() {
#define INDEX(T) \
  "  \"" #T "\": " << game::data::Action{game::data::T{}}.index()

  std::cout
    << "{"

    <<  '\n' << INDEX(SetStage)
    << ",\n" << INDEX(SetFogValue)
    << ",\n" << INDEX(SetFogIncrement)
    << ",\n" << INDEX(SetFogMin)
    << ",\n" << INDEX(SetFogMax)
    << ",\n" << INDEX(SetFogLimitValue)
    << ",\n" << INDEX(SetFogLimitIncrement)
    << ",\n" << INDEX(SetFogLimitMin)
    << ",\n" << INDEX(SetFogLimitMax)
    << ",\n" << INDEX(SetMyopiaValue)
    << ",\n" << INDEX(SetMyopiaIncrement)
    << ",\n" << INDEX(SetMyopiaMin)
    << ",\n" << INDEX(SetMyopiaMax)
    << ",\n" << INDEX(ResetFogValue)
    << ",\n" << INDEX(ResetFogIncrement)
    << ",\n" << INDEX(ResetFogMin)
    << ",\n" << INDEX(ResetFogMax)
    << ",\n" << INDEX(ResetFogAll)
    << ",\n" << INDEX(ResetFogLimitValue)
    << ",\n" << INDEX(ResetFogLimitIncrement)
    << ",\n" << INDEX(ResetFogLimitMin)
    << ",\n" << INDEX(ResetFogLimitMax)
    << ",\n" << INDEX(ResetFogLimitAll)
    << ",\n" << INDEX(ResetMyopiaValue)
    << ",\n" << INDEX(ResetMyopiaIncrement)
    << ",\n" << INDEX(ResetMyopiaMin)
    << ",\n" << INDEX(ResetMyopiaMax)
    << ",\n" << INDEX(ResetMyopiaAll)
    << ",\n" << INDEX(ResetAll)

    << ",\n" << INDEX(PlaySound)

    << "\n}" << std::endl;
  return 0;
}
