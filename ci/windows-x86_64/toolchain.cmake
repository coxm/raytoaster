# Toolchain file for cross-compilation to Windows, based on:
# https://cmake.org/Wiki/File:Toolchain-cross-mingw32-linux.cmake
message(STATUS "Using Windows toolchain")


# The name of the target operating system
set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSTEM_VERSION 10.0)
set(CMAKE_SYSTEM_PROCESSOR x86_64)

# Choose an appropriate compiler prefix

# For classical mingw32.
# see http://www.mingw.org/
#set(TOOLCHAIN_PREFIX "i586-mingw32msvc")

# For 32 or 64 bits mingw-w64.
# See http://mingw-w64.sourceforge.net/
# set(TOOLCHAIN_PREFIX "i686-w64-mingw32" CACHE STRING "Compiler prefix") # 32-bit
set(TOOLCHAIN_PREFIX "x86_64-w64-mingw32" CACHE STRING "Compiler prefix") # 64-bit

# Which compilers to use for C and C++.
find_program(CMAKE_RC_COMPILER NAMES ${TOOLCHAIN_PREFIX}-windres)
# set(CMAKE_RC_COMPILER ${TOOLCHAIN_PREFIX}-windres)
# find_program(CMAKE_C_COMPILER NAMES ${TOOLCHAIN_PREFIX}-gcc)
# set(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}-gcc)
# find_program(CMAKE_CXX_COMPILER NAMES ${TOOLCHAIN_PREFIX}-g++)
# set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}-g++)
set(CMAKE_C_COMPILER /usr/bin/${TOOLCHAIN_PREFIX}-gcc)
set(CMAKE_CXX_COMPILER /usr/bin/${TOOLCHAIN_PREFIX}-g++)
message(STATUS "Using C compiler ${CMAKE_C_COMPILER}")
message(STATUS "Using CXX compiler ${CMAKE_CXX_COMPILER}")

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static-libgcc -static-libstdc++")


# Where is the target environment located?
list(APPEND CMAKE_FIND_ROOT_PATH
  ${CMAKE_SOURCE_DIR}/cmake/windows-x86_64
  ${CMAKE_SOURCE_DIR}/cmake/defaults
  "/usr/${TOOLCHAIN_PREFIX}"
  "/usr/local/${TOOLCHAIN_PREFIX}"
)


# Adjust the default behaviour of the find_xxx() commands:
# search headers and libraries in the target environment, search programs in
# the host environment.
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
