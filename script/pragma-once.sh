files=$(grep -rL "#pragma once" include)
for file in $files; do
  echo "Processing '$file'"
  sed -i '1s/^/#pragma once\n/' "$file"
done
