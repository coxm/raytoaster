;;; convert-xcf.scm
;;;
;;; See ./convert-xcf.sh for explanation.

;; Print a variable number of arguments.
(define (print . args)
  (for-each
    (lambda (arg) (display arg (current-output-port)))
    args))

;; Remove all layers from an image, except those with IDs specified.
(define (remove-layers-except image visible-layer-ids)
  (let* ((get-layers-result (gimp-image-get-layers image))
         (_a0 (print "get-layers-result " get-layers-result "\n"))
         (all-layer-ids (cadr get-layers-result))
         (to-remove
           (filter
             (lambda (layer-id) (not (member layer-id visible-layer-ids)))
             (vector->list all-layer-ids))))
    (print "Removing layers: " to-remove "\n")
    (for-each
      (lambda (layer-id)
        (gimp-image-remove-layer image layer-id))
      to-remove)
    (print "Removed layers: " to-remove "\n")))

;; Get the ID of an image's layer, or error if it doesn't exist.
(define (get-layer-id image layer-name)
  (let ((layer-id (car (gimp-image-get-layer-by-name image layer-name))))
    (when (equal? layer-id -1)
      (error (string-append "Image does not contain layer '" layer-name "'")))
    layer-id))

;; Convert a file to PNG, by merging the specified layers.
(define (convert-to-png input-path output-path layer-names)
  (print "Loading image\n")
  (let*
    ((image (car (gimp-file-load RUN-NONINTERACTIVE input-path input-path)))
     (visible-layer-ids
       (if layer-names
         (map (lambda (layer-name) (get-layer-id image layer-name))
              layer-names)
         (car (gimp-image-get-active-layer image)))))

    (print "Loaded image. Keeping layer IDs: " visible-layer-ids "\n")
    (remove-layers-except image visible-layer-ids)

    (let ((remaining-count (car (gimp-image-get-layers image))))
      (when (equal? remaining-count 0)
        (error "No remaining layers to export from image"))
      (print "Remaining layers: " remaining-count "\n"))

    (gimp-image-merge-visible-layers image CLIP-TO-IMAGE)
    (print "Merged visible layers\n")

    ; Save the PNG file.
    (file-png-save-defaults
      RUN-NONINTERACTIVE
      image
      (car (gimp-image-get-active-layer image))
      output-path
      output-path)
    (print "Saved PNG\n")
    (gimp-image-delete image))
  (print "Done"))
