const fs = require('fs');
const util = require('util');


const readFile = util.promisify(fs.readFile);
module.exports.readFile = readFile;


/**
 * Parse Memcheck suppressions.
 */
const parse = raw => {
  return raw
    .substr(0, raw.lastIndexOf('\n}'))
    .substr(raw.indexOf('{\n') + 2)
    .replace(/^=.*$/mg, '') // Strip out non-suppression output from Memcheck.
    .split(/\s*}[^{]*{\s*/mg) // Ignore junk between braces.
    .reduce((all, supp) => {
      supp = supp.trim();
      if (supp) {
        all.add(supp.split('\n').map(line => line.trim()).join('\n'));
      }
      return all;
    }, new Set());
};
module.exports.parse = parse;


const loadSync = filepath => parse(fs.readFileSync(filepath, 'utf8'));
module.exports.loadSync = loadSync;


const load = async filepath => {
  const content = await readFile(filepath, 'utf8');
  return parse(content);
};
module.exports.load = load;


/**
 * Serialise a set of Memcheck suppressions.
 */
const serialise = (set, format = 'valgrind', indent = 2) => {
  const array = Array.from(set);
  const indentation = indent === '\t' ? '\t' : ' '.repeat(indent);
  switch (format) {
    case 'json':
      return JSON.stringify({
        array,
        count: array.length,
      }, null, indentation);

    case 'valgrind': {
      return array
        .map(supp => supp
          .split('\n')
          .map(line => `${indentation}${line.trim()}`)
          .join('\n')
        )
        .map(supp => `{\n${supp}\n}`)
        .join('\n');
    }

    default:
      throw new Error(`Invalid format: '${format}'`);
  }
};
module.exports.serialise = serialise;
