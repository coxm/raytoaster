#!/usr/bin/env node
const supputil = require('./suppression-utils');


const readStdin = () => new Promise((resolve, reject) => {
  const stdin = process.openStdin();
  const data = [];
  stdin
    .on('data', chunk => {
      data.push(chunk);
    })
    .on('error', reject)
    .on('end', () => {
      return resolve(data.join(''));
    });
});


const input =
  (process.stdin.isTTY
    ? supputil.readFile(process.argv[1], 'utf8')
    : readStdin())
  .then(text => {
    require('fs').writeFileSync('raw.txt', text);
    return text;
  });

input
  .then(supputil.parse)
  .then(suppressions => supputil.serialise(
    suppressions, process.argv.includes('--json') ? 'json' : 'valgrind', 2))
  .then(text => console.log(text))
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
