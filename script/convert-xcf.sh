#!/usr/bin/env bash
# convert-xcf.sh
#
# Convert an XCF file to PNG, using GIMP. This script exists as a stand-in for
# xcftools when that package is not available or installing it is undesirable.
#
# This script invokes GIMP in batch mode, passing a Scheme script constructed
# from ./convert-xcf.scm and this script's own arguments. See ./convert-xcf.scm
# for further details.
set -o errexit
set -o nounset
set -o pipefail
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

usage() {
  echo "$(basename "${BASH_SOURCE[0]}"): Convert XCF to PNG using GIMP."
  echo ""
  echo "Usage:"
  echo ""
  echo "    ${BASH_SOURCE[0]} [-o] <output> <input> <layer...>"
  echo ""
  echo "The -o option, if provided, is ignored. This allows compatibility with"
  echo "xcf2png."
}

main() {
  echo "Args: $@"
  local output="${1:-}"
  if [[ "$output" == "-o" ]]; then
    echo "Silently ignoring -o option for xcftools compatibility"
    shift
    output="${1:-}"
  fi

  if [[ -z "$output" ]]; then
    echo "No output file specified"
    exit 1
  fi
  shift

  local input="${1:-}"
  if [[ -z "$input" ]]; then
    echo "No input file specified"
    exit 1
  fi
  shift

  if (( $# == 0 )); then
    echo "Note: no layers specified; using active layer"
  fi

  local layers="'("
  while (( $# != 0 )); do
    layers="$layers \"$1\""
    shift
  done
  layers="$layers)"

  local script_dir
  script_dir="$( dirname "$( readlink -f "${BASH_SOURCE[0]}" )" )"

  local preface
  local invocation
  preface="$(< "${script_dir}/convert-xcf.scm")"
  invocation="(convert-to-png \"$input\" \"$output\" $layers)"
  gimp \
    --no-interface \
    --batch "$preface $invocation" \
    --batch '(gimp-quit 0)'
}

main "$@"
