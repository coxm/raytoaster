#!/bin/bash
# This script runs the game, assuming the standard installation 
set -u
set -e


root_dir="$( dirname "$( readlink -f "${BASH_SOURCE[0]}" )" )"
main="$root_dir/bin/main"
assets_dir="$root_dir/share/assets"


assets_dir="$assets_dir" "$main"
