# Mechanics
Most of the challenges a player endures in this game are based on affecting
their vision.


-   Myopia: as the player becomes more scared, the viewport becomes more
    pixelated.
-   Red tint: as the player's health decreases, their vision becomes
    progressively more red-tinted.
-   Possibly we'll alter the FoV, though this might require some fine-tuning of
    the camera.
