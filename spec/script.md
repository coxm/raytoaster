# Script
This is the *script*: story-related elements only.

## Implemented

## Ideas

### First experience of myopia
Player is walking through a space and starts to hear the fox cry, triggering
slight myopia. This doesn't last long though: just enough to make it clear it's
an intentional mechanic.


### Dark hallway
#### Simpler version
The player walks down a corridor. As they approach a point, the fog increases
drastically, but then recedes to normal after reaching the end of the corridor.


### Phantom doors
Player passes through a corridor with closed, locked doors. After passing each,
the door is then opened wide while the player's back is turned. Nothing need be
in any of the rooms.


### Timing puzzle with two monsters
The player has to pass through a narrow space/maze without being killed to
continue, while two monsters move around in a set rhythm.


### Myopic maze
Player passes through a maze, at some fixed points becoming more myopic.


### RUN
Near the end of the game, before the final action sequence, the player's vision
becomes more myopic and red-tinted. The camera drops slightly. A clunking sound
effect is played, and the gun disappears.

Soon they recover their vision, but only partially. The gun is gone, replaced
by only their hands, and music plays. The previous objective ("SURVIVE") has
been replaced by "RUN", this time written in red (possibly in the spooky,
cursive font).
