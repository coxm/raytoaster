# Myopia
Simple raycaster game, built with SDL2.


## Building
### Install dependencies
Before building you will need:

-   SDL2, SDL2_ttf and SDL2_mixer installed;
-   a recent NodeJS version, for running build scripts.

If the SDL2 libraries are not locatable by CMake, you may need additional
[options](https://cmake.org/cmake/help/latest/command/find_package.html) when
invoking CMake.

Other C/C++ dependencies will be installed dynamically by CMake.

### Getting started
Install Git submodules and npm packages.

    cd "$repo_dir"
    git submodule init
    git submodule update
    # Install required Node modules.
    yarn # or `npm install`

Then proceed as below.

### CMake
For example:

    build_dir=build/local
    mkdir -p "$build_dir"
    cmake -DCMAKE_BUILD_TYPE=Release -S . -B "$build_dir"
    make --directory="$build_dir"
