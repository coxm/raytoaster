#include <SDL2/SDL_timer.h>

#include "game/data/Root.h"
#include "game/logging/log.h"
#include "game/util/Settings.h"
#include "game/render/LevelRenderer.h"
#include "game/render/OverlayMenuRenderer.h"
#include "game/render/ttf.h"
#include "game/render/colour.h"

#include "game/scenes/init.h"
#include "game/scenes/Level.h"
#include "game/scenes/MenuOption.h"
#include "game/scenes/OverlayMenu.h"


namespace game::scenes {


std::shared_ptr<TTF_Font>
loadFont(game::util::FontConfig const& config) {
  std::shared_ptr<TTF_Font> font{
    TTF_OpenFont(config.filepath.c_str(), config.pointsize),
    game::render::TTFFontDeleter()
  };
  if (!font) {
    LOG_ERROR("Failed to load '" << config.filepath << "' at ptsize "
              << config.pointsize << ": " << TTF_GetError());
    throw std::runtime_error(config.filepath);
  }
  return font;
}


void
init(
  game::util::Settings* settings,
  game::data::Session* session,
  game::data::Root const* root,
  std::shared_ptr<game::render::Renderer> renderer,
  std::shared_ptr<game::audio::Effects> sfx,
  Director* director
) {
  constexpr int mainMenuId = 0;
  constexpr int levelId = 1;
  constexpr int ripId = 2;
  // constexpr int creditsId = 3;

  auto menuFont = loadFont(settings->menuFont());
  auto hudFont = loadFont(settings->hudFont());
  auto objectiveFont = loadFont(settings->objectiveFont());

  auto levelRenderer = std::make_shared<game::render::LevelRenderer>(
    settings,
    renderer,
    renderer->spritesheet(),
    renderer->spriteAtlas(),
    objectiveFont,
    hudFont
  );

  auto const firstLevelIndex = director->size();
  director->emplace_back(std::make_shared<Level>(
    "level",
    levelId,
    settings,
    session,
    levelRenderer,
    sfx,
    director,
    &root->levels[0]
  ));

  { // Main menu.
    auto overlayMenuRenderer =
      std::make_shared<game::render::OverlayMenuRenderer>(
        renderer.get(),
        menuFont,
        game::render::toColour(0xf0000000u)
      );

    auto const* spriteAtlas = renderer->spriteAtlas();
    auto const pistolFrameIndex = spriteAtlas->key.pistol;
    auto mainMenu = std::make_shared<OverlayMenu>(
      // name
      "mainmenu",
      // id
      mainMenuId,
      // settings
      settings,
      // renderer
      overlayMenuRenderer,
      // sfx
      sfx,
      // director
      director,
      // cursorXOffset
      -10 - spriteAtlas->frames[pistolFrameIndex].w,
      // cursorYOffset
      0,
      // cursorFrameIndex
      pistolFrameIndex,
      // style
      [director](MenuOption const& option, SDL_Colour defaultColour) {
        if (option.text != "Continue") {
          return defaultColour;
        }

        Director::iterator current = director->current();
        if (current == director->end()) {
          return defaultColour;
        }

        Scene const* scene = current->get();
        if (scene->id() != mainMenuId && !scene->isRunning()) {
          return SDL_Colour{64, 64, 64, 0xff}; // Disabled.
        }

        return defaultColour;
      }
      // ...options
    );
    constexpr int mainMenuTextX = 175;
    int mainMenuTextY = 0;
    int mainMenuTextYOffset = 100;
    mainMenu->options().assign({
      MenuOption{
        "Continue",
        [](Director* dir, Scene const* menuScene) {
          dir->pop();
        },
        mainMenuTextX, (mainMenuTextY += mainMenuTextYOffset),
      },
      MenuOption{
        "New Game",
        [firstLevelIndex](Director* dir, Scene const* menuScene) {
          dir->jump(firstLevelIndex);
          auto& level = *(*dir)[firstLevelIndex];
          level.stop();
          level.start();
        },
        mainMenuTextX, (mainMenuTextY += mainMenuTextYOffset),
      },
      MenuOption{
        "Quit",
        [](Director* dir, Scene const* menuScene) {
          dir->quit();
        },
        mainMenuTextX, (mainMenuTextY += mainMenuTextYOffset),
      },
    });
    director->emplace_back(std::move(mainMenu));

    auto restInPeace = std::make_shared<OverlayMenu>(
      "rip",
      ripId,
      settings,
      overlayMenuRenderer,
      sfx,
      director,
      -10 - spriteAtlas->frames[pistolFrameIndex].w,
      0,
      pistolFrameIndex,
      [](MenuOption const& option, SDL_Colour defaultColour) {
        return defaultColour;
      }
    );
    restInPeace->options().assign({
      MenuOption{
        "    R.I.P.",
        [](Director* dir, Scene const* scene) {
          dir->pop();
          dir->jump("mainmenu");
        },
        mainMenuTextX, 100
      },
    });
    director->emplace_back(std::move(restInPeace));
  }
  director->reset(firstLevelIndex);
}


} // namespace game::scenes
