#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_render.h>

#include "game/render/OverlayMenuRenderer.h"
#include "game/render/RenderCapture.h"

#include "game/audio/ChannelId.h"
#include "game/audio/EffectId.h"
#include "game/audio/Effects.h"

#include "game/scenes/Director.h"
#include "game/scenes/OverlayMenu.h"


namespace game::scenes {


void OverlayMenu::setOption(int index) {
  int const size = int(m_options.size());
  index = (index +  size) % size;
  auto const& option = m_options[index];
  m_currentOptionIndex = index;
  m_cursorX = option.x + m_cursorXOffset;
  m_cursorY = option.y + m_cursorYOffset;
}


void OverlayMenu::vHandleEvent(SDL_Event const& event) {
  if (event.type != SDL_KEYDOWN) {
    return;
  }

  using EffectId = game::audio::EffectId;
  using ChannelId = game::audio::ChannelId;

  switch (event.key.keysym.sym) {
    case SDLK_ESCAPE:
      m_sfx->play(ChannelId::whatever, EffectId::gunshot);
      m_director->pop();
      break;

    case SDLK_DOWN:
    case SDLK_j:
      m_sfx->play(ChannelId::whatever, EffectId::click);
      setOption(m_currentOptionIndex + 1);
      break;

    case SDLK_UP:
    case SDLK_k:
      m_sfx->play(ChannelId::whatever, EffectId::click);
      setOption(m_currentOptionIndex - 1);
      break;

    case SDLK_RETURN:
      m_sfx->play(ChannelId::whatever, EffectId::gunshot);
      m_options[m_currentOptionIndex].action(m_director, this);
      break;
  }
}


void OverlayMenu::vRender(unsigned ms) {
  m_renderer->render(m_cursorX, m_cursorY, m_cursorFrameIndex);
}


void OverlayMenu::vStart() {
  setOption(0);
  SDL_SetTextureColorMod(m_renderer->background(), 0x6f, 0x6f, 0x6f);
  m_renderer->capture(
    // Render the underlying state (this OverlayMenu will only become the
    // current() state after start() completes).
    [state = m_director->current()->get(), ren = m_renderer->renderer()] {
      state->render(SDL_GetTicks());
    }
  );
  m_renderer->setText(
    m_style,
    std::begin(m_options),
    std::end(m_options),
    m_settings->fontScale());
  m_sfx->play(game::audio::ChannelId::whatever, game::audio::EffectId::click);
}


} // namespace game::scenes
