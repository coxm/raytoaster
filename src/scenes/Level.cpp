#include <cassert>
#include <cmath>

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_mixer.h>

#include "game/util/Settings.h"
#include "game/util/distance.h"
#include "game/logging/log.h"
#include "game/data/LevelConfig.h"
#include "game/data/World.h"
#include "game/data/actions.h"
#include "game/data/action_traits.h"
#include "game/render/LevelRenderer.h"
#include "game/audio/Effects.h"
#include "game/audio/EffectId.h"
#include "game/audio/ChannelId.h"

#include "game/scenes/Scene.h"
#include "game/scenes/Level.h"


namespace game::scenes {


void GunAnimation::tick(unsigned difference) noexcept {
  recoil = std::max(0.f, recoil - difference * 0.0025f);
}


Level::Level(
  std::string const& name,
  int id,
  game::util::Settings const* settings,
  game::data::Session* session,
  std::shared_ptr<game::render::LevelRenderer> renderer,
  std::shared_ptr<game::audio::Effects> sfx,
  Director* director,
  game::data::LevelConfig const* config
)
  : Scene(name, id)
  , m_settings(settings)
  , m_renderer(std::move(renderer))
  , m_sfx(std::move(sfx))
  , m_progress{}
  , m_config(config)
  , m_director(director)
{
}



#ifndef NDEBUG
bool
Level::handleDebugEvent(SDL_Event const& event) {
  switch (event.type) {
    case SDL_KEYDOWN:
      // Use a separate switch to permit repeat presses.
      switch (event.key.keysym.sym) {
        case SDLK_PLUS:
        case SDLK_EQUALS:
          m_progress.atmosphere.fear.incrementBounded(-1.f);
          return true;
        case SDLK_MINUS:
          m_progress.atmosphere.fear.incrementBounded(1.f);
          return true;
        case SDLK_LEFTBRACKET:
          m_progress.atmosphere.fog.incrementBounded(-0.005);
          return true;
        case SDLK_RIGHTBRACKET:
          m_progress.atmosphere.fog.incrementBounded(0.005);
          return true;
        case SDLK_QUOTE:
          m_progress.atmosphere.fogLimit.incrementBounded(1.f);
          return true;
        case SDLK_HASH:
          m_progress.atmosphere.fogLimit.incrementBounded(-1.f);
          return true;
      }
      break;
  }
  return false; // Not handled.
}
#endif // ifndef NDEBUG


void
Level::vHandleEvent(SDL_Event const& event) {
#ifndef NDEBUG
  if (handleDebugEvent(event)) {
    return; // Event was handled.
  }
#endif

  using MoveState = game::util::MoveState;
  switch (event.type) {
    case SDL_MOUSEMOTION:
      m_progress.camera.turnBy(
        -m_settings->mouseSensitivity() * event.motion.xrel);
      break;

    case SDL_MOUSEBUTTONDOWN:
      {
        // Play both sounds together. As the player's myopia sets in, the
        // mixing of the two together will change.
        using namespace game::audio;
        m_sfx->play(ChannelId::playerAttackNormal, EffectId::gunshot);
        m_sfx->play(ChannelId::playerAttackMuted, EffectId::gunshotMuted);
        --m_progress.ammo;
        // Set the gun recoil to a fraction of the gun height.
        m_gunAnimation.reset(SDL_GetTicks(), 0.4f);
      }

    case SDL_KEYDOWN:
      if (event.key.repeat) {
        return;
      }

      switch (event.key.keysym.sym) {
        case SDLK_LEFT:
        case SDLK_a:
          m_progress.camera.movement.add(MoveState::s_strafeLeft);
          break;

        case SDLK_RIGHT:
        case SDLK_d:
          m_progress.camera.movement.add(MoveState::s_strafeRight);
          break;

        case SDLK_UP:
        case SDLK_w:
          m_progress.camera.movement.add(MoveState::s_moveFwd);
          break;

        case SDLK_DOWN:
        case SDLK_s:
          m_progress.camera.movement.add(MoveState::s_moveBwd);
          break;

        case SDLK_q:
          m_progress.camera.movement.add(MoveState::s_turnLeft);
          break;

        case SDLK_e:
          m_progress.camera.movement.add(MoveState::s_turnRight);
          break;

        case SDLK_ESCAPE:
          m_director->push("mainmenu");
          break;

        default:
          break;
      }
      break;

    case SDL_KEYUP:
      if (event.key.repeat) {
        return;
      }
      switch (event.key.keysym.sym) {
        case SDLK_LEFT:
        case SDLK_a:
          m_progress.camera.movement.remove(MoveState::s_strafeLeft);
          break;

        case SDLK_RIGHT:
        case SDLK_d:
          m_progress.camera.movement.remove(MoveState::s_strafeRight);
          break;

        case SDLK_UP:
        case SDLK_w:
          m_progress.camera.movement.remove(MoveState::s_moveFwd);
          break;

        case SDLK_DOWN:
        case SDLK_s:
          m_progress.camera.movement.remove(MoveState::s_moveBwd);
          break;

        case SDLK_q:
          m_progress.camera.movement.remove(MoveState::s_turnLeft);
          break;

        case SDLK_e:
          m_progress.camera.movement.remove(MoveState::s_turnRight);
          break;

        default:
          break;
      }
      break;
  }
}


/**
 * Balance the volumes of two channels for a single degradable sound effect.
 *
 * @param standard the channel for the standard sound effect.
 * @param degraded the channel for the degraded sound effect.
 * @param degradation the level of degradation.
 * @param maxDegradation the max level of degradation.
 */
template <typename ...ChannelPairs>
void
balanceSFX(int degradation, int maxDegradation, ChannelPairs&& ...pairs) {
  int const standardVolume = MIX_MAX_VOLUME / degradation;
  int const degradedVolume = 4 * degradation;
  (Mix_Volume(int(pairs.first),  standardVolume), ...);
  (Mix_Volume(int(pairs.second), degradedVolume), ...);
}


void
Level::vTick(unsigned ms) {
  // Update gun recoil.
  m_gunAnimation.tick(ms - m_msLastTick);

  // Update movement.
  auto const cellIsEmpty = [world = &m_config->world] (int x, int y) {
    return world->vacant(x, y);
  };
  m_progress.camera.tryWalk(cellIsEmpty);
  m_progress.camera.tryStrafe(cellIsEmpty);
  m_progress.camera.turn();

  // Check triggers.
  m_progress.stage.effectTriggers([this](auto const& action) {
    using EffectType = std::decay_t<decltype(action)>;
    if constexpr(std::is_same_v<EffectType, game::data::SetStage>) {
      int const stageId = action.value;
      if (stageId != m_progress.stage.id) {
        LOG_INFO("Progress to stage " << stageId);
        m_progress.stage.assign(
          stageId,
          std::begin(m_config->triggers),
          std::end(m_config->triggers));
      }
    }
    else if constexpr(game::data::is_resetter_v<EffectType>) {
      LOG_DEBUG("Resetting value");
      m_progress.atmosphere.reset(action, m_config->progress.atmosphere);
    }
    else if constexpr(game::data::is_atmospheric_v<EffectType>) {
      LOG_DEBUG("Atmosphere trigger value: " << action.value);
      m_progress.atmosphere.apply(action);
    }
    else if constexpr(std::is_same_v<EffectType, game::data::PlaySound>) {
      LOG_DEBUG("SFX event");
      m_sfx->play(action.channel, action.effect, action.repeats);
    }
    else {
#ifndef NDEBUG
      LOG_ERROR("Unknown trigger type!");
#endif
    }
  }, m_progress.camera.position, m_config->effects.data());

  // Apply any changes to the atmosphere.
  {
    float const monsterVisibility = m_renderer->rayCaster().spriteVisibility();
    float const monsterMyopiaMult = m_settings->monsterMyopiaMult();
    float const monsterMyopiaDiff = m_settings->monsterMyopiaDiff();
    m_progress.atmosphere.fear.increment =
      monsterMyopiaMult * monsterVisibility + monsterMyopiaDiff;
  }
  m_progress.atmosphere.tick();
  if (m_progress.atmosphere.fear.value > m_settings->maxMyopia() - 0.5f) {
    // Turn off walking SFX.
    using namespace game::audio;
    Mix_Pause(ChannelId_Base(ChannelId::playerFootstepsNormal));
    Mix_Pause(ChannelId_Base(ChannelId::playerFootstepsMuted));
    m_director->jump("rip");
  }

  // SFX.
  {
    using MoveState = game::util::MoveState;
    using namespace game::audio;

    // Adjust volume levels; in particular, the balance of some sound effects
    // that degrade as the player becomes myopic.
    balanceSFX(
      m_progress.atmosphere.fear.value,
      m_settings->maxMyopia(),
      std::make_pair(
        game::audio::ChannelId::playerAttackNormal,
        game::audio::ChannelId::playerAttackMuted
      ),
      std::make_pair(
        game::audio::ChannelId::playerFootstepsNormal,
        game::audio::ChannelId::playerFootstepsMuted
      ));
    // Play/pause walking sound effects.
    bool const wasWalking = m_isWalking;
    m_isWalking =
      (m_progress.camera.movement.flags() & MoveState::s_walking) != 0;

    if (m_isWalking != wasWalking) {
      typedef void(*toggle_fn)(int);
      toggle_fn toggles[2] = {
        Mix_Pause,
        Mix_Resume,
      };
      toggles[m_isWalking](ChannelId_Base(ChannelId::playerFootstepsNormal));
      toggles[m_isWalking](ChannelId_Base(ChannelId::playerFootstepsMuted));
    }

    { // Breathing.
      int const multiplier = m_settings->breathingVolumeMult();
      int const volume = int(m_progress.atmosphere.fear.value * multiplier);
      if (volume != m_breathingVolume) {
        constexpr int channel = int(ChannelId::breathing);
        Mix_Chunk* const chunk = m_sfx->get(EffectId::breathing);
        if (volume == 0) { // Turn effect OFF.
          Mix_FadeOutChannel(channel, 500);
        }
        else if (m_breathingVolume < multiplier + 0.5) { // Turn effect ON.
          Mix_FadeInChannel(channel, chunk, -1, 300);
          // Also play the growling sound effect.
          constexpr int monstersChannel = int(ChannelId::monsters);
          Mix_FadeInChannel(
            monstersChannel, m_sfx->get(EffectId::longGrowl), 0, 100);
        }
        Mix_VolumeChunk(chunk, volume);
        m_breathingVolume = volume;
      }
    }
  }

  m_msLastTick = ms;
}


template <typename Vec2>
float
squareDistance(Vec2 a, Vec2 b) {
  auto const c = a - b;
  auto const d = c * c;
  return d.x + d.y;
}


void
Level::vRender(unsigned ms) {
  // Sort sprites so that we render those further away from the camera first.
  std::sort(
    std::begin(m_progress.enemies),
    std::end(m_progress.enemies),
    [camera = m_progress.camera.position](auto const& a, auto const& b) {
      using namespace game::util;
      return l2DistanceSquared(a.position, camera) >
             l2DistanceSquared(b.position, camera);
    });
  m_renderer->render(m_config->world, m_progress, m_gunAnimation.recoil);
}


void
Level::vStart() {
  m_renderer->setObjectiveText("OBJECTIVE: LOCATE SURVIVORS");
  m_progress = m_config->progress;
#ifndef NDEBUG
  // Assert that the camera position points to a valid cell.
  auto const x = m_progress.camera.position.x;
  auto const y = m_progress.camera.position.y;
  assert((0.f <= x && x < m_config->world. width()) && "Invalid camera x");
  assert((0.f <= y && y < m_config->world.height()) && "Invalid camera y");
#endif

  { // SFX.
    using namespace game::audio;

    m_sfx->play(ChannelId::monsters, EffectId::longGrowl, 0);
    m_sfx->play(
      ChannelId::playerFootstepsNormal,
      EffectId::footstepsNormal,
      Effects::s_repeatForever);
    m_sfx->play(
      ChannelId::playerFootstepsMuted,
      EffectId::footstepsMuted,
      Effects::s_repeatForever);
    Mix_Pause(ChannelId_Base(ChannelId::playerFootstepsNormal));
    Mix_Pause(ChannelId_Base(ChannelId::playerFootstepsMuted));
  }

  m_gunAnimation.reset(SDL_GetTicks(), 1.f);
}


} // namespace game::scenes
