#include "game/logging/log.h"

#include "game/scenes/Hooks.h"


namespace game::scenes {


void
Hooks::log(std::string const& scene, char const* operation) {
  LOG_DEBUG("Scene " << operation << ": " << scene);
}


} // namespace game::scenes
