#include <cstdlib>
#include <algorithm>

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_mixer.h>

#include "game/logging/log.h"

#include "game/util/Settings.h"


namespace game::util {


template <typename T>
struct Parse {};


template <>
struct Parse<char const*> {
  inline static char const* parse(char const* raw, char const* defaultValue) {
    return raw;
  }
};


template <>
struct Parse<int> {
  inline static int parse(char const* raw, int defaultValue) {
    return std::stoi(raw);
  }
};


template <>
struct Parse<float> {
  inline static float parse(char const* raw, float defaultValue) {
    return std::stof(raw);
  }
};


template <>
struct Parse<std::uint32_t> {
  static std::uint32_t parse(char const* raw, std::uint32_t defaultValue) {
    auto value = std::stoul(raw);
    if (value > std::numeric_limits<std::uint32_t>::max()) {
      throw std::out_of_range("stou");
    }
    return std::uint32_t(value);
  }
};


template <>
struct Parse<bool> {
  static bool parse(char const* raw, bool defaultValue) {
    std::string s(raw);
    if (s == "" || s == "true") {
      return true;
    }
    if (s == "false") {
      return false;
    }
    return Parse<int>::parse(raw, int(defaultValue));
  }
};


template <>
struct Parse<std::string> {
  inline static std::string
  parse(char const* raw, std::string const& defaultValue) {
    return std::string(raw);
  }
};


template <>
struct Parse<FontConfig> {
  inline static FontConfig
  parse(char const* raw, FontConfig config) {
    std::string input(raw);
    switch (raw[0]) {
      case '\0':
        return config;
      default: {
        char* end;
        unsigned long ptsize = std::strtoul(raw, &end, 10);
        raw = end;
        if (ptsize > std::numeric_limits<int>::max()) {
          throw std::out_of_range("FontConfig:pt");
        }
        config.pointsize = int(ptsize);
      }
    }

    switch (raw[0]) {
      case '\0':
        return config;
      case ',':
        ++raw;
        // [[fallthrough]]
      default: {
        char* end;
        unsigned long colourMask = std::strtoul(raw, &end, 16);
        raw = end;
        if (colourMask > std::numeric_limits<std::uint32_t>::max()) {
          throw std::out_of_range("FontConfig:mod");
        }
        config.colour = std::uint32_t(colourMask);
      }
    }

    switch (raw[0]) {
      case '\0':
        return config;
      case ',':
        ++raw;
        // [[fallthrough]]
      default:
        config.filepath = (raw + 1);
    }

    LOG_DEBUG("Parsed font config " << config.filepath << " at "
              << config.pointsize << " in " << config.colour);
    return config;
  }
};


template <typename T>
T getEnvVar(char const* name, T defaultValue) {
  char const* raw = std::getenv(name);
  T result = defaultValue;
  if (raw) {
    try {
      result = Parse<T>::parse(raw, defaultValue);
    }
    catch (std::invalid_argument const&) {
      LOG_WARN("Invalid argument (" << name << "): " << raw);
    }
    catch (std::out_of_range const&) {
      LOG_WARN("Out of range (" << name << "): " << raw);
    }
  }
  return result;
}


std::uint32_t
getFlag(char const* name, std::uint32_t flag, bool defaultValue = false) {
  bool const value = getEnvVar(name, defaultValue);
  return value ? flag : 0u;
}


std::string
getBaseDir(int const argc, char const* const* const argv) {
  if (auto const* opt = std::getenv("assets_dir"); opt != nullptr) {
    return opt;
  }

  auto const dirname = Path(argv[0]).remove_filename() / "assets";
  LOG_INFO("Will load assets from '" << dirname.string() << "' directory");
  return dirname.string();
}


constexpr int CENTRED = SDL_WINDOWPOS_CENTERED;
Settings::Settings(int const argc, char const* const* const argv)
  : m_baseDir(getBaseDir(argc, argv))
  , m_menuFont(getEnvVar("menu_font", FontConfig{
      (m_baseDir / "fonts/Langar/Langar-Regular.ttf").string(),
      24,
      0xff0000ffu
    }))
  , m_hudFont(getEnvVar("hud_font", FontConfig{
      (m_baseDir / "fonts/PottaOne/PottaOne-Regular.ttf").string(),
      12,
      0x000000ffu
    }))
  , m_objectiveFont(getEnvVar("objective_font", FontConfig{
      (m_baseDir / "fonts/PottaOne/PottaOne-Regular.ttf").string(),
      18,
      0x32bf30bfu
    }))
  , m_windowRect{
      getEnvVar("window_x", CENTRED),
      getEnvVar("window_y", CENTRED),
      getEnvVar("window_w", 640),
      getEnvVar("window_h", 480)
    }
  , m_windowFlags(
      getFlag("fullscreen", SDL_WINDOW_FULLSCREEN, false) |
      getFlag("borderless", SDL_WINDOW_BORDERLESS, true) |
      SDL_WINDOW_INPUT_GRABBED |
      SDL_WINDOW_INPUT_FOCUS |
      SDL_WINDOW_MOUSE_FOCUS |
      SDL_WINDOW_MOUSE_CAPTURE |
      SDL_WINDOW_SHOWN
    )
  , m_pixelScale(std::max(1, getEnvVar("pixelscale", 1)))
  , m_fontScale(getEnvVar("fontscale", 3))
  , m_sceneHeight(getEnvVar("scene_h", (4 * logicalHeight()) / 5))
  , m_mouseSensitivity(getEnvVar("sensitivity", 0.0025f))
  , m_hud{
      SDL_Rect{
        getEnvVar("ammo_x", 50),
        getEnvVar("ammo_y", 10),
        getEnvVar("ammo_w", 120),
        getEnvVar("ammo_h", 120)
      },
      getEnvVar("objective_x", 20),
      getEnvVar("objective_y", 20),
    }
  , m_fogMult(getEnvVar("fog_mult", 0.16f))
  , m_fogLimit(getEnvVar("fog_limit", 10.f))
  , m_fov(getEnvVar("fov", 1.f))
  , m_cameraPlane(getEnvVar("camera_plane", 0.66f))
  , m_logLevel(getEnvVar("log_level", 0u))
  , m_renderTickMS(getEnvVar("render_ms", 33u))
  , m_logicTickMS(getEnvVar("logic_ms", 33u))
  , m_playerSpeed(getEnvVar("player_speed", 0.15f))
  , m_playerTurnSpeed(getEnvVar("player_turn_speed", 0.1f))
  , m_monsterMyopiaMult(getEnvVar("monster_myopia_mult", 5.f))
  , m_monsterMyopiaDiff(getEnvVar("monster_myopia_diff", -0.2f))
  , m_breathingVolumeMult(
      getEnvVar("breathing_volume_mult", MIX_MAX_VOLUME / maxMyopia()))
{
}


} // namespace game::util
