#include "game/util/Path.h"
#ifdef game_HAS_NO_FILESYSTEM


#include <algorithm>



namespace game::util {


Path Path::operator/(Path const& rhs) const {
  if (m_filepath.empty()) {
    return rhs;
  }
  if (rhs.m_filepath.empty()) {
    return m_filepath;
  }

  Path out{m_filepath};
  auto lhsEnd = m_filepath.begin() + m_filepath.size();
  if (m_filepath.back() == separator) {
    --lhsEnd;
  }

  auto rhsBegin = rhs.m_filepath.begin();
  if (rhs.m_filepath.front() == separator) {
    ++rhsBegin;
  }

  auto const lhsSize = std::string::size_type(lhsEnd - m_filepath.begin());
  auto const rhsSize = std::string::size_type(rhs.m_filepath.end() - rhsBegin);
  out.m_filepath.resize(lhsSize + rhsSize, '\0');
  std::copy(m_filepath.begin(), lhsEnd, out.m_filepath.data());
  std::copy(rhsBegin, rhs.m_filepath.end(), out.m_filepath.data() + lhsSize);
  return out;
}


Path Path::remove_filename() const {
  auto const lastSlash = m_filepath.rfind(separator);
  if (lastSlash == std::string::npos) {
    return Path("/");
  }
  return Path(m_filepath.substr(0, lastSlash + 1));
}


} // namespace game::util

#endif // #ifdef game_HAS_NO_FILESYSTEM
