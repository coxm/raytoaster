#include "game/util/numeric.h"


namespace game::util {


std::uint32_t log2(std::uint32_t v) {
  // This implementation from the bithacks page:
  // https://graphics.stanford.edu/~seander/bithacks.html#IntegerLogLookup
  // The table is adjusted to return log2(0) == 0.
  static const char logTable[256] = {
#define LT(n) n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n
    0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3,
    LT(4), LT(5), LT(5), LT(6), LT(6), LT(6), LT(6),
    LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7)
#undef LT
  };

  std::uint32_t t, tt; // temporaries

  if ((tt = v >> 16)) {
    return (t = tt >> 8) ? 24 + logTable[t] : 16 + logTable[tt];
  }
  else {
    return (t = v >> 8) ? 8 + logTable[t] : logTable[v];
  }
}


} // namespace game::util
