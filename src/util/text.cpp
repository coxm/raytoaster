#include "game/util/text.h"



namespace game::util {


std::string
loadTextFile(std::string const& filepath) {
  std::ifstream ifs(filepath);
  if (!ifs.good()) {
    throw std::runtime_error("Invalid file");
  }
  return std::string(
    std::istreambuf_iterator<char>(ifs),
    std::istreambuf_iterator<char>());
}


} // namespace game::util
