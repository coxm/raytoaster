#include <cassert>
#include <stdexcept>
#include <type_traits>

#include <SDL2/SDL_surface.h>

#include "game/logging/log.h"
#include "game/util/numeric.h"

#include "game/render/mipmap.h"


namespace game::render {


unsigned maxMipMapLevel(unsigned width, unsigned height) {
  return std::min(game::util::log2(width), game::util::log2(height));
}


std::pair<std::unique_ptr<SDL_Surface, SDLSurfaceDeleter>, unsigned>
createMipMap(SDL_Surface* src, SDL_Rect const& srcRect, unsigned count) {
  count = game::util::clamp(
    count, 0u, maxMipMapLevel(unsigned(srcRect.w), unsigned(srcRect.h)));

  auto const* format = src->format;
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> mipmap{
    SDL_CreateRGBSurfaceWithFormat(
      0, srcRect.w * 2, srcRect.h, format->BytesPerPixel, format->format)};
  {
    SDL_BlendMode blendMode;
    if (SDL_GetSurfaceBlendMode(src, &blendMode) < 0) {
      LOG_ERROR("Unable to get surface blend mode: " << SDL_GetError());
      throw std::runtime_error("mipmap:srcblend");
    }
    if (SDL_SetSurfaceBlendMode(mipmap.get(), blendMode) < 0) {
      LOG_ERROR("Unable to set surface blend mode: " << SDL_GetError());
      throw std::runtime_error("mipmap:dstblend");
    }
  }

  SDL_Rect dest{0, 0, srcRect.w, srcRect.h};
  SDL_BlitSurface(src, &srcRect, mipmap.get(), &dest);

  unsigned copies = 1u;
  for (; copies < count; ++copies) {
    dest.x += dest.w;
    dest.w /= 2;
    dest.h /= 2;
    assert((dest.w > 0 && dest.h > 0) && "Generating invalid mipmap");
    SDL_BlitScaled(src, &srcRect, mipmap.get(), &dest);
  }

  return std::make_pair(std::move(mipmap), copies);
}


void
getMipMapCoords(
  SDL_Rect* coords,
  int origW,
  int origH,
  unsigned index
) {
  // The x-coordinate increments by decreasing powers of two, i.e. per the
  // harmonic series: 0w, 1w, (3/2)w, (7/4)w, (15/8)w, ..., (2^n/2^(n-1))w, ...
  // The width and height will always be divided by 2^n, and with our
  // mipmap construction above, the y-offset will always be zero.
  index = std::min(index, maxMipMapLevel(unsigned(origW), unsigned(origH)));
  int const i = int(index);
  int const q = 1 << std::max(0, int(i - 1));
  int const p = 1 << i;
  *coords = SDL_Rect{
    ((p - 1) * origW) / q,
    0,
    origW / p,
    origH / p
  };
}


} // namespace game::render
