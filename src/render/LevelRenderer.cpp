#include <cstring>
#include <stdexcept>

#include <SDL2/SDL_video.h>

#include "game/util/log-rect.h"
#include "game/util/numeric.h"
#include "game/util/Settings.h"
#include "game/logging/log.h"
#include "game/data/SpriteAtlas.h"
#include "game/data/Atmosphere.h"
#include "game/data/Progress.h"

#include "game/render/colour.h"
#include "game/render/mipmap.h"
#include "game/render/Renderer.h"
#include "game/render/LevelRenderer.h"


namespace game::render {


inline SDL_Texture*
createTexture(
  SDL_Renderer* renderer,
  int width,
  int height,
  int access = SDL_TEXTUREACCESS_STREAMING
) {
  SDL_Texture* const texture = SDL_CreateTexture(
    renderer, SDL_PIXELFORMAT_RGBA8888, access, width, height);
  if (!texture) {
    LOG_ERROR("Unable to create texture: " << SDL_GetError());
    throw std::runtime_error("SDL_CreateTexture");
  }
  return texture;
}


constexpr unsigned MAX_GUN_SCALES = 5;


LevelRenderer::LevelRenderer(
  game::util::Settings const* settings,
  std::shared_ptr<Renderer> renderer,
  SDL_Surface const* spritesheet,
  game::data::SpriteAtlas const* spriteAtlas,
  std::shared_ptr<TTF_Font> objectiveFont,
  std::shared_ptr<TTF_Font> hudFont
)
  : m_rayCaster{settings, spritesheet, spriteAtlas}
  , m_settings{settings}
  , m_renderer{std::move(renderer)}
  , m_sceneTex{nullptr}
  // , m_hudTex{nullptr}
  , m_gunTex{nullptr}
  , m_objective{
      objectiveFont,
      m_renderer->renderer(),
      settings->hud().objectiveX,
      settings->hud().objectiveY,
      toColour(settings->objectiveFont().colour)}
  // , m_hud{
  //     settings,
  //     m_renderer,
  //     hudFont,
  //     "Ammo",
  //     toColour(settings->hudFont().colour)}
  , m_gunDestRect{}
  , m_gunMipMapCount{0}
{
  auto const& windowRect = m_settings->windowRect();
  auto const sceneHeight = m_settings->sceneHeight();
  m_sceneTex.reset(createTexture(
    m_renderer->renderer(), windowRect.w, sceneHeight));
  // m_hudTex.reset(createTexture(
  //   m_renderer->renderer(), windowRect.w, m_settings->hudHeight()));

  {
    auto const* atlas = m_renderer->spriteAtlas();
    auto const& frame = atlas->frames[atlas->key.machinegun];
    auto [mipmap, count] = createMipMap(
      m_renderer->spritesheet(), frame, MAX_GUN_SCALES + 1);
    m_gunMipMapCount = count;
    m_gunTex.reset(createTexture(
      m_renderer->renderer(), mipmap->w, mipmap->h,
      SDL_TEXTUREACCESS_STATIC));
    SDL_UpdateTexture(m_gunTex.get(), nullptr, mipmap->pixels, mipmap->pitch);
    SDL_SetTextureBlendMode(m_gunTex.get(), SDL_BLENDMODE_BLEND);
    m_gunDestRect.x = (windowRect.w - frame.w) / 2;
    m_gunDestRect.y = (sceneHeight - frame.h);
    m_gunDestRect.w = frame.w;
    m_gunDestRect.h = frame.h;
  }
}


unsigned
fearToGunMipMapIndex(int scale) {
  // We're mapping (roughly) scale => log2(scale), [1, 32ish] -> [0, 3ish]
  return std::min(game::util::log2(unsigned(scale + 1u)) - 1u, MAX_GUN_SCALES);
}


void
LevelRenderer::render(
  game::data::World const& world,
  game::data::Progress const& progress,
  float gunRecoil
) {
  SDL_Renderer* const ren = m_renderer->renderer();
  int const unscaledWidth = m_settings->windowRect().w;
  int const unscaledHeight = m_settings->sceneHeight();
  int const fear = progress.atmosphere.fear.value;
  int const scaledWidth = unscaledWidth / fear;
  int const scaledHeight = unscaledHeight / fear;

  { // Set colour mod according to how "scared" the protagonist is.
    int const saturation = 255 - std::min(7 * fear, 224);
    SDL_SetTextureColorMod(m_sceneTex.get(), 255, saturation, saturation);
  }

  // Scene rect.
  m_rayCaster.render(
    m_sceneTex.get(),
    world,
    progress,
    scaledWidth,
    scaledHeight
  );
  {
    SDL_Rect const src{0, 0, scaledWidth, scaledHeight};
    SDL_Rect const dest{0, 0, unscaledWidth, unscaledHeight};
    SDL_RenderCopy(ren, m_sceneTex.get(), &src, &dest);
  }
  { // Gun.
    unsigned const index = std::min(
      fearToGunMipMapIndex(fear), m_gunMipMapCount);
    SDL_Rect srcRect;
    getMipMapCoords(&srcRect, m_gunDestRect.w, m_gunDestRect.h, index);
    int const gunYOffset = int(gunRecoil * srcRect.h);
    srcRect.h -= gunYOffset;

    SDL_Rect destRect = m_gunDestRect;
    destRect.y += gunYOffset;
    destRect.h -= gunYOffset;
    SDL_RenderCopy(ren, m_gunTex.get(), &srcRect, &destRect);
  }
  { // Objective text.
    unsigned const index = game::util::log2(unsigned(fear / 3));
    m_objective.use(index);
    m_objective.render(ren);
  }

  // Finally, the HUD.
  // m_hud.render();

  SDL_RenderPresent(ren);
}


void
LevelRenderer::setObjectiveText(char const* text) {
  m_objective.blended(m_renderer->renderer(), text);
}


} // namespace game::render
