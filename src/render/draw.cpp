#include <SDL2/SDL_surface.h>

#include "game/render/draw.h"


namespace game::render {


void
drawVerticalLine(
  SDL_Surface* surface,
  int column,
  int ymin,
  int ymax,
  Uint32 colour
) {
  if (ymax < 0 || windowHeight < ymin) {
    return;
  }
  assert((ymin <= ymax) && "ymin must be at most ymax");
  ymin = std::max(0, ymin);
  ymax = std::min(windowHeight, ymax);

  auto const dpix = surface->pitch / 4;
  auto* pixels = static_cast<Uint32*>(surface->pixels) + column + ymin * dpix;
  for (int y = ymin; y <= ymax; ++y, pixels += dpix) {
    *pixels = colour;
  }
}


} // namespace game::render
