#include <SDL2/SDL_surface.h>

#ifdef game_WITH_SDL2_IMAGE
# include <SDL2/SDL_image.h>
#else
# include <stb/stb_image.h>
#endif

#include "game/logging/log.h"

#include "game/render/loadSurface.h"


namespace game::render {


#define IMAGE_INFO(surface) \
     "Bytes pp: " << int(surface->format->BytesPerPixel) << '\n' \
  << "Bits pp:  " << int(surface->format->BitsPerPixel) << '\n' \
  << "Pitch:    " << surface->pitch << '\n' \
  << "Masks:    " << surface->format->Rmask << ", " \
                  << surface->format->Gmask << ", " \
                  << surface->format->Bmask << ", " \
                  << surface->format->Amask
#undef IMAGE_INFO


#ifdef game_WITH_SDL2_IMAGE
std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter>
loadFormattedSurface(char const* filepath, Uint32 format, bool locked) {
  std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter> loaded(
    IMG_Load(filepath)
  );
  if (!loaded) {
    LOG_ERROR("Unable to laod image '" << filepath << "': " << IMG_GetError());
    throw std::runtime_error(filepath);
  }

  std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter> formatted(
    SDL_ConvertSurfaceFormat(loaded.get(), format, 0));
  if (!formatted) {
    LOG_ERROR("Unable to set '" << filepath << "' format: " << IMG_GetError());
    throw std::runtime_error(filepath);
  }

  if (
    locked &&
    SDL_MUSTLOCK(formatted.get()) &&
    SDL_LockSurface(formatted.get()) != 0
  ) {
    LOG_ERROR(
      "Unable to lock surface '" << filepath << "': " << SDL_GetError());
    throw std::runtime_error(filepath);
  }
  return formatted;
}
#else


struct STBImageDeleter {
  inline void operator()(unsigned char* data) const noexcept {
    stbi_image_free(data);
  }
};


Uint32
pixelFormatToChannelCount(Uint32 pixelFormat) noexcept {
  switch (pixelFormat) {
    case SDL_PIXELFORMAT_RGB332:    return 3;
    case SDL_PIXELFORMAT_RGB444:    return 3;
    case SDL_PIXELFORMAT_RGB555:    return 3;
    case SDL_PIXELFORMAT_BGR555:    return 3;
    case SDL_PIXELFORMAT_RGB24:     return 3;
    case SDL_PIXELFORMAT_ARGB8888:  return 4;
    case SDL_PIXELFORMAT_RGBA8888:  return 4;
    case SDL_PIXELFORMAT_ABGR8888:  return 4;
    case SDL_PIXELFORMAT_BGRA8888:  return 4;
    default:                        return 0;
  }
}


std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter>
loadFormattedSurface(char const* filepath, Uint32 format, bool locked) {
  int width, height, numChannels;
  int const desiredChannels = format == pixelFormatToChannelCount(format);
  std::unique_ptr<unsigned char[], STBImageDeleter> data(
    stbi_load(filepath, &width, &height, &numChannels, desiredChannels));
  if (!data) {
    LOG_ERROR("Unable to load image '" << filepath << '\'');
    throw std::runtime_error(filepath);
  }

  int depth, pitch;
  if (numChannels == STBI_rgb) {
    depth = 24;
    pitch = 3 * width;
  }
  else {
    depth = 32;
    pitch = 4 * width;
  }

  // STBI always loads in either RGB24 (3-channel) or RGBA32 (4-channel). The
  // SDL pixel format this corresponds to depends could be RGBA8888 or ABGR8888
  // depending on endianness of the machine. Therefore in some cases we will
  // need to convert the surface to the desired format.
  Uint32 const stbiFormat = numChannels == STBI_rgb
    ? SDL_PIXELFORMAT_RGB24
    : SDL_PIXELFORMAT_RGBA32;

  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> surface(
    SDL_CreateRGBSurfaceWithFormatFrom(
      data.get(), width, height, depth, pitch, stbiFormat));
  if (!surface) {
    LOG_ERROR("Unable to create surface for '" << filepath << "': "
        << SDL_GetError());
    throw std::runtime_error(filepath);
  }

  if (SDL_PIXELFORMAT_RGBA32 != format) {
    std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> tmp(
      SDL_ConvertSurfaceFormat(surface.get(), SDL_PIXELFORMAT_RGBA8888, 0));
    std::swap(tmp, surface);
  }

  if (
    locked &&
    SDL_MUSTLOCK(surface.get()) &&
    SDL_LockSurface(surface.get()) != 0
  ) {
    LOG_ERROR(
      "Unable to lock surface '" << filepath << "': " << SDL_GetError());
    throw std::runtime_error(filepath);
  }
  return surface;
}
#endif


} // namespace game::render
