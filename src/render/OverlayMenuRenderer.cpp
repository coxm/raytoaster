#include <SDL2/SDL_render.h>
#include <SDL2/SDL_ttf.h>

#include "game/util/Settings.h"
#include "game/data/SpriteAtlas.h"

#include "game/logging/log.h"

#include "game/render/colour.h"
#include "game/render/Renderer.h"
#include "game/render/OverlayMenuRenderer.h"


namespace game::render {


OverlayMenuRenderer::OverlayMenuRenderer(
  Renderer* renderer,
  std::shared_ptr<TTF_Font> font,
  SDL_Colour fontColour
)
  : m_font(std::move(font))
  , m_renderer(renderer)
  , m_background(nullptr)
  , m_text{nullptr}
  , m_fontColour{fontColour}
{
  int width, height;
  SDL_GetRendererOutputSize(m_renderer->renderer(), &width, &height);
  m_background.reset(SDL_CreateTexture(
    m_renderer->renderer(),
    SDL_PIXELFORMAT_RGBA8888,
    SDL_TEXTUREACCESS_TARGET,
    width, height));
  m_text.reset(SDL_CreateTexture(
    m_renderer->renderer(),
    SDL_PIXELFORMAT_RGBA8888,
    SDL_TEXTUREACCESS_STATIC,
    width, height));
}


std::unique_ptr<SDL_Surface, SDLSurfaceDeleter>
OverlayMenuRenderer::initTextSurface() {
  int windowWidth, windowHeight;
  SDL_GetWindowSize(m_renderer->window(), &windowWidth, &windowHeight);
  return std::unique_ptr<SDL_Surface, SDLSurfaceDeleter>(
    SDL_CreateRGBSurfaceWithFormat(
      0, // flags ("unused and should be set to 0")
      windowWidth,
      windowHeight,
      32, // Depth in bits.
      SDL_PIXELFORMAT_RGBA8888));
}


void
OverlayMenuRenderer::blitText(
  SDL_Surface* surface,
  char const* text,
  SDL_Color colour,
  int x,
  int y,
  int scale
) {
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> rendered(
    TTF_RenderText_Blended(m_font.get(), text, toColour(colour)));
  if (!rendered) {
    LOG_ERROR("Unable to render text '" << text << "': " << TTF_GetError());
    throw std::runtime_error("Unable to render text");
  }

  SDL_Rect dest{
    x, y,
    scale * rendered->w,
    scale * rendered->h};
  SDL_BlitScaled(rendered.get(), nullptr, surface, &dest);
}


void
OverlayMenuRenderer::render(
  int cursorX,
  int cursorY,
  int cursorFrameIndex
)
  const
{
  // Background (the previous scene, pre-rendered).
  SDL_RenderCopy(m_renderer->renderer(), m_background.get(), nullptr, nullptr);

  // Text (all options, pre-rendered).
  SDL_RenderCopy(m_renderer->renderer(), m_text.get(), nullptr, nullptr);

  { // Cursor.
    auto const& frame = m_renderer->spriteAtlas()->frames[cursorFrameIndex];
    SDL_Rect const dest{
      cursorX,
      cursorY,
      frame.w,
      frame.h
    };
    SDL_RenderCopy(
      m_renderer->renderer(), m_renderer->spritesheetTexture(),
      &frame, &dest);
    SDL_RenderPresent(m_renderer->renderer());
  }
}


} // namespace game::render
