#include <SDL2/SDL_render.h>

#include "game/util/Settings.h"
#include "game/data/Root.h"

#include "game/render/Renderer.h"
#include "game/render/init.h"


namespace game::render {


std::shared_ptr<Renderer>
init(game::util::Settings const* settings, game::data::Root const* gamedata) {
  auto renderer = std::make_shared<Renderer>(
    settings, gamedata, "k9",
    (settings->basedir() / "textures/spritesheet.png").string());
  SDL_SetRenderDrawColor(renderer->renderer(), 0x00, 0x00, 0x00, 0xff);
  return renderer;
}


} // namespace game::render
