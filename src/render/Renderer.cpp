#include <stdexcept>

#include "game/data/Root.h"
#include "game/util/Settings.h"
#include "game/logging/log.h"

#include "game/render/loadSurface.h"
#include "game/render/Renderer.h"


namespace game::render {


template <typename Fn, typename... Args>
inline auto
invoke(Fn fn, char const* error, Args&& ...args) {
  auto* const p = fn(std::forward<Args>(args)...);
  if (!p) {
    LOG_ERROR(error << ": " << SDL_GetError());
    throw std::runtime_error(error);
  }
  return p;
}


inline SDL_Texture*
createTexture(SDL_Renderer* renderer, int width, int height) {
  return invoke(
    SDL_CreateTexture, "Unable to create texture",
    renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING,
    width, height);
}


Renderer::Renderer(
  game::util::Settings const* settings,
  game::data::Root const* gamedata,
  std::string const& windowName,
  std::string const& spritesheetPath
)
  : m_settings(settings)
  , m_window(invoke(
      SDL_CreateWindow,
      "Unable to create window",
      windowName.c_str(),
      settings->windowRect().x, settings->windowRect().y,
      settings->windowRect().w, settings->windowRect().h,
      settings->windowFlags()))
  , m_renderer(invoke(
      SDL_CreateRenderer,
      "Unable to create renderer",
      m_window.get(), -1,
      SDL_RENDERER_ACCELERATED |  // We want accelerated if possible.
      SDL_RENDERER_PRESENTVSYNC | // Allow VSync if possible.
      SDL_RENDERER_TARGETTEXTURE  // We need target textures for the menus.
    ))
  , m_spritesheet(game::render::loadFormattedSurface(
      spritesheetPath.c_str(), SDL_PIXELFORMAT_RGBA8888, true))
  , m_spritesheetTex(m_spritesheet == nullptr ? nullptr : invoke(
      SDL_CreateTextureFromSurface, "Unable to create spritesheet texture",
      m_renderer.get(), m_spritesheet.get()
    ))
  , m_spriteAtlas(&gamedata->spriteAtlas)
  , m_sceneTex(createTexture(
      m_renderer.get(), settings->logicalWidth(), settings->sceneHeight()))
  , m_rayCaster(settings, m_spritesheet.get(), m_spriteAtlas)
{
  auto const& windowRect = settings->windowRect();
  auto const logicalWidth = settings->logicalWidth();
  auto const logicalHeight = settings->logicalHeight();
  if (logicalWidth != windowRect.w || logicalHeight != windowRect.h) {
    SDL_RenderSetLogicalSize(m_renderer.get(), logicalWidth, logicalHeight);
  }
  if (!m_spritesheet) {
    LOG_CRITICAL("Failed to load " << spritesheetPath);
    throw std::runtime_error(spritesheetPath);
  }
  // TODO: can we enable this? Might speed up rendering.
  // SDL_SetTextureBlendMode(m_sceneTex.get(), SDL_BLENDMODE_NONE);
  if (!m_spritesheetTex) {
    LOG_CRITICAL("Failed to create spritesheet texture: " << SDL_GetError());
    throw std::runtime_error("Unable to create spritesheet texture");
  }
}


Renderer::Renderer(Renderer&&) noexcept = default;
Renderer& Renderer::operator=(Renderer&&) noexcept = default;
Renderer::~Renderer() noexcept = default;


} // namespace game::render
