#include <utility>

#include "game/render/SurfaceLock.h"


namespace game::render {


int surfaceLockNoOp(SDL_Surface* surface) {
  return 0;
}


void surfaceUnlockNoOp(SDL_Surface* surface) {
}


} // namespace game::render
