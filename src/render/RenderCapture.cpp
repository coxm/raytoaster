#include <stdexcept>

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>

#include "game/render/RenderCapture.h"


namespace game::render {


RenderCapture::RenderCapture(SDL_Renderer* renderer, SDL_Texture* texture)
  : m_renderer(renderer)
  , m_texture(texture)
{
#ifndef NDEBUG
  SDL_RendererInfo info;
  SDL_GetRendererInfo(m_renderer, &info);
  if (0 != SDL_GetRendererInfo(m_renderer, &info)) {
    throw std::runtime_error("Unable to get renderer info");
  }
  if (0 == (info.flags & SDL_RENDERER_TARGETTEXTURE)) {
    throw std::runtime_error("Renderer does not support texture targets");
  }
  int access;
  if (0 != SDL_QueryTexture(texture, nullptr, &access, nullptr, nullptr)) {
    throw std::runtime_error("Unable to query texture");
  }
  if (access != SDL_TEXTUREACCESS_TARGET) {
    throw std::runtime_error("Texture is not suitable for targetting");
  }
#endif
  SDL_SetRenderTarget(m_renderer, m_texture);
}


} // namespace game::render
