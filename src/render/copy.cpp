#include <cstring>

#include <SDL2/SDL_surface.h>

#include "game/render/copy.h"


namespace game::render {


void
copy(
  SDL_Rect const& src,
  SDL_Surface const* surface,
  std::byte* texturePixels,
  int destPitch
)
  noexcept
{
  std::byte const* surfacePixels = static_cast<std::byte*>(surface->pixels);
  constexpr int bpp = sizeof(int); // bytes per pixel.
  assert((surface->format->BytesPerPixel == bpp) && "Invalid surface format");

  for (int row = 0; row < src.h; ++row) {
    std::memcpy(
      texturePixels + row * destPitch,
      surfacePixels + (row + src.y) * surface->pitch + src.x * bpp,
      src.w * bpp);
  }
}


void
copy(
  SDL_Rect const& srcRect,
  SDL_Surface const* src,
  int xDest,
  int yDest,
  SDL_Texture* texture
)
  noexcept
{
  SDL_Rect dest{xDest, yDest, srcRect.w, srcRect.h};
  void* texturePixelsVoid;
  int texturePitch;
  SDL_LockTexture(texture, &dest, &texturePixelsVoid, &texturePitch);
  std::byte* texturePixels = static_cast<std::byte*>(texturePixelsVoid);
  copy(srcRect, src, texturePixels, texturePitch);
  SDL_UnlockTexture(texture);
}


} // namespace game::render
