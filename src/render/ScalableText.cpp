#include <SDL2/SDL_render.h>
#include <SDL2/SDL_ttf.h>

#include "game/render/ScalableText.h"


namespace game::render {


ScalableText::ScalableText(
  std::shared_ptr<TTF_Font> font,
  SDL_Renderer* renderer,
  int destX, int destY,
  SDL_Colour foreground,
  SDL_Colour background,
  unsigned maxMapped
)
  : m_font(font)
  , m_texture(destX, destY)
  , m_foreground(foreground)
  , m_background(background)
{
}


void
ScalableText::solid(SDL_Renderer* renderer, char const* text) {
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> surface{
    TTF_RenderText_Solid(m_font.get(), text, m_foreground)};
  m_texture.reset(renderer, surface.get());
}


void
ScalableText::shaded(SDL_Renderer* renderer, char const* text) {
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> surface{
    TTF_RenderText_Shaded(m_font.get(), text, m_foreground, m_background)};
  m_texture.reset(renderer, surface.get());
}


void
ScalableText::blended(SDL_Renderer* renderer, char const* text) {
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> surface{
    TTF_RenderText_Blended(m_font.get(), text, m_foreground)};
  m_texture.reset(renderer, surface.get());
}


void
ScalableText::render(SDL_Renderer* renderer) const noexcept {
  m_texture.render(renderer);
}


} // namespace game::render
