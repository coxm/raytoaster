#include <cstring>
#include <cstdlib>

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>

#include "game/util/Errors.h"
#include "game/util/numeric.h"
#include "game/data/SpriteAtlas.h"
#include "game/data/Progress.h"
#include "game/data/Atmosphere.h"
#include "game/data/World.h"
#include "game/data/Atmosphere.h"
#include "game/data/StaticCamera.h"
#include "game/render/RayCaster.h"
#include "game/render/TextureLock.h"
#include "game/render/raycast.h"


namespace game::render {


RayCaster::RayCaster(
  game::util::Settings const* settings,
  SDL_Surface const* spritesheet,
  game::data::SpriteAtlas const* spriteAtlas
)
  : m_spritesheet(spritesheet)
  , m_spriteAtlas(spriteAtlas)
  , m_zBuffer(std::make_unique<float[]>(settings->sceneWidth()))
  , m_spriteVisibility(0.f)
{
}


Uint32
sampleSurface(
  Uint32 const* pixels,
  int const surfaceWidth,
  SDL_Rect const& frame,
  float x, float y
) {
  int const tx = game::util::clamp(
    int(frame.w  * (x - int(x))),
    0,
    frame.w - 1
  );
  int const ty = game::util::clamp(
    int(frame.h * (y - int(y))),
    0,
    frame.h - 1
  );
  return pixels[(frame.y + ty) * surfaceWidth + frame.x + tx];
}


void
RayCaster::render(
  TextureLock const& texture,
  game::data::World const& world,
  game::data::Progress const& progress,
  int width,
  int height
)
  noexcept
{
  auto const* const frames = m_spriteAtlas->frames.data();
  auto const* const spritesheetPixels =
    static_cast<Uint32 const*>(m_spritesheet->pixels);
  auto const spritesheetWidth = m_spritesheet->w;

  raycastBackground(
    // Get floor and ceiling pixels.
    [&world, frames, spritesheetPixels, spritesheetWidth] (float x, float y) {
      int const cellX = game::util::clamp(int(x), 0, world.width() - 1);
      int const cellY = game::util::clamp(int(y), 0, world.height() - 1);
      auto const cell = world.cell(cellX, cellY);
      auto const& floorFrame = frames[cell.floor()];
      auto const& ceilingFrame = frames[cell.ceiling()];
      return std::make_pair(
        sampleSurface(spritesheetPixels, spritesheetWidth, floorFrame, x, y),
        sampleSurface(spritesheetPixels, spritesheetWidth, ceilingFrame, x, y)
      );
    },
    progress.camera,
    texture,
    width, height,
    progress.atmosphere.fog.value,
    progress.atmosphere.fogLimit.value);

  raycastWalls(
    // Wall finding function.
    [&world, frames, spritesheetPixels, spritesheetWidth] (
      int cellX, int cellY,
      int* wallSurfaceWidth,
      SDL_Rect* frame
    ) -> Uint32 const* {
      auto const [index, occupied] = world.cell(cellX, cellY).wall();
      if (!occupied) {
        return nullptr;
      }
      *frame = frames[index];
      *wallSurfaceWidth = spritesheetWidth;
      return spritesheetPixels + frame->y * spritesheetWidth + frame->x;
    },
    // SetDepth function: update the z-buffer for each column.
    [buffer = m_zBuffer.get()] (int x, float depth) {
      buffer[x] = depth;
    },
    progress.camera,
    texture,
    width,
    height,
    progress.atmosphere.fog.value,
    progress.atmosphere.fogLimit.value
  );

  m_spriteVisibility = 0.f;

  using Sprite = game::data::Enemy;
  raycastSprites(
    // Position getter.
    [] (Sprite const& sprite) { return sprite.position; },
    // Sprite query function.
    [frames, spritesheetWidth, spritesheetPixels] (
      Sprite const& sprite,
      SpriteQuery* query
    ) {
      query->pixels = spritesheetPixels;
      query->frame = frames[sprite.frame];
      query->divideX = sprite.divide.x;
      query->divideY = sprite.divide.y;
      query->surfaceWidth = spritesheetWidth;
      query->yOffset = sprite.yOffset;
    },
    // Transparency function.
    [] (Uint32 colour) {
      return (colour & 0x000000ff) == 0;
    },
    // Sprite visibility callback.
    [this, mult = 1.f / width] (Sprite const& sprite, int visibleColumns) {
      m_spriteVisibility += visibleColumns * mult;
    },
    progress.enemies.begin(), progress.enemies.end(),
    m_zBuffer.get(),
    progress.camera,
    texture,
    width,
    height
  );
}


} // namespace game::render
