#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_render.h>

#include "game/render/mipmap.h"
#include "game/render/MipMappedTexture.h"


namespace game::render {


MipMappedTexture::MipMappedTexture() noexcept
  : m_texture(nullptr)
  , m_origW(0)
  , m_origH(0)
  , m_index(0)
  , m_count(0)
{
}


MipMappedTexture::MipMappedTexture(
  SDL_Renderer* renderer,
  SDL_Surface* source,
  unsigned maxMapped
)
  : m_texture(nullptr)
  , m_origW()
  , m_origH()
  , m_index()
  , m_count()
{
  reset(renderer, source, maxMapped);
}


MipMappedTexture::MipMappedTexture(
  SDL_Renderer* renderer,
  SDL_Surface* source,
  SDL_Rect const& srcRect,
  unsigned maxMapped
)
  : m_texture(nullptr)
  , m_origW()
  , m_origH()
  , m_index()
  , m_count()
{
  reset(renderer, source, srcRect, maxMapped);
}


void
MipMappedTexture::reset(
  SDL_Renderer* renderer,
  SDL_Surface* src,
  unsigned maxMapped
) {
  reset(renderer, src, SDL_Rect{0, 0, src->w, src->h}, maxMapped);
}


void
MipMappedTexture::reset(
  SDL_Renderer* renderer,
  SDL_Surface* src,
  SDL_Rect const& srcRect,
  unsigned maxMapped
) {
  auto const [surface, count] = createMipMap(src, srcRect, maxMapped);
  m_texture.reset(SDL_CreateTextureFromSurface(renderer, surface.get()));
  m_origW = src->w;
  m_origH = src->h;
  m_index = 0;
  m_count = count;
}



} // namespace game::render
