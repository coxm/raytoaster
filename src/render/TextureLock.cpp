#include <cassert>
#include <cstring>

#include <SDL2/SDL_error.h>
#include <SDL2/SDL_render.h>

#include "game/logging/log.h"

#include "game/render/TextureLock.h"


namespace game::render {


void TextureLock::clear() const {
  int height;
#ifndef NDEBUG
  // Guard this variable as it's unused if NDEBUG is set, causing errors.
  int const result =
#endif
  SDL_QueryTexture(m_texture, nullptr, nullptr, nullptr, &height);
  assert(result != 0 && "Unable to clear texture");
  std::memset(m_pixels, 0, height * m_pitch);
}


void TextureLock::doLock(SDL_Texture* texture, SDL_Rect const* rect) {
#define LOCK SDL_LockTexture(texture, rect, &m_pixels, &m_pitch)
#ifdef NDEBUG
  LOCK;
#else
  int const result = LOCK;
  assert((result == 0) && "Can't lock non-streaming texture");
#endif
  m_width = m_pitch / sizeof(Uint32);
#undef LOCK
}


} // namespace game::render
