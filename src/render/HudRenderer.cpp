#include <cstring>
#include <iostream>
#include "game/util/log-rect.h"

#include <SDL2/SDL_render.h>
#include <SDL2/SDL_ttf.h>

#include "game/util/Settings.h"
#include "game/data/SpriteAtlas.h"

#include "game/render/copy.h"
#include "game/render/Renderer.h"
#include "game/render/HudRenderer.h"


namespace game::render {


HudRenderer::HudRenderer(
  game::util::Settings const* settings,
  std::shared_ptr<Renderer> renderer,
  std::shared_ptr<TTF_Font> const& font,
  char const* ammoText,
  SDL_Colour textColour
)
  : m_font(std::move(font))
  , m_renderer(std::move(renderer))
  , m_settings(settings)
  , m_texture(nullptr)
  , m_ammoTextRegion{
      settings->hud().ammoTextDest.x, settings->hud().ammoTextDest.y, 0, 0}
  , m_textColour(textColour)
{
  auto const width = m_settings->windowRect().w;
  auto const height = m_settings->hudHeight();
  m_texture.reset(SDL_CreateTexture(
    m_renderer->renderer(),
    SDL_PIXELFORMAT_RGBA8888,
    SDL_TEXTUREACCESS_STREAMING,
    width, height));
  {
    auto const* atlas = m_renderer->spriteAtlas();
    TextureLock tex{m_texture.get()};
    copy(
      atlas->frames[atlas->key.hudBackground],
      m_renderer->spritesheet(),
      tex.bytes(),
      tex.pitch());
  }
  if (ammoText) {
    setAmmoText(ammoText);
  }
}


void
HudRenderer::setAmmoText(char const* const text) {
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> written{
    TTF_RenderText_Blended(m_font.get(), text, m_textColour)};
  int const scaledW = written->w * m_settings->fontScale();
  int const scaledH = written->h * m_settings->fontScale();
  SDL_Rect const overwriteRect{
    m_ammoTextRegion.x,
    m_ammoTextRegion.y,
    std::max(m_ammoTextRegion.w, scaledW),
    std::max(m_ammoTextRegion.h, scaledH)};
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> surface{
    SDL_CreateRGBSurfaceWithFormat(
      0, overwriteRect.w, overwriteRect.h, 32, SDL_PIXELFORMAT_RGBA8888)};

  { // Copy background.
    auto const* spriteAtlas = m_renderer->spriteAtlas();
    auto const& bgFrame = spriteAtlas->frames[spriteAtlas->key.hudBackground];
    auto* spritesheet = m_renderer->spritesheet(); // Can't be const.
    SDL_Rect const srcRect{
      bgFrame.x + m_ammoTextRegion.x,
      bgFrame.y + m_ammoTextRegion.y,
      overwriteRect.w,
      overwriteRect.h
    };
    SDL_Rect destRect{0, 0, overwriteRect.w, overwriteRect.h};
    SDL_BlitSurface(spritesheet, &srcRect, surface.get(), &destRect);
  }

  { // Write over background.
    m_ammoTextRegion.w = scaledW;
    m_ammoTextRegion.h = scaledH;
    SDL_Rect destRect{0, 0, scaledW, scaledH};
    SDL_BlitScaled(written.get(), nullptr, surface.get(), &destRect);
  }

  // Copy to texture.
  TextureLock tex(m_texture.get(), overwriteRect);
  copy(
    SDL_Rect{0, 0, overwriteRect.w, overwriteRect.h},
    surface.get(),
    tex.bytes(),
    tex.pitch());
}


void
HudRenderer::render() const noexcept {
  SDL_Rect const dest{
    0, m_settings->sceneHeight(),
    m_settings->windowRect().w, m_settings->hudHeight()
  };
  SDL_RenderCopy(m_renderer->renderer(), m_texture.get(), nullptr, &dest);
}


} // namespace game::render
