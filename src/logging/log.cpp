#include <cstdio>

#include <SDL2/SDL_timer.h>

#include "game/logging/log.h"


namespace game::logging {


std::string now() {
  // SDL version.
  auto elapsed = SDL_GetTicks();
  auto const milliseconds = elapsed % 1000;
  elapsed = (elapsed - milliseconds) / 1000;
  auto const seconds = elapsed % 60;
  elapsed = (elapsed - seconds) / 60;
  auto const minutes = elapsed % 60;
  elapsed = (elapsed - minutes) / 60;
  auto const hours = elapsed % 100;
  // Try to keep it below 22 characters, the likely limit for small string
  // optimisation on 64-bit platforms. E.g. "[hh:mm:ss.mls] " has 15.
  constexpr std::size_t buflen = 16;
  char output[buflen] = {'\0'};
  std::snprintf(&output[0], buflen, "[%02d:%02d:%02d.%03d] ",
                hours, minutes, seconds, milliseconds);
  return output;
}


} // namespace game::logging
