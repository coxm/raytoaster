#include <fstream>

#include "game/logging/log.h"

#include "game/data/FileFormat.h"
#include "game/data/archive-data.h"
#include "game/data/init.h"
#include "game/data/Root.h"
#include "game/data/serialisation.h"


namespace game::data {


std::unique_ptr<Root>
init(std::string const& filepath) {
  auto root = std::make_unique<Root>();
  std::ifstream ifs(
      filepath,
      std::ios_base::in | std::ios_base::binary);
  if (!archiveData(*root, ifs, FileFormat::binary)) {
    LOG_CRITICAL("Unable to load game data from " << filepath);
    throw std::runtime_error(filepath);
  }
  return root;
}


} // namespace game::data
