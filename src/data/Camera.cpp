#include <cmath>

#include "game/data/Camera.h"


namespace game::data {


void
Camera::turn() noexcept {
  auto const multiplier = movement.multiplier(
    movement.s_turnLeft, movement.s_turnRight);
  auto const radians = multiplier * rotationSpeed;
  turnBy(radians);
}


void
Camera::turnBy(float radians) noexcept {
  auto const c = std::cos(radians);
  auto const s = std::sin(radians);
  direction = vm::vec2{
    c * direction.x - s * direction.y,
    s * direction.x + c * direction.y
  };
  plane = vm::vec2{
    c * plane.x - s * plane.y,
    s * plane.x + c * plane.y
  };
  angle += radians;
}


} // namespace game::data
