#include <cmath>
#include <memory>
#include <utility>
#include <iterator>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "game/logging/log.h"
#include "game/util/Errors.h"
#include "game/util/mainloop.h"
#include "game/util/Settings.h"
#include "game/data/init.h"
#include "game/data/Root.h"
#include "game/data/Session.h"
#include "game/audio/init.h"
#include "game/render/init.h"
#include "game/scenes/init.h"


int main(int argc, char* argv[]) {
  game::util::Settings settings(argc, argv);
  game::logging::init(settings.logLevel());

  std::atexit(SDL_Quit);
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO) < 0) {
    LOG_CRITICAL("SDL_Init failed: " << SDL_GetError());
    return game::util::errors::SDL_INIT_FAILED;
  }
  LOG_INFO("Initialised SDL");
  if (TTF_Init() == -1) {
    LOG_CRITICAL("TTF_Init failed: " << TTF_GetError());
    return game::util::errors::TTF_INIT_FAILED;
  }
  LOG_INFO("Initialised SDL_ttf");
  std::atexit(TTF_Quit);
  if (SDL_CaptureMouse(SDL_TRUE) == -1) {
    LOG_INFO("Mouse capture not supported");
  }
  if (SDL_SetRelativeMouseMode(SDL_TRUE) != 0) {
#ifdef NDEBUG
    SDL_GetError(); // Pop the error.
#else
    LOG_DEBUG("Relative mouse mode not possible: " << SDL_GetError());
#endif
  }
  else {
    LOG_DEBUG("Set relative mouse mode: true");
  }

  game::data::Session session;
  std::unique_ptr<game::data::Root const> gamedata;
  {
    auto const gamedataPath = settings.basedir() / "data/gamedata";
    LOG_INFO("Game data path: " << gamedataPath);
    gamedata = game::data::init(gamedataPath.string());
  }

  std::shared_ptr<game::audio::Effects> sfx;
  {
    auto result = game::audio::init(settings);
    if (result.second) {
      LOG_CRITICAL("Audio init (game::audio::init) failed");
      return result.second;
    }
    sfx = std::move(result.first);
  }

  auto renderer = game::render::init(&settings, gamedata.get());
  LOG_INFO("Initialised renderer");

  {
    game::scenes::Director director(std::getenv("once") != nullptr);
    game::scenes::init(
      &settings, &session, gamedata.get(), renderer, sfx, &director);
    LOG_INFO("Initialised scenes");

    game::util::mainloop(
      // Event handler.
      [&director] (SDL_Event const& event) {
        if (event.type == SDL_QUIT) {
          director.quit();
          return;
        }
        director.atCurrent().handleEvent(event);
      },
      // Whether to continue or not.
      [&director] {
        return !director.hasQuit();
      },
      // Tick.
      [&director] (unsigned frameTime) {
        director.atCurrent().tick(frameTime);
      },
      // Render.
      [&director] (unsigned timestep) {
        director.atCurrent().render(timestep);
      },
      settings.renderTickMS(),
      settings.logicTickMS()
    );
  }

  renderer.reset(); // Close window so we still look responsive to quit event.
  SDL_Delay(350); // Delay so any playing sounds can finish.

  return 0;
}
