#include <algorithm>

#include <utility>

#include <SDL2/SDL_mixer.h>

#include "game/logging/log.h"
#include "game/util/Errors.h"
#include "game/util/Settings.h"

#include "game/audio/Effects.h"
#include "game/audio/EffectId.h"
#include "game/audio/ChannelId.h"


namespace game::audio {


std::pair<std::shared_ptr<Effects>, int>
init(game::util::Settings const& settings) {
  {
    constexpr int flags = 0;
    if (Mix_Init(flags) != flags) {
      LOG_CRITICAL("Mix_Init failed: " << Mix_GetError());
      return {nullptr, game::util::errors::MIX_INIT_FAILED};
    }
    int const numAllocated = Mix_AllocateChannels(ChannelId_count);
    if (numAllocated != ChannelId_count) {
      LOG_CRITICAL("Allocated only " << numAllocated
                   << " mix channels (expected " << ChannelId_count << ')');
      return {nullptr, game::util::errors::MIX_ALLOCATE_CHANNELS_FAILED};
    }
    LOG_INFO("Initialised SDL_mixer");
    std::atexit(Mix_Quit);

    constexpr int frequency = 2 * MIX_DEFAULT_FREQUENCY;
    constexpr Uint16 format = MIX_DEFAULT_FORMAT;
    constexpr int numChannels = 2; // 2 for stereo, 1 for mono.
    constexpr int chunkSize = 1024;
    if (Mix_OpenAudio(frequency, format, numChannels, chunkSize) < 0) {
      LOG_CRITICAL("Mix_OpenAudio failed: " << Mix_GetError());
      return {nullptr, game::util::errors::MIX_OPENAUDIO_FAILED};
    }
  }

  Effects::container_type container{std::size_t(EffectId_count)};

  int error = 0;
  auto const load = [
    &error,
    &container
  ] (EffectId name, std::string const& filepath) {
    Mix_Chunk* chunk = Mix_LoadWAV(filepath.c_str());
    if (!chunk) {
      LOG_ERROR("Unable to load effect: " << filepath);
      error = game::util::errors::MIX_CHUNK_LOAD_FAILED;
      return;
    }
#ifndef NDEBUG
    else {
      LOG_DEBUG("Loaded effect from " << filepath);
    }
#endif
    container[Effects::size_type(name)].reset(chunk);
  };

#define LOAD(name, basename) \
  load(EffectId::name, \
       (settings.basedir() / "sfx/" #basename ".wav").string());

  LOAD(gunshot, gunshot);
  LOAD(gunshotMuted, gunshot-muted);
  Mix_Volume(ChannelId_Base(ChannelId::playerAttackMuted), 0);
  LOAD(click, click);
  LOAD(footstepsNormal, footsteps);
  LOAD(footstepsMuted, footsteps-muted);
  Mix_Volume(ChannelId_Base(ChannelId::playerFootstepsMuted), 0);
  LOAD(roarDeep, roar-deep);
  LOAD(breathing, breathing);
  LOAD(longGrowl, long-growl);
#undef LOAD

  return {std::make_shared<Effects>(std::move(container)), 0};
}


} // namespace game::audio
