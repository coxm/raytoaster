#include <SDL2/SDL_mixer.h>

#include "game/logging/log.h"

#include "game/audio/Effects.h"


namespace game::audio {


int Effects::play(int channel, size_type effect, int repeats) const {
  int const result = Mix_PlayChannel(
    channel, m_container[effect].get(), repeats);
#ifndef NDEBUG
  if (result == -1) {
    LOG_WARN("Error playing sound: " << Mix_GetError());
  }
#endif
  return result;
}

} // namespace game::audio
