#pragma once
#include <auteur/Scene.h>

#include "./Hooks.h"


union SDL_Event;


namespace game::scenes {


class Scene: public auteur::Scene<Hooks> {
public:
  using auteur::Scene<Hooks>::Scene;
  using id_type = auteur::Scene<Hooks>::id_type;

  inline void handleEvent(SDL_Event const& event) {
    vHandleEvent(event);
  }

private:
  virtual void vHandleEvent(SDL_Event const& event) = 0;
};


} // namespace game::scenes
