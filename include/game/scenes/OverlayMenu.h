#pragma once
#include <memory>

#include "./Scene.h"
#include "./MenuOption.h"


union SDL_Event;
struct SDL_Color;


namespace game::data { class Settings; }
namespace game::audio { class Effects; }
namespace game::render { class OverlayMenuRenderer; }
namespace game::audio { class Effects; }


namespace game::scenes {


class Director;


class OverlayMenu: public Scene {
public:
  template <typename Style, typename... Args>
  inline OverlayMenu(
    std::string name,
    Scene::id_type id,
    game::util::Settings const* settings,
    std::shared_ptr<game::render::OverlayMenuRenderer> renderer,
    std::shared_ptr<game::audio::Effects> sfx,
    Director* director,
    int cursorXOffset,
    int cursorYOffset,
    unsigned cursorFrameIndex,
    Style&& style,
    Args&& ...options
  )
    : Scene(std::move(name), id)
    , m_options{std::forward<Args>(options)...}
    , m_style(std::forward<Style>(style))
    , m_renderer(std::move(renderer))
    , m_sfx(std::move(sfx))
    , m_settings(settings)
    , m_director(director)
    , m_cursorX(0)
    , m_cursorY(0)
    , m_cursorXOffset(cursorXOffset)
    , m_cursorYOffset(cursorYOffset)
    , m_cursorFrameIndex(cursorFrameIndex)
  {
  }

  OverlayMenu(OverlayMenu const&) = delete;
  OverlayMenu(OverlayMenu&&) noexcept = default;
  OverlayMenu& operator=(OverlayMenu const&) = delete;
  OverlayMenu& operator=(OverlayMenu&&) noexcept = default;
  ~OverlayMenu() noexcept = default;

  std::vector<MenuOption>& options() noexcept { return m_options; }
  std::vector<MenuOption> const& options() const noexcept { return m_options; }

  game::render::OverlayMenuRenderer* renderer() const {
    return m_renderer.get();
  }

  void setOption(int index);

private:
  virtual void vHandleEvent(SDL_Event const& event) override;
  virtual void vRender(unsigned ms) override;
  virtual void vStart() override;

  std::vector<MenuOption> m_options;
  std::function<SDL_Color(MenuOption const&, SDL_Color)> m_style;
  std::shared_ptr<game::render::OverlayMenuRenderer> m_renderer;
  std::shared_ptr<game::audio::Effects> m_sfx;
  game::util::Settings const* m_settings;
  Director* m_director;
  int m_cursorX;
  int m_cursorY;
  int m_cursorXOffset;
  int m_cursorYOffset;
  unsigned m_cursorFrameIndex;
  int m_currentOptionIndex = 0;
};


} // namespace game::scenes
