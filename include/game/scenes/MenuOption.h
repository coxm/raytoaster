#pragma once
#include <functional>
#include <string>


namespace game::scenes {


class Director;
class Scene;


struct MenuOption {
  using signature = void(Director*, Scene const*);
  std::string text;
  std::function<signature> action;
  int x;
  int y;
};


} // namespace game::scenes
