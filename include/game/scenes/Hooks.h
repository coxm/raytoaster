#pragma once
#include <string>

#include <auteur/Scene.h>


namespace game::scenes {


/**
 * Auteur hooks for @ref game::scenes::Scene.
 *
 * Make every operation idempotent, plus logging.
 */
struct Hooks {
  static void log(std::string const& scene, char const* operation);

  template <typename Fn>
  inline void init(Fn&& func, auteur::Scene<Hooks> const& scene) {
    if (scene.isNot(scene.s_initialised)) {
      log(scene.name(), "preinit");
      func();
      log(scene.name(), "postinit");
    }
  }

  template <typename Fn>
  inline void deinit(Fn&& func, auteur::Scene<Hooks> const& scene) {
    if (scene.is(scene.s_initialised)) {
      log(scene.name(), "predeinit");
      func();
      log(scene.name(), "postdeinit");
    }
  }

  template <typename Fn>
  inline void start(Fn&& func, auteur::Scene<Hooks> const& scene) {
    if (scene.isNot(scene.s_started)) {
      log(scene.name(), "prestart");
      func();
      log(scene.name(), "poststart");
    }
  }

  template <typename Fn>
  inline void stop(Fn&& func, auteur::Scene<Hooks> const& scene) {
    if (scene.is(scene.s_started)) {
      log(scene.name(), "prestop");
      func();
      log(scene.name(), "poststop");
    }
  }

  template <typename Fn>
  inline void pause(Fn&& func, auteur::Scene<Hooks> const& scene) {
    if (scene.isNot(scene.s_paused)) {
      log(scene.name(), "prepause");
      func();
      log(scene.name(), "postpause");
    }
  }

  template <typename Fn>
  inline void unpause(Fn&& func, auteur::Scene<Hooks> const& scene) {
    if (scene.is(scene.s_paused)) {
      log(scene.name(), "preunpause");
      func();
      log(scene.name(), "postunpause");
    }
  }

  template <typename Fn>
  inline void tick(Fn&& func, auteur::Scene<Hooks> const& scene, unsigned ms) {
    func(ms);
  }
};


} // namespace game::scenes
