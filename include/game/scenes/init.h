#pragma once
#include <memory>

#include "./Director.h"


namespace game::util { class Settings; }
namespace game::data {
  struct Session;
  struct Root;
}
namespace game::render { class Renderer; }
namespace game::audio {
  class Effects;
}


namespace game::scenes {


void
init(
  game::util::Settings* settings,
  game::data::Session* session,
  game::data::Root const* root,
  std::shared_ptr<game::render::Renderer> renderer,
  std::shared_ptr<game::audio::Effects> effects,
  Director* director
);


} // namespace game::scenes
