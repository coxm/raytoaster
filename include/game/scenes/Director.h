#pragma once
#include <auteur/Director.h>

#include "./Scene.h"


namespace game::scenes {


class Director: public auteur::Director<Scene> {
  using base_type = auteur::Director<Scene>;

public:
  using iterator = base_type::iterator;
  using const_iterator = base_type::const_iterator;
  using reference = base_type::reference;
  using const_reference = base_type::const_reference;
  using pointer = base_type::pointer;
  using const_pointer = base_type::const_pointer;
  using transition_type = base_type::transition_type;

  inline Director(bool hasQuit = false)
    : base_type::Director()
    , m_hasQuit(hasQuit)
  {
  }

  inline void push(iterator it) {
    jump(it, transition_type::Type::pause);
  }
  inline void push(std::size_t index) {
    jump(index, transition_type::Type::pause);
  }
  inline void push(std::string const& name) {
    jump(name, transition_type::Type::pause);
  }

  inline void pop() {
    jump(
      previous(),
      transition_type::Type::stop,
      transition_type::Type::unpause);
  }

  void quit() noexcept { m_hasQuit = true; }
  bool hasQuit() const noexcept { return m_hasQuit; }

private:
  bool m_hasQuit;
};


} // namespace game::scenes
