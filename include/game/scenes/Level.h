#pragma once
#include <memory>

#include <SDL2/SDL_ttf.h>

#include "game/data/Progress.h"

#include "./Scene.h"
#include "./Director.h"


union SDL_Event;


namespace game::util { class Settings; }
namespace game::data {
  struct Session;
  struct LevelConfig;
}
namespace game::render { class LevelRenderer; }
namespace game::audio { class Effects; }


namespace game::scenes {


struct GunAnimation {
  float recoil = 0.f;
  unsigned started = 0u;

  inline void reset(unsigned now, float initialValue) noexcept {
    recoil = initialValue;
    started = now;
  }

  void tick(unsigned difference) noexcept;
};


class Level: public Scene {
public:
  Level(
    std::string const& name,
    int id,
    game::util::Settings const* settings,
    game::data::Session* session,
    std::shared_ptr<game::render::LevelRenderer> renderer,
    std::shared_ptr<game::audio::Effects> effects,
    Director* director,
    game::data::LevelConfig const* config
  );

  Level(Level const&) = delete;
  Level(Level&&) noexcept = default;
  Level& operator=(Level const&) = delete;
  Level& operator=(Level&&) noexcept = default;
  ~Level() noexcept = default;

private:
  virtual void vStart() override;
  virtual void vRender(unsigned frameTime) override;
  virtual void vTick(unsigned frameTime) override;
  virtual void vHandleEvent(SDL_Event const& event) override;

#ifndef NDEBUG
  bool handleDebugEvent(SDL_Event const& event);
#endif

  game::util::Settings const* m_settings;
  std::shared_ptr<game::render::LevelRenderer> m_renderer;
  std::shared_ptr<game::audio::Effects> m_sfx;
  game::data::Progress m_progress;
  game::data::LevelConfig const* m_config;
  Director* m_director;
  GunAnimation m_gunAnimation = GunAnimation{};
  int m_breathingVolume = 0;
  unsigned m_msLastTick = 0u;
  bool m_isWalking = false;
};


} // namespace game::scenes
