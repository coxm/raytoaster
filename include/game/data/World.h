#pragma once
#include <cstdint>
#include <cassert>
#include <utility>
#include <vector>


namespace game::data {


class Cell {
public:
  using value_type = std::uint8_t;

  inline Cell() noexcept
    : m_wall(0)
    , m_floor(0)
    , m_ceiling(0)
  {
  }

  inline Cell(value_type wall, value_type floor, value_type ceiling) noexcept
    : m_wall(wall)
    , m_floor(floor)
    , m_ceiling(ceiling)
  {
  }

  inline Cell(Cell const& cell) noexcept = default;
  inline ~Cell() noexcept = default;

  inline void setWall(value_type wall) noexcept { m_wall = wall; }
  inline void setFloor(value_type floor) noexcept { m_floor = floor; }
  inline void setCeiling(value_type ceiling) noexcept { m_ceiling = ceiling; }

  inline bool occupied() const noexcept { return m_wall != 0; }
  inline bool vacant() const noexcept { return m_wall == 0; }

  inline std::pair<value_type, bool> wall() const noexcept {
    return {m_wall - 1, m_wall != 0};
  }
  inline value_type floor() const noexcept { return m_floor; }
  inline value_type ceiling() const noexcept { return m_ceiling; }

  inline value_type rawWall() const noexcept { return m_wall; }
  inline value_type rawFloor() const noexcept { return m_floor; }
  inline value_type rawCeiling() const noexcept { return m_ceiling; }

private:
  value_type m_wall;
  value_type m_floor;
  value_type m_ceiling;
};


class World {
public:
  using value_type = Cell;
  using reference = value_type&;
  using const_reference = value_type const&;
  using container_type = std::vector<value_type>;

  inline World()
    : m_cells()
    , m_width(0)
    , m_height(0)
  {
  }

  inline World(int width, int height, container_type&& data)
    : m_cells(std::move(data))
    , m_width(width)
    , m_height(height)
  {
    assert(
      (m_cells.size() == std::size_t(width * height)) && "Invalid World data");
  }

  World(World const& other)
    : m_cells(other.m_cells)
    , m_width(other.m_width)
    , m_height(other.m_height)
  {
  }

  World(World&& world) noexcept
    : m_cells(std::move(world.m_cells))
    , m_width(std::exchange(world.m_width, 0))
    , m_height(std::exchange(world.m_height, 0))
  {
  }

  inline World& operator=(World const& other) {
    m_cells = std::move(other.m_cells);
    m_width = other.m_width;
    m_height = other.m_height;
    return *this;
  }

  World& operator=(World&& other) noexcept {
    swap(other);
    return *this;
  }

  ~World() noexcept = default;

  void swap(World& other) noexcept {
    std::swap(m_cells, other.m_cells);
    std::swap(m_width, other.m_width);
    std::swap(m_height, other.m_height);
  }

  inline int width() const noexcept { return m_width; }
  inline int height() const noexcept { return m_height; }
  inline int size() const noexcept { return m_width * m_height; }

  inline container_type const& container() const noexcept { return m_cells; }

  template <typename VecT>
  inline reference cell(VecT vec) noexcept {
    return cell(vec.x, vec.y);
  }
  template <typename VecT>
  inline const_reference cell(VecT vec) const noexcept {
    return cell(vec.x, vec.y);
  }

  inline reference cell(unsigned x, unsigned y) noexcept {
    assert((0 <= x && x <  m_width) && "Invalid x-coordinate");
    assert((0 <= y && y < m_height) && "Invalid y-coordinate");
    return m_cells[x + y * m_width];
  }
  inline const_reference cell(unsigned x, unsigned y) const noexcept {
    return m_cells[x + y * m_width];
  }

  inline bool isInRange(unsigned x, unsigned y) const noexcept {
    return 0 <= x && x <= m_width && 0 <= y && y <= m_height;
  }

  inline bool occupied(unsigned x, unsigned y) const noexcept {
    return m_cells[x + y * m_width].occupied();
  }

  inline bool vacant(unsigned x, unsigned y) const noexcept {
    return m_cells[x + y * m_width].vacant();
  }

private:
  std::vector<value_type> m_cells;
  unsigned m_width;
  unsigned m_height;
};


inline void swap(World& a, World& b) {
  a.swap(b);
}


} // namespace game::data
