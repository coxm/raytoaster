#pragma once
#include <vector>

#include "./Atmosphere.h"
#include "./Camera.h"
#include "./Stage.h"
#include "./Enemy.h"


namespace game::data {


struct Progress {
  Camera camera = {};
  Atmosphere atmosphere = {};
  Stage stage = {};
  std::vector<Enemy> enemies = {};
  int ammo = 24;
};


} // namespace game::data
