#pragma once
#include <vm/vec2.hpp>


namespace game::data {


struct StaticCamera {
  vm::vec2 position;
  vm::vec2 direction;
  vm::vec2 plane;
};


} // namespace game::data
