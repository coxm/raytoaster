#pragma once


namespace game::data {


struct SetStage { int value = -1; };

struct SetFogValue { float value = 0.f; };
struct SetFogIncrement { float value = 0.f; };
struct SetFogMin { float value = 0.f; };
struct SetFogMax { float value = 0.f; };

struct SetFogLimitValue { float value = 0.f; };
struct SetFogLimitIncrement { float value = 0.f; };
struct SetFogLimitMin { float value = 0.f; };
struct SetFogLimitMax { float value = 0.f; };

struct SetMyopiaValue { float value = 0.f; };
struct SetMyopiaIncrement { float value = 0.f; };
struct SetMyopiaMin { float value = 0.f; };
struct SetMyopiaMax { float value = 0.f; };


struct ResetFogValue {};
struct ResetFogIncrement {};
struct ResetFogMin {};
struct ResetFogMax {};
struct ResetFogAll {};

struct ResetFogLimitValue {};
struct ResetFogLimitIncrement {};
struct ResetFogLimitMin {};
struct ResetFogLimitMax {};
struct ResetFogLimitAll {};

struct ResetMyopiaValue {};
struct ResetMyopiaIncrement {};
struct ResetMyopiaMin {};
struct ResetMyopiaMax {};
struct ResetMyopiaAll {};

struct ResetAll {};

struct PlaySound {
  std::uint16_t effect = 0;
  std::int8_t channel = -1;
  std::int8_t repeats = 0;
};


// struct PlayMusic { unsigned value = 0; };


} // namespace game::data
