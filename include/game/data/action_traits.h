#pragma once
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>

#include "./actions.h"


namespace game::data {


/// Whether this action affects atmospheric conditions of gameplay.
template <typename Action>
struct is_atmospheric { constexpr static bool value = false; };
template <typename Action>
constexpr bool is_atmospheric_v = is_atmospheric<Action>::value;

/// Whether this action resets a value to the level default.
template <typename Action>
struct is_resetter { constexpr static bool value = false; };
template <typename Action>
constexpr bool is_resetter_v = is_resetter<Action>::value;


#define SET_TRUE(r, TraitsT, T) \
  template <> struct TraitsT<T> { constexpr static bool value = true; };


BOOST_PP_SEQ_FOR_EACH(SET_TRUE, is_atmospheric, BOOST_PP_VARIADIC_TO_SEQ(
  SetFogValue,
  SetFogIncrement,
  SetFogMin,
  SetFogMax,
  SetFogLimitValue,
  SetFogLimitIncrement,
  SetFogLimitMin,
  SetFogLimitMax,
  SetMyopiaValue,
  SetMyopiaIncrement,
  SetMyopiaMin,
  SetMyopiaMax
));


BOOST_PP_SEQ_FOR_EACH(SET_TRUE, is_resetter, BOOST_PP_VARIADIC_TO_SEQ(
  ResetFogValue,
  ResetFogIncrement,
  ResetFogMin,
  ResetFogMax,
  ResetFogAll,
  ResetFogLimitValue,
  ResetFogLimitIncrement,
  ResetFogLimitMin,
  ResetFogLimitMax,
  ResetFogLimitAll,
  ResetMyopiaValue,
  ResetMyopiaIncrement,
  ResetMyopiaMin,
  ResetMyopiaMax,
  ResetMyopiaAll,
  ResetAll
));


} // namespace game::data
