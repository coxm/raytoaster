#pragma once
#include "./World.h"
#include "./Trigger.h"
#include "./Effect.h"
#include "./Progress.h"


namespace game::data {


struct LevelConfig {
  std::vector<Trigger> triggers = {};
  std::vector<Effect> effects = {};
  World world = {};
  Progress progress = {};
};


} // namespace game::data
