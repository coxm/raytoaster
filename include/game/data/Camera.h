#pragma once
#include <utility>

#include <vm/vec2.hpp>

#include "game/util/MoveState.h"

#include "./StaticCamera.h"


namespace game::data {


struct Camera: public StaticCamera {
  float angle;
  float speed;
  float rotationSpeed;
  game::util::MoveState movement;

  inline Camera()
    : StaticCamera{
        vm::vec2{0.f, 0.f},
        vm::vec2{0.f, 0.f},
        vm::vec2{0.f, 0.f}}
    , angle(0.f)
    , speed(0.f)
    , rotationSpeed(0.f)
    , movement()
  {
  }

  inline Camera(
    vm::vec2 position_,
    vm::vec2 direction_,
    vm::vec2 plane_,
    float angle_ = 0.f,
    float speed_ = 0.f,
    float rotationSpeed_ = 0.f,
    game::util::MoveState movement_ = game::util::MoveState()
  )
    noexcept
    : StaticCamera{position_, direction_, plane_}
    , angle(angle_)
    , speed(speed_)
    , rotationSpeed(rotationSpeed_)
    , movement(movement_)
  {
  }

  Camera(Camera const&) noexcept = default;
  Camera(Camera&&) noexcept = default;
  Camera& operator=(Camera const&) noexcept = default;
  Camera& operator=(Camera&&) noexcept = default;
  ~Camera() noexcept = default;

  template <typename EmptyChecker>
  void
  tryWalk(EmptyChecker const& cellIsEmpty) {
    int const mult = movement.multiplier(
      movement.s_moveFwd, movement.s_moveBwd);
    float const newX = position.x + direction.x * mult * speed;
    if (cellIsEmpty(newX, position.y)) {
      position.x = newX;
    }
    float const newY = position.y + direction.y * mult * speed;
    if (cellIsEmpty(position.x, newY)) {
      position.y = newY;
    }
  }

  template <typename EmptyChecker>
  void
  tryStrafe(EmptyChecker const& cellIsEmpty) {
    int const mult = movement.multiplier(
      movement.s_strafeLeft, movement.s_strafeRight);
    float const newX = position.x - direction.y * mult * speed;
    if (cellIsEmpty(newX, position.y)) {
      position.x = newX;
    }
    float const newY = position.y + direction.x * mult * speed;
    if (cellIsEmpty(position.x, newY)) {
      position.y = newY;
    }
  }

  void turn() noexcept;
  void turnBy(float radians) noexcept;
};


} // namespace game::data
