#pragma once
#include <ios>

#include "./FileFormat.h"


namespace game::data {


inline std::ios_base::openmode
getOpenMode(FileFormat format, std::ios_base::openmode base) {
#ifdef game_ARCHIVE_FORMAT_BINARY_ENABLED
  return format == FileFormat::binary ? (base | std::ios_base::binary) : base;
#else
  return base;
#endif
}


} // namespace game::data
