#pragma once
#include <cstdint>

#include <vm/vec2.hpp>


namespace game::data {


struct Enemy {
  vm::vec2 position;
  vm::ivec2 divide;
  int id;
  unsigned frame;
  int yOffset;
};


} // namespace game::data
