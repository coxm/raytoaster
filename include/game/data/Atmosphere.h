#pragma once
#include "game/data/actions.h"
#include "game/data/Variable.h"


namespace game::data {


struct Atmosphere {
  Variable<float> fog = {};
  Variable<float> fogLimit = {};
  Variable<float> fear = {};

  inline void tick() noexcept {
    fog.tick();
    fogLimit.tick();
    fear.tick();
  }

#define APPLY(Action, what) \
  inline void apply(Action instance) noexcept { what = instance.value; }

  APPLY(SetFogValue,            fog.value);
  APPLY(SetFogIncrement,        fog.increment);
  APPLY(SetFogMin,              fog.min);
  APPLY(SetFogMax,              fog.max);
  APPLY(SetFogLimitValue,       fogLimit.value);
  APPLY(SetFogLimitIncrement,   fogLimit.increment);
  APPLY(SetFogLimitMin,         fogLimit.min);
  APPLY(SetFogLimitMax,         fogLimit.max);
  APPLY(SetMyopiaValue,         fear.value);
  APPLY(SetMyopiaIncrement,     fear.increment);
  APPLY(SetMyopiaMin,           fear.min);
  APPLY(SetMyopiaMax,           fear.max);
#undef ASSIGN

#define RESET(Action, what) \
  inline void reset(Action, Atmosphere const& defaults) noexcept { \
    what = defaults.what; \
  }

  RESET(ResetFogValue,          fog.value);
  RESET(ResetFogIncrement,      fog.increment);
  RESET(ResetFogMin,            fog.min);
  RESET(ResetFogMax,            fog.max);
  RESET(ResetFogLimitValue,     fogLimit.value);
  RESET(ResetFogLimitIncrement, fogLimit.increment);
  RESET(ResetFogLimitMin,       fogLimit.min);
  RESET(ResetFogLimitMax,       fogLimit.max);
  RESET(ResetMyopiaValue,       fear.value);
  RESET(ResetMyopiaIncrement,   fear.increment);
  RESET(ResetMyopiaMin,         fear.min);
  RESET(ResetMyopiaMax,         fear.max);
#undef RESET

#define GROUP_RESET(Base, variable) \
  inline void reset(Base ## All, Atmosphere const& defaults) noexcept { \
    reset(Base ## Value{}, defaults); \
    reset(Base ## Min{}, defaults); \
    reset(Base ## Max{}, defaults); \
    reset(Base ## Increment{}, defaults); \
  }

  GROUP_RESET(ResetFog,       fog);
  GROUP_RESET(ResetFogLimit,  fogLimit);
  GROUP_RESET(ResetMyopia,    fear);
#undef GROUP_RESET

  inline void reset(ResetAll, Atmosphere const& defaults) noexcept {
    reset(ResetFogAll(), defaults);
    reset(ResetFogLimitAll(), defaults);
    reset(ResetMyopiaAll(), defaults);
  }
};


} // namespace game::data
