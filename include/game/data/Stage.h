#pragma once
#include <vector>

#include "./Trigger.h"
#include "./Effect.h"


namespace game::data {


struct Stage {
  std::vector<Trigger> triggers = {};
  int id = -1;

  template <typename TriggerIter>
  void assign(int newId, TriggerIter begin, TriggerIter end) {
    std::vector<Trigger> newTriggers;
    // A stage ID of -1 means the trigger is active everywhere.
    // If newId is -1, we want exactly the triggers with stageId -1.
    // Otherwise, we want those with  stageId equal to either -1 or newID.
    for (; begin != end; ++begin) {
      if (begin->stageId == -1 || begin->stageId == newId) {
        newTriggers.push_back(*begin);
      }
    }
    std::swap(triggers, newTriggers);
    id = newId;
  }

  template <typename Visitor>
  void effectTriggers(
    Visitor const& visitor,
    vm::vec2 position,
    Effect const* effects
  ) {
    for (auto& trigger: triggers) {
      if (trigger.useCount >= trigger.maxUseCount) {
        continue;
      }
      if (!trigger.area.contains(position)) {
        continue;
      }
      auto const* effect = effects + trigger.effectBegin;
      auto const* const end = effects + trigger.effectEnd;
      for (; effect != end; ++effect) {
        std::visit(visitor, effect->action);
      }
      ++trigger.useCount;
    }
  }
};


} // namespace game::data
