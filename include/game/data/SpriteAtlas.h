#pragma once
#include <vector>

#include <SDL2/SDL_rect.h>


namespace game::data {


struct SpriteAtlas {
  struct FrameKey {
    unsigned bullet;
    unsigned pistol;
    unsigned machinegun;
    unsigned hudBackground;
  };

  std::vector<SDL_Rect> frames = {};
  FrameKey key;
};


} // namespace game::data
