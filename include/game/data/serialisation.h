#pragma once
/**
 * @file serialise.h
 *
 * Serialisation functions for the gamedata and savedata trees.
 */
#include <cassert>
#include <algorithm>
#include <optional>

#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/variant.hpp>

#include <SDL2/SDL_rect.h>

#include <vm/vec2.hpp>

#include "game/util/MoveState.h"

#include "./macros.h"
#include "./Root.h"


namespace cereal { // Custom Cereal serialisers for standard objects.


template <typename Archive, typename T>
void load(Archive& archive, std::optional<T>& opt) {
  bool exists;
  archive(exists);
  if (exists) {
    archive(*opt);
  }
}


template <typename Archive, typename T>
void save(Archive& archive, std::optional<T> const& opt) {
  bool const exists{opt};
  archive(exists);
  if (exists) {
    archive(*opt);
  }
}


template <typename Archive, typename A, typename B>
void load(Archive& archive, std::pair<A, B>& pair) {
  archive(
    cereal::make_nvp("first", pair.first),
    cereal::make_nvp("second", pair.second)
  );
}


template <typename Archive, typename A, typename B>
void save(Archive& archive, std::pair<A, B> const& pair) {
  archive(
    cereal::make_nvp("first", pair.first),
    cereal::make_nvp("second", pair.second)
  );
}


game_CEREALISE_EXTERNAL_AUTONAME(SDL_Rect, x, y, w, h);


template <typename Archive, typename Coordinate>
void save(Archive& archive, vm::Vec2T<Coordinate> v) {
  archive(cereal::make_nvp("x", v.x), cereal::make_nvp("y", v.y));
}


template <typename Archive, typename Coordinate>
void load(Archive& archive, vm::Vec2T<Coordinate>& v) {
  archive(cereal::make_nvp("x", v.x), cereal::make_nvp("y", v.y));
}


}


namespace game::data { // Cereal serialisers for our objects.


template <typename Archive>
void load(Archive& archive, World& world) {
  int width;
  int height;
  World::container_type cells;
  archive(
    cereal::make_nvp("width", width),
    cereal::make_nvp("height", height),
    cereal::make_nvp("cells", cells));
  world = World(width, height, std::move(cells));
}


template <typename Archive>
void save(Archive& archive, World const& world) {
  archive(
    cereal::make_nvp("width", world.width()),
    cereal::make_nvp("height", world.height()),
    cereal::make_nvp("cells", world.container()));
}


template <typename Archive>
void load(Archive& archive, Cell& cell) {
  std::uint8_t wall, floor, ceiling;
  archive(
    cereal::make_nvp("wall", wall),
    cereal::make_nvp("floor", floor),
    cereal::make_nvp("ceiling", ceiling));
  cell = Cell(wall, floor, ceiling);
}


template <typename Archive>
void save(Archive& archive, Cell const& cell) {
  auto const wall = cell.rawWall();
  auto const floor = cell.rawFloor();
  auto const ceiling = cell.rawCeiling();
  archive(
    cereal::make_nvp("wall", wall),
    cereal::make_nvp("floor", floor),
    cereal::make_nvp("ceiling", ceiling));
}


template <typename Archive>
void load(Archive& archive, Circle& circle) {
  float x, y, r;
  archive(
    cereal::make_nvp("x", x),
    cereal::make_nvp("y", y),
    cereal::make_nvp("r", r));
  circle = Circle(x, y, r);
}


template <typename Archive>
void save(Archive& archive, Circle const& circle) {
  archive(
    cereal::make_nvp("x", circle.x()),
    cereal::make_nvp("y", circle.y()),
    cereal::make_nvp("r", circle.r()));
}


template <typename Archive, typename T>
void load(Archive& archive, Variable<T>& var) {
  archive(
    cereal::make_nvp("value", var.value),
    cereal::make_nvp("min", var.min),
    cereal::make_nvp("max", var.max),
    cereal::make_nvp("increment", var.increment));
}


template <typename Archive, typename T>
void save(Archive& archive, Variable<T> const& var) {
  archive(
    cereal::make_nvp("value", var.value),
    cereal::make_nvp("min", var.min),
    cereal::make_nvp("max", var.max),
    cereal::make_nvp("increment", var.increment));
}


template <typename Archive>
void load(Archive& archive, Camera& camera) {
  constexpr float plane = 0.66f;
  constexpr float fov = -1.f;
  constexpr float speed = 0.15f;
  constexpr float turnSpeed = 0.1f;
  float x, y, angle;
  archive(
    cereal::make_nvp("x", x),
    cereal::make_nvp("y", y),
    cereal::make_nvp("angle", angle));
  camera = game::data::Camera{
    vm::vec2{x, y},
    vm::vec2{fov, 0},
    vm::vec2{0, plane},
    0.f,
    speed,
    turnSpeed,
    game::util::MoveState()
  };
  camera.turnBy(angle);
}


template <typename Archive>
void save(Archive& archive, Camera const& camera) {
  archive(
    cereal::make_nvp("x", camera.position.x),
    cereal::make_nvp("y", camera.position.y),
    cereal::make_nvp("angle", camera.angle));
}


template <typename Archive>
void save(Archive& archive, LevelConfig const& levelConfig) {
  archive(
    cereal::make_nvp("triggers", levelConfig.triggers),
    cereal::make_nvp("effects", levelConfig.effects),
    cereal::make_nvp("world", levelConfig.world),
    cereal::make_nvp("progress", levelConfig.progress));
}


template <typename Archive>
void load(Archive& archive, LevelConfig& levelConfig) {
  archive(
    cereal::make_nvp("triggers", levelConfig.triggers),
    cereal::make_nvp("effects", levelConfig.effects),
    cereal::make_nvp("world", levelConfig.world),
    cereal::make_nvp("progress", levelConfig.progress));
  levelConfig.progress.stage.assign(
    -1, std::begin(levelConfig.triggers), std::end(levelConfig.triggers));
}


game_CEREALISE_EXTERNAL_AUTONAME(Root, spriteAtlas, levels);
game_CEREALISE_EXTERNAL_AUTONAME(SpriteAtlas, frames, key);
game_CEREALISE_EXTERNAL_AUTONAME(SpriteAtlas::FrameKey,
  bullet,
  pistol,
  machinegun,
  hudBackground
);
game_CEREALISE_EXTERNAL_AUTONAME(Progress,
  camera,
  atmosphere,
  stage,
  enemies
);
game_CEREALISE_EXTERNAL_AUTONAME(Stage, triggers, id);
game_CEREALISE_EXTERNAL_AUTONAME(Atmosphere, fog, fogLimit, fear);
game_CEREALISE_EXTERNAL_AUTONAME(Trigger,
  area, stageId, effectBegin, effectEnd, maxUseCount);
// game_CEREALISE_EXTERNAL_AUTONAME(AABB, min, max);
game_CEREALISE_EXTERNAL_AUTONAME(Effect, action);
game_CEREALISE_EXTERNAL_AUTONAME(Enemy, position, divide, id, frame, yOffset);

// Trigger actions.
game_CEREALISE_EXTERNAL_AUTONAME(SetStage, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetFogValue, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetFogIncrement, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetFogMin, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetFogMax, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetFogLimitValue, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetFogLimitIncrement, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetFogLimitMin, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetFogLimitMax, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetMyopiaValue, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetMyopiaIncrement, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetMyopiaMin, value);
game_CEREALISE_EXTERNAL_AUTONAME(SetMyopiaMax, value);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogValue);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogIncrement);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogMin);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogMax);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogAll);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogLimitValue);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogLimitIncrement);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogLimitMin);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogLimitMax);
game_CEREALISE_EXTERNAL_EMPTY(ResetFogLimitAll);
game_CEREALISE_EXTERNAL_EMPTY(ResetMyopiaValue);
game_CEREALISE_EXTERNAL_EMPTY(ResetMyopiaIncrement);
game_CEREALISE_EXTERNAL_EMPTY(ResetMyopiaMin);
game_CEREALISE_EXTERNAL_EMPTY(ResetMyopiaMax);
game_CEREALISE_EXTERNAL_EMPTY(ResetMyopiaAll);
game_CEREALISE_EXTERNAL_EMPTY(ResetAll);
game_CEREALISE_EXTERNAL_AUTONAME(PlaySound, effect, channel);


} // namespace game::data
