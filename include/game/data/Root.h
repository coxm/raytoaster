#pragma once
#include <vector>

#include "./SpriteAtlas.h"
#include "./LevelConfig.h"


namespace game::data {


struct Root {
  SpriteAtlas spriteAtlas = {};
  std::vector<LevelConfig> levels = {};
};


} // namespace game::data
