#pragma once
#include <type_traits>
#include <istream>
#include <ostream>

#ifdef game_ARCHIVE_FORMAT_XML_ENABLED
# include <cereal/archives/xml.hpp>
#endif
#ifdef game_ARCHIVE_FORMAT_JSON_ENABLED
# include <cereal/archives/json.hpp>
#endif
#ifdef game_ARCHIVE_FORMAT_YAML_ENABLED
# include <cereal-yaml/archives/yaml.hpp>
#endif
#ifdef game_ARCHIVE_FORMAT_BINARY_ENABLED
# include <cereal/archives/binary.hpp>
#endif


/**
 * Traits class for cereal archives.
 *
 * @tparam IsInputStream std::true_type or std::false_type depending on whether
 * we're using an input or output archive.
 */
template <typename IsInputStream>
struct ArchiveTraits;


/// ArchiveTraits specialisation for input streams.
template <>
struct ArchiveTraits<std::true_type> {
#ifdef game_ARCHIVE_FORMAT_XML_ENABLED
  using xml_archive = cereal::XMLInputArchive;
#endif
#ifdef game_ARCHIVE_FORMAT_JSON_ENABLED
  using json_archive = cereal::JSONInputArchive;
#endif
#ifdef game_ARCHIVE_FORMAT_YAML_ENABLED
  using yaml_archive = cereal::YAMLInputArchive;
#endif
#ifdef game_ARCHIVE_FORMAT_BINARY_ENABLED
  using binary_archive = cereal::BinaryInputArchive;
#endif
  using stream_type = std::istream;
};


/// ArchiveTraits specialisation for output streams.
template <>
struct ArchiveTraits<std::false_type> {
#ifdef game_ARCHIVE_FORMAT_XML_ENABLED
  using xml_archive = cereal::XMLOutputArchive;
#endif
#ifdef game_ARCHIVE_FORMAT_JSON_ENABLED
  using json_archive = cereal::JSONOutputArchive;
#endif
#ifdef game_ARCHIVE_FORMAT_YAML_ENABLED
  using yaml_archive = cereal::YAMLOutputArchive;
#endif
#ifdef game_ARCHIVE_FORMAT_BINARY_ENABLED
  using binary_archive = cereal::BinaryOutputArchive;
#endif
  using stream_type = std::ostream;
};
