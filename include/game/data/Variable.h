#pragma once
#include <algorithm>


namespace game::data {


template <typename T>
struct Variable {
  T value = {};
  T min = {};
  T max = {};
  T increment = {};

  inline void tick() noexcept {
    setBounded(value + increment);
  }

  inline void setBounded(T newValue) noexcept {
    value = std::clamp(newValue, min, max);
  }

  inline void incrementBounded(T amount) noexcept {
    setBounded(value + amount);
  }
};


} // namespace game::data
