#pragma once
#include <variant>

#include "./actions.h"


namespace game::data {


using Action = std::variant<
  SetStage,

  SetFogValue,
  SetFogIncrement,
  SetFogMin,
  SetFogMax,
  SetFogLimitValue,
  SetFogLimitIncrement,
  SetFogLimitMin,
  SetFogLimitMax,
  SetMyopiaValue,
  SetMyopiaIncrement,
  SetMyopiaMin,
  SetMyopiaMax,

  ResetFogAll,
  ResetFogValue,
  ResetFogIncrement,
  ResetFogMin,
  ResetFogMax,
  ResetFogLimitAll,
  ResetFogLimitValue,
  ResetFogLimitIncrement,
  ResetFogLimitMin,
  ResetFogLimitMax,
  ResetMyopiaAll,
  ResetMyopiaValue,
  ResetMyopiaIncrement,
  ResetMyopiaMin,
  ResetMyopiaMax,
  ResetAll,

  PlaySound
>;


struct Effect {
  Action action = {};
};


} // namespace game::data
