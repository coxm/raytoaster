#pragma once
#include <vm/vec2.hpp>


namespace game::data {


struct AABB {
  vm::vec2 min = {0, 0};
  vm::vec2 max = {0, 0};

  template <typename Vec2>
  inline bool contains(Vec2 const& v) const noexcept {
    return contains(v.x, v.y);
  }

  inline bool contains(float x, float y) const noexcept {
    return min.x <= point.x && point.x <= max.x
        && min.y <= point.y && point.y <= max.y;
  }
};


} // namespace game::data
