#pragma once
#include "game/logging/log.h"

#include "./ArchiveTraits.h"


namespace game::data {


/**
 * Either load or save data, depending on the archive traits.
 *
 * @tparam DataT the data type to process.
 * @tparam ArchiveTraitsT a traits class for the relevant stream type.
 *
 * @param data the data to load/save.
 * @param stream an input or output stream.
 * @param format the serialisation format.
 * @param traits the traits instance.
 */
template <typename DataT, typename ArchiveTraitsT>
bool archiveData(
  DataT& data,
  typename ArchiveTraitsT::stream_type& stream,
  FileFormat format,
  ArchiveTraitsT traits
) {
  if (!stream.good()) {
    LOG_CRITICAL("I/O unavailable for stream");
    return false;
  }

  switch (format) {
#if defined (game_ARCHIVE_FORMAT_JSON_ENABLED) || defined (game_DATA_CONVERTER_TOOL)
    case FileFormat::json:
      { typename ArchiveTraitsT::json_archive{stream}(data); }
      break;
#endif

#if defined (game_ARCHIVE_FORMAT_BINARY_ENABLED) || defined (game_DATA_CONVERTER_TOOL)
    case FileFormat::binary:
      { typename ArchiveTraitsT::binary_archive{stream}(data); }
      break;
#endif

    default:
      LOG_ERROR("Unknown format: " << FileFormat_Base(format));
      return false;
  }

  return true;
}


/**
 * Load or save data.
 *
 * Like archiveData above, only the traits type is automatically deduced,
 * provided the stream is exactly one of std::istream, std::ostream.
 */
template <typename DataT, typename Stream>
inline std::enable_if_t<
  std::is_base_of_v<std::ostream, Stream> xor
    std::is_base_of_v<std::istream, Stream>,
  int
>
archiveData(
  DataT& data,
  Stream& stream,
  FileFormat format
) {
  using Traits = ArchiveTraits<
    std::bool_constant<std::is_base_of_v<std::istream, Stream>>
  >;
  return archiveData(data, stream, format, Traits());
}


} // namespace game::data
