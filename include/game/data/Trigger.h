#pragma once
#include "./Circle.h"


namespace game::data {


struct Trigger {
  Circle area = {};
  int stageId = -1;
  unsigned effectBegin = 0;
  unsigned effectEnd = 0;
  int maxUseCount = 1;
  int useCount = 0;
};


} // namespace game::data
