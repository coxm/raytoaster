#pragma once


namespace game::data {


class Circle {
public:
  Circle(float x = 0, float y = 0, float r = 0) noexcept
    : m_x(x)
    , m_y(y)
    , m_r2(r * r)
    , m_r(r)
  {
  }

  template <typename Vec2>
  inline bool contains(Vec2 const& v) const noexcept {
    return contains(v.x, v.y);
  }

  inline bool contains(float x, float y) const noexcept {
    float const dx = m_x - x;
    float const dy = m_y - y;
    return dx * dx + dy * dy < m_r2;
  }

  inline float x() const noexcept { return m_x; }
  inline float y() const noexcept { return m_y; }
  inline float r2() const noexcept { return m_r2; }
  inline float r() const noexcept { return m_r; }

private:
  float m_x;
  float m_y;
  float m_r2;
  float m_r;
};


} // namespace game::data
