#pragma once
#include <type_traits>
#include <string>

#include <boost/preprocessor/punctuation/remove_parens.hpp>
#include <boost/preprocessor/list/transform.hpp>
#include <boost/preprocessor/list/to_tuple.hpp>
#include <boost/preprocessor/variadic/to_list.hpp>

#include <cereal/cereal.hpp>

#include "game/logging/log.h"


// Convert a list element to an NVP.
// `elem` -> `cereal::make_nvp("elem", elem)`
#define game_MAKE_CEREAL_NVP(d, data, elem) cereal::make_nvp(#elem, elem)

// Convert a list element to an NVP, but accessing from an owner.
// `elem` -> `cereal::make_nvp("elem", owner.elem)`
#define game_MAKE_OWNED_CEREAL_NVP(d, data, elem) \
  cereal::make_nvp(#elem, data.elem)


// Deduce Name-Value Pairs for variadic args.
// `a, b` -> `cereal::make_nvp("a", a), cereal::make_nvp("b", b)`
#define game_CEREAL_AUTO_NVP(...) BOOST_PP_REMOVE_PARENS( \
  BOOST_PP_LIST_TO_TUPLE( \
    BOOST_PP_LIST_TRANSFORM( \
      game_MAKE_CEREAL_NVP, \
      _, \
      BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__))))


// Deduce Name-Value Pairs for owned variadic args.
// `obj, a, b` -> `cereal::make_nvp("a", obj.a), cereal::make_nvp("b", obj.b)`
#define game_CEREAL_AUTO_NVP_OWNED(owner, ...) BOOST_PP_REMOVE_PARENS( \
  BOOST_PP_LIST_TO_TUPLE( \
    BOOST_PP_LIST_TRANSFORM( \
      game_MAKE_OWNED_CEREAL_NVP, \
      owner, \
      BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__))))


// Define load and save methods for a class/struct, given a variadict list of
// properties.
#define game_CEREALISE_INTERNAL_NAMED(...) \
  template <typename Archive> \
  void load(Archive& archive) { \
    archive(__VA_ARGS__); \
  } \
\
  template <typename Archive> \
  void save(Archive& archive) const { \
    archive(__VA_ARGS__); \
  }


#define game_CEREALISE_EXTERNAL_NAMED_WITH_VAR(Type, varname, ...) \
  template <typename Archive> \
  void load(Archive& archive, Type& varname) { \
    archive(__VA_ARGS__); \
  } \
\
  template <typename Archive> \
  void save(Archive& archive, Type const& varname) { \
    archive(__VA_ARGS__); \
  }


#define game_CEREALISE_EXTERNAL_NAMED(Type, ...) \
  game_CEREALISE_EXTERNAL_NAMED_WITH_VAR(Type, instance)


// Define load and save methods for a class/struct, given property names as
// variadic args. They will automatically be given the same name if the
// serialisation format allows.
#define game_CEREALISE_INTERNAL_AUTONAME(...) \
  game_CEREALISE_INTERNAL_NAMED( \
    game_CEREAL_AUTO_NVP(__VA_ARGS__))


// Define external load and save functions for a type, given a variadic list of
// properties.
#define game_CEREALISE_EXTERNAL_AUTONAME(Type, ...) \
  game_CEREALISE_EXTERNAL_NAMED_WITH_VAR(Type, instance, \
    game_CEREAL_AUTO_NVP_OWNED(instance, __VA_ARGS__))


#define game_CEREALISE_EXTERNAL_EMPTY(Type) \
  template <typename Archive> void load(Archive&, Type&) {} \
  template <typename Archive> void save(Archive&, Type const&) {}
