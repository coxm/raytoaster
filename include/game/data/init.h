#pragma once
#include <memory>
#include <string>


namespace game::data {


struct Root;


std::unique_ptr<Root>
init(std::string const& filepath);


} // namespace game::data
