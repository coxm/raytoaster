#pragma once
#include <cassert>
#include <string_view>


namespace game::data {


using FileFormat_Base = unsigned char;


enum class FileFormat: FileFormat_Base {
  unknown = 0,
  json,
  binary,
};
constexpr FileFormat_Base FileFormat_count = 3;


constexpr char const* FileFormat_names[FileFormat_count] = {
  "unknown"
, "json"
, "binary"
};


inline char const* name_FileFormat(FileFormat format) {
  auto const index = static_cast<FileFormat_Base>(format);
  assert((0 <= index && index < FileFormat_count) && "Bad format");
  return FileFormat_names[index];
}


constexpr FileFormat
parseFormat(std::string_view const format) {
  if (format == "json") {
    return FileFormat::json;
  }
  if (format == "bin" || format == "binary") {
    return FileFormat::binary;
  }
  return FileFormat::unknown;
}


constexpr FileFormat
suggestFormat(std::string_view const filepath) {
  auto const extensionIndex = filepath.rfind('.');
  return extensionIndex == std::string_view::npos
    ? FileFormat::unknown
    : parseFormat(filepath.substr(extensionIndex + 1));
}


} // namespace game::data
