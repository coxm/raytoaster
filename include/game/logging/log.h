#pragma once
#include <iostream>


#define game_LOG_LEVEL_TRACE    0
#define game_LOG_LEVEL_DEBUG    1
#define game_LOG_LEVEL_INFO     2
#define game_LOG_LEVEL_WARN     3
#define game_LOG_LEVEL_ERROR    4
#define game_LOG_LEVEL_FATAL    5


#ifdef game_LOG_COLOURS
# define ANSI_RESET       "\033[0m"
# define ANSI_BLACK       "\033[30m"
# define ANSI_RED         "\033[31m"
# define ANSI_GREEN       "\033[32m"
# define ANSI_YELLOW      "\033[33m"
# define ANSI_BLUE        "\033[34m"
# define ANSI_MAGENTA     "\033[35m"
# define ANSI_CYAN        "\033[36m"
# define ANSI_WHITE       "\033[37m"
# define ANSI_BOLDBLACK   "\033[1m\033[30m"
# define ANSI_BOLDRED     "\033[1m\033[31m"
# define ANSI_BOLDGREEN   "\033[1m\033[32m"
# define ANSI_BOLDYELLOW  "\033[1m\033[33m"
# define ANSI_BOLDBLUE    "\033[1m\033[34m"
# define ANSI_BOLDMAGENTA "\033[1m\033[35m"
# define ANSI_BOLDCYAN    "\033[1m\033[36m"
# define ANSI_BOLDWHITE   "\033[1m\033[37m"
# define ANSI_BOLD        "\033[01m"
# define ANSI_DARKGREY    "\033[02m"
# define ANSI_ITALIC      "\033[03m"
#else
# define ANSI_RESET       ""
# define ANSI_BLACK       ""
# define ANSI_RED         ""
# define ANSI_GREEN       ""
# define ANSI_YELLOW      ""
# define ANSI_BLUE        ""
# define ANSI_MAGENTA     ""
# define ANSI_CYAN        ""
# define ANSI_WHITE       ""
# define ANSI_BOLDBLACK   ""
# define ANSI_BOLDRED     ""
# define ANSI_BOLDGREEN   ""
# define ANSI_BOLDYELLOW  ""
# define ANSI_BOLDBLUE    ""
# define ANSI_BOLDMAGENTA ""
# define ANSI_BOLDCYAN    ""
# define ANSI_BOLDWHITE   ""
# define ANSI_BOLD        ""
# define ANSI_DARKGREY    ""
# define ANSI_ITALIC      ""
#endif


/**
 * game_DEFAULT_LOG_LEVEL: the default log level set, if none specified.
 *
 * Either "info" (if NDEBUG is defined), or "trace" (i.e. log everything).
 */
#ifdef NDEBUG
# define game_DEFAULT_LOG_LEVEL game_LOG_LEVEL_INFO
#else
# define game_DEFAULT_LOG_LEVEL game_LOG_LEVEL_TRACE
#endif


/**
 * game_ACTIVE_LOG_LEVEL: the actual compile-time log level.
 *
 * This can be overridden by providing a compile definition.
 */
#ifndef game_ACTIVE_LOG_LEVEL
# define game_ACTIVE_LOG_LEVEL game_DEFAULT_LOG_LEVEL
#endif


/// Get a reference to the global logging object.
#define LOGGER(level) (std::cout \
    << game::logging::now() \
    << game::logging::levelnames::level)


#define LOG_END ANSI_RESET << std::endl


/// Do nothing.
#define LOG_NOTHING(...) while(false){}


/// LOG_TRACE
#if (game_ACTIVE_LOG_LEVEL <= game_LOG_LEVEL_TRACE)
# define LOG_TRACE(...) (LOGGER(TRACE) << __VA_ARGS__ << LOG_END)
#else
# define LOG_TRACE LOG_NOTHING
#endif


/// LOG_DEBUG
#if (game_ACTIVE_LOG_LEVEL <= game_LOG_LEVEL_DEBUG)
# define LOG_DEBUG(...) (LOGGER(DEBUG) << __VA_ARGS__ << LOG_END)
#else
# define LOG_DEBUG LOG_NOTHING
#endif


/// LOG_INFO
#if (game_ACTIVE_LOG_LEVEL <= game_LOG_LEVEL_INFO)
# define LOG_INFO(...) (LOGGER(INFO) << __VA_ARGS__ << LOG_END)
#else
# define LOG_INFO LOG_NOTHING
#endif


/// LOG_WARN
#if (game_ACTIVE_LOG_LEVEL <= game_LOG_LEVEL_WARN)
# define LOG_WARN(...) (LOGGER(WARN) << __VA_ARGS__ << LOG_END)
#else
# define LOG_WARN LOG_NOTHING
#endif


/// LOG_ERROR
#if (game_ACTIVE_LOG_LEVEL <= game_LOG_LEVEL_ERROR)
# define LOG_ERROR(...) (LOGGER(ERROR) << __VA_ARGS__ << LOG_END)
#else
# define LOG_ERROR LOG_NOTHING
#endif


/// LOG_FATAL
#if (game_ACTIVE_LOG_LEVEL <= game_LOG_LEVEL_FATAL)
# define LOG_FATAL(...) (LOGGER(FATAL) << __VA_ARGS__ << LOG_END)
#else
# define LOG_FATAL LOG_NOTHING
#endif

#define LOG_CRITICAL(...) LOG_FATAL(__VA_ARGS__)


namespace game::logging {


namespace levelcolours {
  constexpr char const* TRACE = ANSI_DARKGREY;
  constexpr char const* DEBUG = ANSI_WHITE;
  constexpr char const* INFO = ANSI_GREEN;
  constexpr char const* WARN = ANSI_YELLOW;
  constexpr char const* ERROR = ANSI_RED;
  constexpr char const* FATAL = ANSI_BOLDRED;
} // namespace levelcolours


namespace levelnames {
  constexpr char const* TRACE = "[" ANSI_WHITE    "TRACE" ANSI_RESET "] ";
  constexpr char const* DEBUG = "[" ANSI_CYAN     "DEBUG" ANSI_RESET "] ";
  constexpr char const* INFO =  "[" ANSI_GREEN    "INFO"  ANSI_RESET "] ";
  constexpr char const* WARN =  "[" ANSI_YELLOW   "WARN"  ANSI_RESET "] ";
  constexpr char const* ERROR = "[" ANSI_RED      "ERROR" ANSI_RESET "] ";
  constexpr char const* FATAL = "[" ANSI_BOLDRED  "FATAL" ANSI_RESET "] ";
} // namespace prefixes


inline void init(unsigned level) {}


std::string now();


} // namespace game::logging
