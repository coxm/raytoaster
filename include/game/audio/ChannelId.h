#pragma once
namespace game::audio {


using ChannelId_Base = int;
enum class ChannelId: ChannelId_Base {
  whatever = -1,
  playerAttackNormal = 0,
  playerAttackMuted,
  playerFootstepsNormal,
  playerFootstepsMuted,
  breathing,
  monsters,
  _COUNT
};
constexpr ChannelId_Base ChannelId_count = int(ChannelId::_COUNT);


} // namespace game::audio
