#pragma once
struct Mix_Chunk;
struct _Mix_Music;


extern "C" {
  void Mix_FreeChunk(Mix_Chunk* chunk);
  void Mix_FreeMusic(_Mix_Music* music);
}


namespace game::audio {


struct MixChunkDeleter {
  inline void operator()(Mix_Chunk* chunk) const noexcept {
    Mix_FreeChunk(chunk);
  }
};


struct MixMusicDeleter {
  inline void operator()(_Mix_Music* music) const noexcept {
    Mix_FreeMusic(music);
  }
};


} // namespace game::audio
