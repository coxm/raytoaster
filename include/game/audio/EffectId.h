#pragma once
namespace game::audio {


enum class EffectId: unsigned {
  gunshot = 0,
  gunshotMuted,
  click,
  footstepsNormal,
  footstepsMuted,
  roarDeep,
  breathing,
  longGrowl,
  _COUNT
};
constexpr unsigned EffectId_count = unsigned(EffectId::_COUNT);


} // namespace game::audio
