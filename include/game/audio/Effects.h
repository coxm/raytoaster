#pragma once
#include <memory>
#include <vector>

#include "./deleters.h"


struct Mix_Chunk;


namespace game::audio {


enum class EffectId: unsigned;
enum class ChannelId: int;


class Effects {
public:
  using value_type = std::unique_ptr<Mix_Chunk, MixChunkDeleter>;
  using container_type = std::vector<value_type>;
  using size_type = container_type::size_type;

  constexpr static int s_repeatForever = -1;

  template <typename... Args>
  inline Effects(Args&& ...args)
    : m_container{std::forward<Args>(args)...}
  {
  }

  container_type& container() noexcept { return m_container; }
  container_type const& container() const noexcept { return m_container; }

  inline int play(ChannelId channel, EffectId effect, int repeats = 0) const {
    return play(int(channel), size_type(effect), repeats);
  }
  int play(int channel, size_type effect, int repeats = 0) const;

  inline Mix_Chunk* get(EffectId id) const { return get(int(id)); }
  inline Mix_Chunk* get(size_type index) const {
    return m_container[index].get();
  }

private:
  container_type m_container;
};


} // namespace game::audio
