#pragma once
#include <memory>


namespace game::util { class Settings; }


namespace game::audio {


class Effects;


std::pair<std::shared_ptr<Effects>, int>
init(game::util::Settings const& settings);


} // namespace game::audio
