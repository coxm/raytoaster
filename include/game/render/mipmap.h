#pragma once
#include <utility>
#include <memory>

#include "./deleters.h"


struct SDL_Rect;


namespace game::render {


std::pair<std::unique_ptr<SDL_Surface, SDLSurfaceDeleter>, unsigned>
createMipMap(
  SDL_Surface* src,
  SDL_Rect const& srcRect,
  unsigned maxCount = 32);


void
getMipMapCoords(
  SDL_Rect* coords,
  int origW,
  int origH,
  unsigned index);


unsigned
maxMipMapLevel(int width, int height);


} // namespace game::render
