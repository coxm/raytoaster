#pragma once
#include <cstddef>


struct SDL_Rect;
struct SDL_Texture;


extern "C" {
  void SDL_UnlockTexture(SDL_Texture*);
}


namespace game::render {


class TextureLock {
public:
  inline TextureLock(SDL_Texture* texture, SDL_Rect const& rect) noexcept
    : TextureLock(texture, &rect)
  {
  }

  inline TextureLock(SDL_Texture* texture, SDL_Rect const* rect = nullptr)
    noexcept
    : m_texture(texture)
    , m_pixels(nullptr)
    , m_pitch(0)
    , m_width(0)
  {
    doLock(texture, rect);
  }

  TextureLock(TextureLock const&) = delete;
  TextureLock(TextureLock&&) = delete;
  TextureLock& operator=(TextureLock const&) = delete;
  TextureLock& operator=(TextureLock&&) = delete;

  inline ~TextureLock() noexcept {
    SDL_UnlockTexture(m_texture);
  }

  inline void clear() const;

  inline std::byte* bytes() const noexcept {
    return static_cast<std::byte*>(m_pixels);
  }

  inline std::byte* bytes(int x, int y) const noexcept {
    return bytes() + x * sizeof(Uint32) + y * m_pitch;
  }

  inline Uint32* pixels32() const noexcept {
    return static_cast<Uint32*>(m_pixels);
  }

  inline Uint32* pixels32(int x, int y) const noexcept {
    return pixels32() + x + y * m_width;
  }

  inline void set(int x, int y, Uint32 colour) const noexcept {
    *pixels32(x, y) = colour;
  }

  inline void* data() const noexcept { return m_pixels; }
  inline int pitch() const noexcept { return m_pitch; }
  inline int width() const noexcept { return m_width; }

private:
  void doLock(SDL_Texture*, SDL_Rect const* rect);

  SDL_Texture* m_texture;
  void* m_pixels;
  int m_pitch;
  int m_width;
};


} // namespace game::render
