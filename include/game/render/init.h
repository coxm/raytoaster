#pragma once

#include <memory>


namespace game::util { class Settings; }
namespace game::data { struct Root; }

namespace game::render {


class Renderer;


std::shared_ptr<Renderer>
init(game::util::Settings const* settings, game::data::Root const* gamedata);


} // namespace game::render
