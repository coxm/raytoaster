#pragma once
#include <memory>
#include <string>

#include <SDL2/SDL_pixels.h>

#include "./deleters.h"


struct SDL_Texture;
struct SDL_Rect;
struct _TTF_Font;


namespace game::util { class Settings; }


namespace game::render {


class Renderer;


class HudRenderer {
public:
  HudRenderer(
    game::util::Settings const* settings,
    std::shared_ptr<Renderer> renderer,
    std::shared_ptr<_TTF_Font> const& font,
    char const* ammoText = "Ammo",
    SDL_Colour textColour = SDL_Colour{0xff, 0, 0, 0xff}
  );

  HudRenderer(HudRenderer const&) = delete;
  HudRenderer(HudRenderer&&) noexcept = default;
  HudRenderer& operator=(HudRenderer const&) = delete;
  HudRenderer& operator=(HudRenderer&&) noexcept = default;
  ~HudRenderer() noexcept = default;

  void render() const noexcept;

  void setAmmoText(char const* text);

private:
  std::shared_ptr<_TTF_Font> m_font;
  std::shared_ptr<Renderer> m_renderer;
  game::util::Settings const* m_settings;
  std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_texture;
  SDL_Rect m_ammoTextRegion;
  SDL_Colour m_textColour;
};


} // namespace game::render
