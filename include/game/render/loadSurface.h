#pragma once
#include <memory>

#include "./deleters.h"


struct SDL_Renderer;
struct SDL_PixelFormat;


namespace game::render {


std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter>
loadFormattedSurface(char const* filepath, Uint32 format, bool locked = true);


} // namespace game::render
