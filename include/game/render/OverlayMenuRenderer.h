#pragma once
#include <cstdint>
#include <memory>

#include <SDL2/SDL_pixels.h>

#include "./RenderCapture.h"
#include "./TextureLock.h"
#include "./deleters.h"
#include "./Renderer.h"


struct SDL_Renderer;
struct SDL_Surface;
struct SDL_Texture;
struct _TTF_Font;


extern "C" {
  SDL_Texture* SDL_CreateTextureFromSurface(SDL_Renderer*, SDL_Surface*);
}


namespace game::util { class Settings; }


namespace game::render {


class OverlayMenuRenderer {
public:
  OverlayMenuRenderer(
    Renderer* renderer,
    std::shared_ptr<_TTF_Font> font,
    SDL_Colour fontColour
  );

  OverlayMenuRenderer(OverlayMenuRenderer const&) = delete;
  OverlayMenuRenderer(OverlayMenuRenderer&&) noexcept = default;
  OverlayMenuRenderer& operator=(OverlayMenuRenderer const&) = delete;
  OverlayMenuRenderer& operator=(OverlayMenuRenderer&&) = default;
  ~OverlayMenuRenderer() noexcept = default;

  SDL_Texture* background() const noexcept { return m_background.get(); }
  Renderer* renderer() const noexcept { return m_renderer; }

  void render(int cursorX, int cursorY, int cursorFrameIndex) const;

  template <typename... RenderFn>
  void capture(RenderFn&& ...fn) {
    RenderCapture cap(m_renderer->renderer(), m_background.get());
    (fn(), ...);
  }

  template <typename Iter, typename Style>
  void
  setText(Style const& style, Iter begin, Iter end, int scale = 1) {
    auto canvas = initTextSurface();
    for (; begin != end; ++begin) {
      SDL_Colour const colour = style(*begin, m_fontColour);
      blitText(canvas.get(), begin->text.c_str(), colour,
               begin->x, begin->y, scale);
    }
    m_text.reset(
      SDL_CreateTextureFromSurface(m_renderer->renderer(), canvas.get()));
  }

private:
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> initTextSurface();

  void
  blitText(
    SDL_Surface* surface,
    char const* text,
    SDL_Color colour,
    int x,
    int y,
    int scale = 1
  );

  std::shared_ptr<_TTF_Font> m_font;
  Renderer* m_renderer;
  std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_background;
  std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_text;
  SDL_Colour m_fontColour;
};


} // namespace game::render
