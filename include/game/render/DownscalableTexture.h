#pragma once
#include "./MipMappedTexture.h"


namespace game::render {


class DownscalableTexture {
public:
  constexpr static auto s_noMapLimit = MipMappedTexture::s_noMapLimit;

  DownscalableTexture(int destX = 0, int destY = 0) noexcept
    : m_tex()
    , m_destX(destX)
    , m_destY(destY)
  {
  }

  inline DownscalableTexture(
    SDL_Renderer* renderer,
    SDL_Surface* src,
    int destX = 0, int destY = 0,
    unsigned maxMapped = s_noMapLimit
  )
    : m_tex(renderer, src, maxMapped)
    , m_destX(destX)
    , m_destY(destY)
  {
  }

  inline DownscalableTexture(
    SDL_Renderer* renderer,
    SDL_Surface* src,
    SDL_Rect const& srcRect,
    int destX, int destY,
    unsigned maxMapped = s_noMapLimit
  )
    : m_tex(renderer, src, srcRect, maxMapped)
    , m_destX(destX)
    , m_destY(destY)
  {
  }

  template <typename ...Args>
  inline void reset(Args ...args) {
    m_tex.reset(std::forward<Args>(args)...);
  }

  inline SDL_Texture* get() noexcept { return m_tex.get(); }

  inline void use(unsigned index) noexcept { m_tex.use(index); }
  inline void up() noexcept { m_tex.up(); }
  inline void down() noexcept { m_tex.down(); }

  inline void render(SDL_Renderer* ren) const noexcept {
    SDL_Rect src;
    m_tex.coords(&src);
    SDL_Rect dest{
      m_destX, m_destY,
      m_tex.originalWidth(), m_tex.originalHeight()
    };
    SDL_RenderCopy(ren, m_tex.get(), &src, &dest);
  }

  inline void clear() noexcept { m_tex.clear(); }

private:
  MipMappedTexture m_tex;
  int m_destX;
  int m_destY;
};


} // namespace game::render
