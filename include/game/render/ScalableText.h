#pragma once
#include <memory>
#include <utility>

#include <SDL2/SDL_pixels.h>

#include "./deleters.h"
#include "./DownscalableTexture.h"


struct _TTF_Font;


namespace game::render {


class ScalableText {
public:
  inline ScalableText()
    : m_font{}
    , m_texture{}
    , m_foreground{0, 0, 0, 0}
    , m_background{0, 0, 0, 0}
  {
  }

  ScalableText(
    std::shared_ptr<_TTF_Font> font,
    SDL_Renderer* renderer,
    int destX, int destY,
    SDL_Colour foreground,
    SDL_Colour background = SDL_Colour{0, 0, 0, 0},
    unsigned maxMapped = MipMappedTexture::s_noMapLimit
  );

  inline void
  setFont(std::shared_ptr<_TTF_Font> font) noexcept {
    m_font = font;
  }

  inline void use(unsigned index) noexcept { m_texture.use(index); }

  void solid(SDL_Renderer* renderer, char const* text);
  void shaded(SDL_Renderer* renderer, char const* text);
  void blended(SDL_Renderer* renderer, char const* text);

  void render(SDL_Renderer* renderer) const noexcept;

  inline void clear() noexcept { m_texture.clear(); }

private:
  std::shared_ptr<_TTF_Font> m_font;
  DownscalableTexture m_texture;
  SDL_Colour m_foreground;
  SDL_Colour m_background;
};


} // namespace game::render
