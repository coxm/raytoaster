#pragma once
#include <SDL2/SDL_ttf.h>


namespace game::render {


struct TTFFontDeleter {
  inline void operator()(TTF_Font* font) const noexcept {
    TTF_CloseFont(font);
  }
};


} // namespace game::render
