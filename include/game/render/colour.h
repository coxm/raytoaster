#pragma once
#include <cstdint>

#include <SDL2/SDL_pixels.h>


namespace game::render {


inline SDL_Colour
toColour(std::uint32_t u) noexcept {
  return SDL_Colour{
    Uint8((u & 0xff000000u) >> 24),
    Uint8((u & 0x00ff0000u) >> 16),
    Uint8((u & 0x0000ff00u) >>  8),
    Uint8((u & 0x000000ffu)      )
  };
}


inline SDL_Colour
toColour(SDL_Colour c) noexcept {
  return c;
}


inline std::uint32_t
fromColour(SDL_Colour c) noexcept {
  return (c.r << 24u) | (c.g << 16u) | (c.b <<  8u) | c.a;
}


inline std::uint32_t
fromColour(std::uint32_t u) noexcept {
  return u;
}


} // namespace game::render
