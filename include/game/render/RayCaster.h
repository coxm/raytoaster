#pragma once
#include <memory>
#include <vector>

#include <SDL2/SDL_rect.h>

#include "game/util/Settings.h"

#include "./deleters.h"
#include "./TextureLock.h"


namespace game::data {
  struct SpriteAtlas;
  struct Progress;
  class World;
}


namespace game::render {


class RayCaster {
public:
  RayCaster(
    game::util::Settings const* settings,
    SDL_Surface const* spritesheet,
    game::data::SpriteAtlas const* spriteAtlas);
  RayCaster(RayCaster const&) = delete;
  RayCaster(RayCaster&&) noexcept = default;
  RayCaster& operator=(RayCaster const&) = delete;
  RayCaster& operator=(RayCaster&&) noexcept = default;
  ~RayCaster() noexcept = default;

  /**
   * Render to a screen surface.
   *
   * @note The surface *must* be locked before rendering.
   */
  void
  render(
    TextureLock const& texture,
    game::data::World const& world,
    game::data::Progress const& progress,
    int width,
    int height
  ) noexcept;

  /// Get the ratio of screen width in which sprites are visible.
  float spriteVisibility() const noexcept { return m_spriteVisibility; }

private:
  /// The spritesheet.
  SDL_Surface const* m_spritesheet;
  /// The sprite atlas.
  game::data::SpriteAtlas const* m_spriteAtlas;
  /// A depth buffer for sprite rendering, indexed by pixel column.
  std::unique_ptr<float[]> m_zBuffer;
  /// How many rendered columns are occupied by a monster.
  float m_spriteVisibility;
};


} // namespace game::render
