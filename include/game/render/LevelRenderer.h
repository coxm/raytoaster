#pragma once
#include <memory>

#include <SDL2/SDL_ttf.h>

#include "./deleters.h"
#include "./RayCaster.h"
#include "./ScalableText.h"
// #include "./HudRenderer.h"


struct SDL_Texture;


namespace game::util { class Settings; }
namespace game::data {
  struct Progress;
}


namespace game::render {


class Renderer;


class LevelRenderer {
public:
  LevelRenderer(
    game::util::Settings const* settings,
    std::shared_ptr<Renderer> renderer,
    SDL_Surface const* spritesheet,
    game::data::SpriteAtlas const* spriteAtlas,
    std::shared_ptr<TTF_Font> objectiveFont,
    std::shared_ptr<TTF_Font> hudFont
  );

  LevelRenderer(LevelRenderer const&) = delete;
  LevelRenderer(LevelRenderer&&) noexcept = default;
  LevelRenderer& operator=(LevelRenderer const&) = delete;
  LevelRenderer& operator=(LevelRenderer&&) noexcept = default;
  ~LevelRenderer() noexcept = default;

  void
  render(
    game::data::World const& world,
    game::data::Progress const& progress,
    float gunRecoil
  );

  void setObjectiveText(char const* text);
  inline void clearObjectiveText() { m_objective.clear(); }

  inline RayCaster const& rayCaster() const noexcept { return m_rayCaster; }

  SDL_Rect const& gunDestRect() const noexcept { return m_gunDestRect; }

private:
  RayCaster m_rayCaster;
  game::util::Settings const* m_settings;
  std::shared_ptr<Renderer> m_renderer;
  /// The ray-casted scene texture.
  std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_sceneTex;
  /// The HUD texture.
  // std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_hudTex;
  /// The gun texture.
  std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_gunTex;
  /// The objective text, rendered over the scene texture.
  mutable ScalableText m_objective;
  /// The HUD renderer: renders only the lower portion of the screen.
  // HudRenderer m_hud;
  /// The destination rect for the gun texture, in actual position.
  SDL_Rect m_gunDestRect;
  unsigned m_gunMipMapCount;
};


} // namespace game::render
