#pragma once


struct SDL_Window;
struct SDL_Renderer;
struct SDL_Surface;
struct SDL_Texture;


extern "C" void SDL_DestroyWindow(SDL_Window*);
extern "C" void SDL_DestroyRenderer(SDL_Renderer*);
extern "C" void SDL_FreeSurface(SDL_Surface*);
extern "C" void SDL_DestroyTexture(SDL_Texture*);


namespace game::render {


struct SDLWindowDeleter {
  inline void operator()(SDL_Window* window) const noexcept {
    SDL_DestroyWindow(window);
  }
};


struct SDLRendererDeleter {
  inline void operator()(SDL_Renderer* renderer) const noexcept {
    SDL_DestroyRenderer(renderer);
  }
};


struct SDLSurfaceDeleter {
  inline void operator()(SDL_Surface* surface) const noexcept {
    SDL_FreeSurface(surface);
  }
};


struct SDLTextureDeleter {
  inline void operator()(SDL_Texture* texture) const noexcept {
    SDL_DestroyTexture(texture);
  }
};


} // namespace game::render
