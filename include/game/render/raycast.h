#pragma once
#include <cmath>
#include <cstring>
#include <cassert>

#include <SDL2/SDL_surface.h>

#include "game/util/numeric.h"
#include "game/data/SpriteAtlas.h"
#include "game/data/World.h"
#include "game/data/StaticCamera.h"

#include "./TextureLock.h"


namespace game::render {


constexpr float DEFAULT_FOG_MULT = 1.f;
constexpr float DEFAULT_FOG_LIMIT = 40.f;


inline Uint32
applyColourMod(Uint32 colour, Uint32 modifier) {
  Uint32 const r = ( (colour                >> 24) * modifier) / 255u;
  Uint32 const g = (((colour & 0x00ff0000u) >> 16) * modifier) / 255u;
  Uint32 const b = (((colour & 0x0000ff00u) >>  8) * modifier) / 255u;
  return (r << 24) | (g << 16) | (b << 8) | (colour & 0x000000ffu);
}


template <typename GetBackgroundPixels>
void
raycastBackground(
  GetBackgroundPixels const& getBackgroundPixels,
  game::data::StaticCamera const& camera,
  game::render::TextureLock const& tex,
  int texWidth,
  int texHeight,
  float fogMult = DEFAULT_FOG_MULT,
  float fogLimit = DEFAULT_FOG_LIMIT,
  int texOffsetX = 0,
  int texOffsetY = 0
) {
  // Ray direction for left-most (x == 0) and right-most (x == w) rays.
  auto const rayLHS = camera.direction - camera.plane;
  auto const rayRHS = camera.direction + camera.plane;

  int const xEnd = texOffsetX + texWidth;
  int const halfHeight = texHeight / 2;
  int const yEnd = texOffsetY + halfHeight;
  for (int y = texOffsetY; y < yEnd; ++y) {
    // Imagine the camera is positioned perpendicularly to the screen, pointing
    // at the midpoint, and 1 unit away (i.e. it is normal to the screen).
    // It casts a ray towards the floor (or ceiling), passing through the
    // screen pixel (x, y).
    // The camera z-coordinate.
    float const cameraZ = 0.5f * texHeight;
    // The z-coordinate where the camera ray hits the screen.
    float const screenZ = halfHeight - y; // y - halfHeight
    // The horizontal distance from the camera to the point where the ray hits
    // the floor.
    float const hdist = cameraZ / screenZ;
    // Colour modifier (distance fog).
    Uint32 const fog = Uint32(
      game::util::lerp(255.f, fogLimit, std::min(fogMult * hdist, 1.f)));

    // Step vector, in world coordinates.
    auto const floorStep = (hdist / texWidth) * (rayRHS - rayLHS);
    // World coordinates of current column.
    auto floorPos = camera.position + hdist * rayLHS;

    for (int x = texOffsetX; x < xEnd; ++x, floorPos += floorStep) {
      auto const pixels = getBackgroundPixels(floorPos.x, floorPos.y);
      // Floor.
      tex.set(x, texHeight - y - 1, applyColourMod(pixels.first, fog));
      // Ceiling.
      tex.set(x, y, applyColourMod(pixels.second, fog));
    }
  }
}


template <typename FindWall, typename SetDepth>
void
raycastWalls(
  FindWall const& findWall,
  SetDepth const& setDepth,
  game::data::StaticCamera const& camera,
  game::render::TextureLock const& tex,
  int texWidth,
  int texHeight,
  float fogMult = DEFAULT_FOG_MULT,
  float fogLimit = DEFAULT_FOG_LIMIT
) {
  for (int column = 0; column < texWidth; ++column) {
    // The x-coordinate of column, in camera space: [0, texWidth) -> [-1, 1)
    float const cameraX = 2 * float(column) / texWidth - 1;
    float const rayDirX = camera.direction.x + camera.plane.x * cameraX;
    float const rayDirY = camera.direction.y + camera.plane.y * cameraX;
    vm::ivec2 currentCell{
      int(camera.position.x),
      int(camera.position.y)};

     // Length of ray from one x or y-side to next x or y-side
    float deltaDistX = std::abs(1 / rayDirX);
    float deltaDistY = std::abs(1 / rayDirY);

    // Length of ray from current position to next x or y-side
    float sideDistX;
    float sideDistY;

    // What direction to step in x or y-direction (either +1 or -1)
    int stepX;
    int stepY;

    // Calculate step and initial sideDist
    if (rayDirX < 0) {
      stepX = -1;
      sideDistX = (camera.position.x - currentCell.x) * deltaDistX;
    }
    else {
      stepX = 1;
      sideDistX = (currentCell.x + 1.0 - camera.position.x) * deltaDistX;
    }
    if (rayDirY < 0) {
      stepY = -1;
      sideDistY = (camera.position.y - currentCell.y) * deltaDistY;
    }
    else {
      stepY = 1;
      sideDistY = (currentCell.y + 1.0 - camera.position.y) * deltaDistY;
    }

    // DDA algorithm.
    Uint32 const* wallPixels = nullptr;
    SDL_Rect frame;
    int wallSurfaceWidth;
    int wallIsEastWest;
    do {
      // Jump to next map square, OR in x-direction, OR in y-direction
      if (sideDistX < sideDistY) {
        sideDistX += deltaDistX;
        currentCell.x += stepX;
        wallIsEastWest = 0;
      }
      else {
        sideDistY += deltaDistY;
        currentCell.y += stepY;
        wallIsEastWest = 1;
      }
      // Check if ray has hit a wall
      wallPixels = findWall(
        currentCell.x, currentCell.y, &wallSurfaceWidth, &frame);
    } while (wallPixels == nullptr);

    // Calculate distance from wall to camera plane (not camera point; we're
    // operating in an orthogonal perspective).
    // Note: trying to avoid branching by using multiples results in small
    // tears.
    float const hdist = wallIsEastWest == 0
      ? (currentCell.x - camera.position.x + (1 - stepX) / 2) / rayDirX
      : (currentCell.y - camera.position.y + (1 - stepY) / 2) / rayDirY;

    // Call setDepth. This allows us to e.g. set values in a Z-buffer.
    setDepth(column, hdist);

    // If hdist > texHeight, the line height will be (floored to) zero, hence
    // we'll render an empty line (and divide by zero).
    if (hdist > texHeight) {
      continue;
    }

    int const wallHeightOnScreen = int(texHeight / hdist);

    // Calculate lowest and highest pixel to fill in current stripe
    int const ymin = (texHeight - wallHeightOnScreen) / 2;
    int const ymaxClamped = std::min(
      texHeight - 1, (texHeight + wallHeightOnScreen) / 2);

    // Get x- or y-coordinate where the ray hits the wall, depending on its
    // orientation.
    float wallX = wallIsEastWest == 0
      ? camera.position.y + hdist * rayDirY
      : camera.position.x + hdist * rayDirX;
    wallX -= std::floor(wallX);

    // Convert the wall x-coordinate to a texture "u"-coordinate (not really
    // but the name will be useful).
    int u = int(wallX * frame.w);
    if (
      (wallIsEastWest == 0 && rayDirX > 0) ||
      (wallIsEastWest == 1 && rayDirY < 0)
    ) {
      u = frame.w - u - 1;
    }

    // How much incrementing the screen y increments the texture v-coord.
    float const yIncr = float(frame.h) / wallHeightOnScreen;

    // Colour modifier (distance fog).
    Uint32 const fog = Uint32(
      game::util::lerp(255.f, fogLimit, std::min(fogMult * hdist, 1.f)));

    auto* viewportPixels = tex.pixels32(column, std::max(0, ymin));

    for (
      int j = std::max(ymin, 0);
      j <= ymaxClamped;
      ++j, viewportPixels += tex.width()
    ) {
      int const texCoordY = std::min(int((j - ymin) * yIncr), frame.h - 1);
      Uint32 colour =
      // Get the colour at those coordinates in the texture. Recall that
      // our textures are on their side, to make this copying a bit more
      // cache-friendly.
        wallPixels[wallSurfaceWidth * u + texCoordY];

      // And bit-shift if the wall is East-West. We're basically dividing
      // each colour component by two for East-West walls, in order to give the
      // effect of some simple lighting.
      if (wallIsEastWest) {
        // This magic number erases bits shifted down from one component into
        // another.
        colour = (colour >> 1u) & 0x7f7f7fu;
      }

      *viewportPixels = applyColourMod(colour, fog);
    }
  }
}


struct SpriteQuery {
  Uint32 const* pixels;
  SDL_Rect frame;
  int divideX;
  int divideY;
  int surfaceWidth;
  int yOffset;
};


template <
  typename GetPosition,
  typename QuerySprite,
  typename Transparent,
  typename SetVisible,
  typename SpriteIter
>
void
raycastSprites(
  GetPosition const& getPosition,
  QuerySprite const& querySprite,
  Transparent const& transparent,
  SetVisible const& setVisible,
  SpriteIter begin,
  SpriteIter end,
  float const* zBuffer,
  game::data::StaticCamera const& camera,
  TextureLock const& tex,
  int texWidth,
  int texHeight
) {
  for (; begin != end; ++begin) {
    auto const relpos = getPosition(*begin) - camera.position;
    // Multiply by the inverse camera matrix.
    float const inverseDeterminant = 1.f / (
      camera.plane.x * camera.direction.y -
      camera.plane.y * camera.direction.x
    );

    // The transform: the x-coordinate and the depth within the screen.
    float const transformX = inverseDeterminant * (
      camera.direction.y * relpos.x - camera.direction.x * relpos.y
    );
    float const transformZ = inverseDeterminant * (
      (-camera.plane.y * relpos.x + camera.plane.x * relpos.y)
    );
    if (transformZ <= 0) {
      continue;
    }

    SpriteQuery sprite{
      nullptr,
      SDL_Rect{0, 0, 0, 0},
      1, 1,
      0,
      0
    };
    querySprite(*begin, &sprite);

    int const screenX = int((texWidth / 2) * (1 + transformX / transformZ));

    int const height =
      std::abs(int(texHeight / transformZ)) / sprite.divideY;
    int const ymin = (texHeight - height) / 2 + sprite.yOffset;
    int const ymax = ymin + height;
    int const yminClamped = std::max(0, ymin);
    int const ymaxClamped = std::min(ymax, texHeight - 1);

    int const width = std::abs(int(texWidth / transformZ)) / sprite.divideX;
    int const xmin = screenX - width / 2;
    int const xmax = xmin + width;
    int const xminClamped = std::max(0, xmin);
    int const xmaxClamped = std::min(xmax, texWidth - 1);

    int visibleColumns = 0;

    for (int xOut = xminClamped; xOut < xmaxClamped; ++xOut) {
      if (transformZ >= zBuffer[xOut]) {
        continue;
      }
      ++visibleColumns;

      constexpr int ONE = 256;
      constexpr int HALF = 128;
      int const tx = sprite.frame.x + (
        ((ONE * (xOut - (screenX - width / 2)) * sprite.frame.w) / width) / ONE
      );
      for (int yOut = yminClamped; yOut < ymaxClamped; ++yOut) {
        int const diff = ONE * yOut + HALF * (height - texHeight + 1);
        assert((diff >= 0) && "Invalid sprite casting diff");
        int const ty =
          sprite.frame.y + ((diff * sprite.frame.h - 1) / height) / ONE;
        Uint32 const colour = sprite.pixels[tx + ty * sprite.surfaceWidth];
        if (!transparent(colour)) {
          tex.set(xOut, yOut, colour);
        }
      }
    }

    setVisible(*begin, visibleColumns);
  }
}


} // namespace game::render
