#pragma once


struct SDL_Renderer;
struct SDL_Texture;


extern "C" int SDL_SetRenderTarget(SDL_Renderer*, SDL_Texture*);


namespace game::render {


class RenderCapture {
public:
  RenderCapture(SDL_Renderer* renderer, SDL_Texture* texture);
  RenderCapture(RenderCapture const&) = delete;
  RenderCapture(RenderCapture&&) = delete;
  RenderCapture& operator=(RenderCapture const&) = delete;
  RenderCapture& operator=(RenderCapture&&) = delete;

  inline ~RenderCapture() noexcept {
    SDL_SetRenderTarget(m_renderer, nullptr);
  }

private:
  SDL_Renderer* m_renderer;
  SDL_Texture* m_texture;
};


} // namespace game::render
