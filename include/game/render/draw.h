#pragma once
#include <SDL2/SDL_surface.h>


namespace game::render {


void
drawVerticalLine(
  SDL_Surface* surface,
  int column,
  int ymin,
  int ymax,
  Uint32 colour
);


} // namespace game::render
