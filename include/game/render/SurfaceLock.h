#pragma once
#include <utility>

#include <SDL2/SDL_surface.h>


namespace game::render {


class SurfaceLock {
public:
  inline SurfaceLock(SDL_Surface* surface)
    : m_surface(surface)
    , m_mustLock(SDL_MUSTLOCK(surface))
    , m_failed(m_mustLock ? SDL_LockSurface(m_surface) : 0)
  {
  }

  SurfaceLock(SurfaceLock const&) = delete;
  SurfaceLock& operator=(SurfaceLock const&) = delete;

  inline SurfaceLock(SurfaceLock&& other) noexcept
    : m_surface(std::exchange(other.m_surface, nullptr))
    , m_mustLock(std::exchange(other.m_mustLock, false))
    , m_failed(std::exchange(other.m_failed, false))
  {
  }

  SurfaceLock& operator=(SurfaceLock&& other) noexcept {
    swap(other);
    return *this;
  }

  inline void swap(SurfaceLock& other) noexcept {
    std::swap(m_surface, other.m_surface);
    std::swap(m_mustLock, other.m_mustLock);
    std::swap(m_failed, other.m_failed);
  }

  inline ~SurfaceLock() noexcept {
    if (m_mustLock && m_surface) {
      SDL_UnlockSurface(m_surface);
    }
  }

  inline int failed() const noexcept {
    return m_failed;
  }

private:
  SDL_Surface* m_surface;
  bool m_mustLock;
  int m_failed;
};


int surfaceLockNoOp(SDL_Surface* surface);
void surfaceUnlockNoOp(SDL_Surface* surface);


} // namespace game::render
