#pragma once
#include <algorithm>
#include <limits>

#include <SDL2/SDL_rect.h>

#include "./mipmap.h"


namespace game::render {


class MipMappedTexture {
public:
  static constexpr unsigned s_noMapLimit =
    std::numeric_limits<unsigned>::max();

  MipMappedTexture() noexcept;
  MipMappedTexture(
    SDL_Renderer*,
    SDL_Surface* src,
    unsigned maxMapped = s_noMapLimit);
  MipMappedTexture(
    SDL_Renderer* renderer,
    SDL_Surface* src,
    SDL_Rect const& srcRect,
    unsigned maxMapped = s_noMapLimit);

  MipMappedTexture(MipMappedTexture const&) = delete;
  MipMappedTexture(MipMappedTexture&&) noexcept = default;
  MipMappedTexture& operator=(MipMappedTexture const&) = delete;
  MipMappedTexture& operator=(MipMappedTexture&&) noexcept = default;
  ~MipMappedTexture() noexcept = default;

  void reset(
    SDL_Renderer*,
    SDL_Surface* src,
    unsigned maxMapped = s_noMapLimit);

  void reset(
    SDL_Renderer*,
    SDL_Surface* src,
    SDL_Rect const& srcRect,
    unsigned maxMapped = s_noMapLimit);

  inline SDL_Texture* get() const noexcept { return m_texture.get(); }

  inline void use(unsigned index) noexcept {
    m_index = std::min(index, m_count);
  }
  inline void down() noexcept { use(m_index + 1u); }
  inline void up() noexcept { m_index = std::max(1u, m_index) - 1u; }

  inline void coords(SDL_Rect* coords) const {
    getMipMapCoords(coords, m_origW, m_origH, m_index);
  }

  inline int originalWidth() const noexcept { return m_origW; }
  inline int originalHeight() const noexcept { return m_origH; }

  inline unsigned index() const noexcept { return m_index; }
  inline unsigned count() const noexcept { return m_count; }

  inline void clear() noexcept {
    m_texture.reset();
    m_origW = 0;
    m_origH = 0;
  }

private:
  std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_texture;
  int m_origW;
  int m_origH;
  unsigned m_index;
  unsigned m_count;
};


} // namespace game::render
