#pragma once
#include <memory>
#include <string>

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>

#include "./RayCaster.h"


namespace game::util { class Settings; }
namespace game::data {
  struct Root;
}


namespace game::render {


/**
 * A renderer for levels (and possibly menus; we'll see).
 *
 * Divides the window into two basic regions, which will always be separate:
 * -  the scene (where the action takes place);
 * -  the HUD.
 *
 * Scene includes:
 * -  the scene texture (streaming; the ray caster target);
 * -  the gun texture (streams the gun animations).
 *
 * HUD includes:
 * -  the health display;
 * -  the ammo display;
 * -  probably other things; maybe a face.
 */
class Renderer {
public:
  Renderer(
    game::util::Settings const* settings,
    game::data::Root const* gamedata,
    std::string const& windowName,
    std::string const& spritesheetPath
  );

  Renderer(Renderer const&) = delete;
  Renderer(Renderer&&) noexcept;
  Renderer& operator=(Renderer const&) = delete;
  Renderer& operator=(Renderer&&) noexcept;
  ~Renderer() noexcept;

  RayCaster& rayCaster() noexcept { return m_rayCaster; }
  RayCaster const& rayCaster() const noexcept { return m_rayCaster; }

  inline void
  present() const noexcept {
    SDL_RenderClear(m_renderer.get());
    SDL_Rect const dest{
      0, 0, m_settings->logicalWidth(), m_settings->logicalHeight()};
    SDL_RenderCopy(m_renderer.get(), m_sceneTex.get(), nullptr, &dest);
    SDL_RenderPresent(m_renderer.get());
  }

  inline SDL_Window* window() noexcept { return m_window.get(); }
  inline SDL_Window const* window() const noexcept { return m_window.get(); }

  inline SDL_Renderer* renderer() noexcept { return m_renderer.get(); }
  inline SDL_Renderer const* renderer() const noexcept {
    return m_renderer.get();
  }

  inline SDL_Texture* sceneTexture() noexcept { return m_sceneTex.get(); }
  inline SDL_Texture const* sceneTexture() const noexcept {
    return m_sceneTex.get();
  }

  inline SDL_Surface* spritesheet() noexcept { return m_spritesheet.get(); }
  inline SDL_Surface const* spritesheet() const noexcept {
    return m_spritesheet.get();
  }

  inline SDL_Texture* spritesheetTexture() noexcept {
    return m_spritesheetTex.get();
  }
  inline SDL_Texture const* spritesheetTexture() const noexcept {
    return m_spritesheetTex.get();
  }

  inline game::data::SpriteAtlas const* spriteAtlas() const noexcept {
    return m_spriteAtlas;
  }

  inline int windowWidth() const noexcept {
    return m_settings->windowRect().w;
  }
  inline int windowHeight() const noexcept {
    return m_settings->windowRect().h;
  }

private:
  /// The global game settings.
  game::util::Settings const* m_settings;
  /// The SDL window we're rendering to.
  std::unique_ptr<SDL_Window, SDLWindowDeleter> m_window;
  /// The SDL renderer we're rendering with.
  std::unique_ptr<SDL_Renderer, SDLRendererDeleter> m_renderer;
  /// The sprite sheet image, as a surface.
  std::unique_ptr<SDL_Surface, SDLSurfaceDeleter> m_spritesheet;
  /// The sprite sheet image, as a texture.
  std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_spritesheetTex;
  /// The sprite atlas.
  game::data::SpriteAtlas const* m_spriteAtlas;
  /// The texture we're using to render the scene.
  std::unique_ptr<SDL_Texture, SDLTextureDeleter> m_sceneTex;

  RayCaster m_rayCaster;
};


} // namespace game::render
