#pragma once
namespace game::render {


/**
 * Get the margin required to centre a rect within another.
 *
 * @param outer the side length of the outer rectangle.
 * @param inner the side length of the inner rectangle.
 */
constexpr int margin(int outer, int inner) {
  return (outer - inner) / 2;
}


} // namespace game::render
