#pragma once
#include <cstddef> // std::byte
#include <cassert>

#include <SDL2/SDL_render.h>


struct SDL_Surface;


namespace game::render {


/// Copy a portion of a surface to a texture's pixels.
void
copy(
  SDL_Rect const& srcRect,
  SDL_Surface const* src,
  std::byte* texturePixels,
  int destPitch
) noexcept;


/// Copy a portion of a surface to a region of a texture's pixels.
inline void
copy(
  SDL_Rect const& srcRect,
  SDL_Surface const* src,
  int xDest,
  int yDest,
  std::byte* texturePixels,
  int destPitch
)
  noexcept
{
  copy(
    srcRect,
    src,
    texturePixels + yDest * destPitch + xDest * sizeof(Uint32),
    destPitch);
}


/// Copy a portion of a surface to a buffer.
void
copy(
  SDL_Rect const& srcRect,
  SDL_Surface const* src,
  int xDest,
  int yDest,
  SDL_Texture* texture
) noexcept;


} // namespace game::render
