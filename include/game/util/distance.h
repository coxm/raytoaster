#include <vm/vec2.hpp>


namespace game::util {


inline float
l2DistanceSquared(vm::vec2 a, vm::vec2 b) {
  auto const c = a - b;
  auto const d = c * c;
  return d.x + d.y;
}



} // namespace game::util
