#pragma once
#include "./has_include.h"


#if defined(__cpp_lib_filesystem) || game_HAS_INCLUDE(<filesystem>)

#   define game_HAS_FILESYSTEM 1
#   include <filesystem>
    namespace game::util {
      namespace filesystem = ::std::filesystem;
    }


#elif defined(__cpp_lib_experimental_filesystem) || game_HAS_INCLUDE(<experimental/filesystem>)

#   define game_HAS_EXPERIMENTAL_FILESYSTEM
#   include <experimental/filesystem>

    namespace game::util {
      namespace filesystem = ::std::experimental::filesystem;
    }


#elif defined(game_ALLOW_NO_FILESYSTEM)
#   define game_HAS_NO_FILESYSTEM 1


#else
#   error No filesystem support
#endif
