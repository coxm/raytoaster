#pragma once
#include "./bithacks.h"


namespace game::util {


class MoveState {
public:
  using mask_type = int;

  constexpr static mask_type s_moveFwd     = 0x0001;
  constexpr static mask_type s_moveBwd     = 0x0002;
  constexpr static mask_type s_moveFwdBwd  = s_moveFwd | s_moveBwd;

  constexpr static mask_type s_strafeLeft  = 0x0004;
  constexpr static mask_type s_strafeRight = 0x0008;
  constexpr static mask_type s_moveLR      = s_strafeLeft | s_strafeRight;

  constexpr static mask_type s_turnLeft    = 0x0010;
  constexpr static mask_type s_turnRight   = 0x0020;
  constexpr static mask_type s_turnLR      = s_turnLeft | s_turnRight;

  constexpr static mask_type s_attack      = 0x0100;
  constexpr static mask_type s_altAttack   = 0x0200;

  constexpr static mask_type s_walking = s_moveFwdBwd | s_moveLR;

  inline MoveState(mask_type mask = 0u) noexcept
    : m_flags{mask}
  {
  }

  MoveState(MoveState const&) noexcept = default;

  inline MoveState(MoveState&& other) noexcept
    : m_flags{other.m_flags}
  {
    other.m_flags = 0u;
  }

  inline MoveState& operator=(MoveState const&) noexcept = default;

  inline MoveState& operator=(MoveState&& other) noexcept {
    m_flags = other.m_flags;
    other.m_flags = 0u;
    return *this;
  }

  inline ~MoveState() noexcept = default;

  inline mask_type flags() const noexcept { return m_flags; }
  inline void add(mask_type mask) noexcept { m_flags |= mask; }
  inline void remove(mask_type mask) noexcept { m_flags &= ~mask; }
  inline bool has(mask_type mask) const noexcept { return m_flags & mask; }
  inline operator bool() const noexcept { return m_flags != 0; }

  /**
   * Toggle a mask.
   *
   * @param on 0 or 1, i.e. `int(bool(on))`.
   */
  void toggle(mask_type mask, int on) noexcept {
    m_flags = bitsToggledIf(m_flags, mask, on);
  }

  inline void reset(mask_type mask = 0) noexcept { m_flags = mask; }

  /** Return `1` if `mask & cond`; otherwise `0`. */
  inline int
  condition(mask_type cond) const noexcept {
    return isNonzeroAsInt(m_flags & cond);
  }

  /** Return `ifTrue` if `mask & cond`; otherwise `0`. */
  inline int
  condition(mask_type cond, mask_type ifTrue) const noexcept {
    return isNonzeroAsInt(m_flags & cond) * ifTrue;
  }

  inline int
  multiplier(mask_type positiveFlag, mask_type negativeFlag) const noexcept {
    int const pos = isNonzeroAsInt(positiveFlag & m_flags);
    int const neg = isNonzeroAsInt(negativeFlag & m_flags);
    int const nonzero = pos ^ neg;
    return (nonzero ^ -neg) + neg;
  }

  bool inline operator==(MoveState const other) const noexcept;
  bool inline operator!=(MoveState const other) const noexcept;

private:
  mask_type m_flags;
};


} // namespace game::util
