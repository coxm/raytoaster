#pragma once
namespace game::util {


/**
 * Check if an integer is nonzero.
 *
 * @returns 0 (if zero) or 1 (if nonzero).
 */
inline int isNonzeroAsInt(int n) noexcept {
  return ((n | (~n + 1)) >> 31) & 1;
}


/**
 * Set or clear bits according to a mask.
 *
 * Equivalent to:
 * @code
 * if (doModify) word |= modMask; else word &= ~modMask;
 * @endcode
 */
template <typename Integral>
Integral bitsToggledIf(Integral word, Integral modMask, bool doModify) {
  return (word & ~modMask) | (-doModify & modMask);
}


} // namespace game::util
