#pragma once
#include <algorithm>

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_timer.h>

#include "game/logging/log.h"


namespace game::util {


template <
  typename EventHandler,
  typename Continuer,
  typename Ticker,
  typename Renderer
>
void
mainloop(
  EventHandler const& handleEvent,
  Continuer const& doContinue,
  Ticker const& tick,
  Renderer const& render,
  unsigned renderMS = 33u,
  unsigned logicMS = 33u
) {
  unsigned const waitTime = std::min(renderMS, logicMS);
  unsigned nextRenderTime = SDL_GetTicks();
  unsigned nextLogicTime = nextRenderTime;
  SDL_Event event;
  do {
    unsigned const now = SDL_GetTicks();

    while (SDL_PollEvent(&event) != 0) {
      handleEvent(event);
    }

    for (; nextLogicTime < now; nextLogicTime += logicMS) {
      tick(nextLogicTime);
    }

    if (nextRenderTime < now) {
      render(nextRenderTime);
      nextRenderTime += renderMS;
    }

    SDL_Delay(waitTime);
  }
  while (doContinue());
}


} // namespace game::util
