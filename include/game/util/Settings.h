#pragma once
#include <cstdint>
#include <string>

#include <SDL2/SDL_rect.h>

#include "./Path.h"


namespace game::util {


struct HudSettings {
  /// The destination rect for the ammo text (relative to the HUD rect).
  SDL_Rect ammoTextDest;
  /// The objective text's x-coordinate (relative to scene rect).
  int objectiveX;
  /// The objective text's y-coordinate (relative to scene rect).
  int objectiveY;
};


struct FontConfig {
  std::string filepath = "";
  int pointsize = 12;
  std::uint32_t colour = 0x000000ff;
};


class Settings {
public:
  constexpr static int s_maxPixelScale = 31;

  Settings(int argc, char const* const* argv);
  Settings(Settings const&) = delete;
  Settings(Settings&&) = delete;
  Settings& operator=(Settings const&) = delete;
  Settings& operator=(Settings&&) = delete;
  ~Settings() noexcept = default;

  inline float mouseSensitivity() const noexcept { return m_mouseSensitivity; }
  inline void mouseSensitivity(float v) noexcept { m_mouseSensitivity = v; }

  inline SDL_Rect const& windowRect() const noexcept { return m_windowRect; }
  inline std::uint32_t windowFlags() const noexcept { return m_windowFlags; }

  inline int sceneWidth() const noexcept { return m_windowRect.w; }
  inline int sceneHeight() const noexcept { return m_sceneHeight; }
  inline int hudHeight() const noexcept {
    return m_windowRect.h - m_sceneHeight;
  }

  inline int pixelScale() const noexcept {
    return m_pixelScale;
  }
  inline int fontScale() const noexcept {
    return m_fontScale;
  }

  inline int logicalWidth() const noexcept {
    return m_windowRect.w / m_pixelScale;
  }
  inline int logicalHeight() const noexcept {
    return m_windowRect.h / m_pixelScale;
  }
  inline int logicalSceneHeight() const noexcept {
    return m_sceneHeight / m_pixelScale;
  }

  inline FontConfig const& menuFont() const noexcept { return m_menuFont; }
  inline FontConfig const& hudFont() const noexcept { return m_hudFont; }
  inline FontConfig const& objectiveFont() const noexcept {
    return m_objectiveFont;
  }

  inline void setPixelScale(int scale) noexcept { m_pixelScale = scale; }
  inline void setFogMult(float mult) noexcept { m_fogMult = mult; }
  inline void setFogLimit(float limit) noexcept { m_fogLimit = limit; }
  inline void setFov(float fov) noexcept { m_fov = fov; }
  inline void setCameraPlane(float plane) noexcept { m_cameraPlane = plane; }

  inline HudSettings const& hud() const noexcept { return m_hud; }

  Path const& basedir() const noexcept { return m_baseDir; }

  inline float fogMult() const noexcept { return m_fogMult; }
  inline float fogLimit() const noexcept { return m_fogLimit; }

  inline float fov() const noexcept { return m_fov; }
  inline float cameraPlane() const noexcept { return m_cameraPlane; }

  inline unsigned logLevel() const noexcept { return m_logLevel; }
  inline unsigned renderTickMS() const noexcept { return m_renderTickMS; }
  inline unsigned logicTickMS() const noexcept { return m_logicTickMS; }

  inline float playerSpeed() const noexcept { return m_playerSpeed; }
  inline float playerTurnSpeed() const noexcept { return m_playerTurnSpeed; }

  inline int maxMyopia() const noexcept { return 32; }

  inline float monsterMyopiaMult() const noexcept {
    return m_monsterMyopiaMult;
  }
  inline float monsterMyopiaDiff() const noexcept {
    return m_monsterMyopiaDiff;
  }

  inline float breathingVolumeMult() const noexcept {
    return m_breathingVolumeMult;
  }

private:
  Path m_baseDir;
  FontConfig m_menuFont;
  FontConfig m_hudFont;
  FontConfig m_objectiveFont;
  SDL_Rect m_windowRect;
  std::uint32_t m_windowFlags;
  /// The pixel scaling constant: must be at least 1.
  int m_pixelScale;
  /// Pixel multiplier applied to fonts when rendering.
  int m_fontScale;
  /// The height in pixels of the ray-casted scene, before scaling.
  int m_sceneHeight;
  /// Mouse sensitivity.
  float m_mouseSensitivity;
  HudSettings m_hud;
  /// Fog multiplier.
  float m_fogMult;
  /// Fog limit (darkest value; clamped between 0.f and 255.f).
  float m_fogLimit;
  /// Camera direction vector, x-coordinate.
  float m_fov;
  /// Camera plane, y-coordinate.
  float m_cameraPlane;
  /// Log level (also affected by config option game_DEBUG).
  unsigned m_logLevel;
  /// Render speed: attempt to render once every `m_renderTickMS` milliseconds.
  unsigned m_renderTickMS;
  /// Logic speed: attempt to tick once every `m_logicTickMS` milliseconds.
  unsigned m_logicTickMS;
  /// Player walking speed.
  float m_playerSpeed;
  /// Player turn speed.
  float m_playerTurnSpeed;
  /// Multiplier for the monster visibility/fear equation.
  float m_monsterMyopiaMult;
  /// Offset for the monster visibility/fear equation.
  float m_monsterMyopiaDiff;
  /// Multiplier for the breathing sound effect volume.
  float m_breathingVolumeMult;
};


} // namespace game::util
