#pragma once
#include <SDL2/SDL_rect.h>


template <typename OS>
OS& operator<<(OS& os, SDL_Rect const& rect) {
  return os
    << "(x: " << rect.x
    << ", y: " << rect.y
    << ", w: " << rect.w
    << ", h: " << rect.h
    << ')';
}
