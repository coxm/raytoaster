#pragma once
#include <string>


namespace game::util {


std::string
loadTextFile(std::string const& filepath);


} // namespace game::util
