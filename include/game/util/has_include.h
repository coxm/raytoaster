// Provide a shim for __has_include (which always returns false).
#ifdef __has_include
# define game_HAS_INCLUDE __has_include
#else
# define game_HAS_INCLUDE(include) 0
#endif
