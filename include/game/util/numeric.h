#pragma once
#include <type_traits>
#include <cstdint>


namespace game::util {


template <typename T>
inline constexpr int
signum(T x, std::false_type is_signed) {
  return T(0) < x;
}

template <typename T>
inline constexpr int
signum(T x, std::true_type is_signed) {
  return (T(0) < x) - (x < T(0));
}

template <typename T>
inline constexpr int
signum(T x) {
  return signum(x, std::is_signed<T>());
}


/**
 * Get the log (base 2) of an unsigned integer.
 *
 * @todo(C++20) replace with std::bit_width when we support only C++>=20.
 */
std::uint32_t log2(std::uint32_t i);


/**
 * Shim for std::clamp.
 *
 * @todo(C++20) replace with std::clamp when we have a cstdlib that supports
 * it! Seems the cstdlib that comes with g++-8 (Debian Buster) does not.
 */
template <typename T>
constexpr T clamp(T value, T min, T max) noexcept {
  return (value < min) ? min : ((max < value) ? max : value);
}


/**
 * Shim for std::lerp.
 *
 * @todo(C++20)
 */
constexpr float lerp(float a, float b, float t) noexcept {
  if ((a <= 0 && b >= 0) || (a >= 0 && b <= 0)) {
    return t * b + (1 - t) * a;
  }
  if (t == 1) {
    return b;
  }
  float const f = a + t * (b - a);
  return (t > 1) == (b > a)
    ? (b < f ? f : b)
    : (b > f ? f : b);
}


} // namespace game::util
