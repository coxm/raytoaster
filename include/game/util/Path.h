#pragma once
#include <string>
#include <utility>

#include "./filesystem.h"


#if defined(game_HAS_FILESYSTEM) || defined(game_HAS_EXPERIMENTAL_FILESYSTEM)

namespace game::util {
  using Path = game::util::filesystem::path;
}

#elif defined(game_HAS_NO_FILESYSTEM)
  // No standard filesystem support; provide a shim.
  namespace game::util {


  class Path {
  public:
    using value_type = char;

    constexpr static value_type separator = '/';

    template <typename... Args>
    inline Path(Args&& ...args): m_filepath{std::forward<Args>(args)...} {}

    Path operator/(Path const& rhs) const;

    std::string string() const { return m_filepath; }

    inline char const* c_str() const { return m_filepath.c_str(); }

    Path remove_filename() const;

  private:
    std::string m_filepath;
  };


  } // namespace game::util

#else
# error Unsure whether game has filesystem support or not.
#endif
