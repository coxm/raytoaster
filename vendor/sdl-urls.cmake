# Windows builds (32- and 64-bit, built by MinGW for w64).
set(SDL2_URL "https://libsdl.org/release/SDL2-devel-2.0.14-mingw.tar.gz" CACHE STRING "URL from which to download the SDL2 development library")
set(SDL2_image_URL "https://www.libsdl.org/projects/SDL_image/release/SDL2_image-devel-2.0.5-mingw.tar.gz" CACHE STRING "URL from which to download the SDL2_image development library")
set(SDL2_mixer_URL "https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-devel-2.0.4-mingw.tar.gz" CACHE STRING "URL from which to download the SDL2_mixer development library")
set(SDL2_ttf_URL "https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-devel-2.0.15-mingw.tar.gz" CACHE STRING "URL from which to download the SDL2_ttf development library")


# MD5 checksums for each of the above archives.
set(SDL2_URL_HASH       1f331c4e29f54cfc470976ac43994617 CACHE STRING "MD5 hash of the development library bundle for SDL2 ")
set(SDL2_image_URL_HASH 2704080296a24b765ce1e9c3c427a970 CACHE STRING "MD5 hash of the development library bundle for SDL2_image ")
set(SDL2_mixer_URL_HASH 067275e50b40aed65dc472b9964c1d38 CACHE STRING "MD5 hash of the development library bundle for SDL2_mixer ")
set(SDL2_ttf_URL_HASH   9d537c47736ba8112ee63cc94871ff06 CACHE STRING "MD5 hash of the development library bundle for SDL2_ttf ")
