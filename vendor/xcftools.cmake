# This project does not use CMake, and is unlikely to be installed on the
# system (it was removed from Debian a while ago due to being unmaintained), so
# we build a recently forked version as an external project.
message(STATUS "Fetching xcftools...")
ExternalProject_Add(
  xcftools
  GIT_REPOSITORY https://github.com/j-jorge/xcftools.git
  GIT_TAG be488bd2e35a0c6794141411cb5d290950ba4345
  PREFIX xcftools
  TMP_DIR external/xcftools/tmp
  STAMP_DIR external/xcftools/stamp
  DOWNLOAD_DIR external/xcftools/tmp
  SOURCE_DIR external/xcftools/src
  BINARY_DIR external/xcftools/src
  INSTALL_DIR external/xcftools/src
  LOG_DIR external/xcftools/log
  CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo "Configuring xcftools"
            COMMAND <SOURCE_DIR>/configure
            COMMAND ${CMAKE_COMMAND} -E echo "Configured xcftools"
  BUILD_COMMAND ${CMAKE_COMMAND} -E echo "Building xcf2png"
        COMMAND make xcf2png
        COMMAND ${CMAKE_COMMAND} -E echo "Built xcf2png"
  INSTALL_COMMAND "" # Disable installation.
  BUILD_BYPRODUCTS <SOURCE_DIR>/xcf2png)
set(
  game_XCF2PNG_BINARY
  ${CMAKE_CURRENT_BINARY_DIR}/external/xcftools/src/xcf2png
  CACHE
  STRING
  "Location of xcf2png binary")
message(STATUS "game_XCF2PNG_BINARY: ${game_XCF2PNG_BINARY}")
