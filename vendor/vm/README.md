# Vector Mathematics
Vector Mathematics or "vm" is an incredibly simple vector maths library. It is
intended to be a drop-in replacement for glm only in basic cases. More complex
functionality is avoided, with the main goal being easy compatibility.
