/// vm: simple drop-in replacement for glm.
#pragma once
namespace vm {


template <typename T>
struct Vec2T {
  T x;
  T y;
};


template <typename T>
inline Vec2T<T> operator*(T scalar, Vec2T<T> v) {
  return {scalar * v.x, scalar * v.y};
}


template <typename T>
inline Vec2T<T> operator/(T scalar, Vec2T<T> v) {
  return {scalar / v.x, scalar / v.y};
}


template <typename T>
inline Vec2T<T> operator*(Vec2T<T> v, T scalar) {
  return {scalar * v.x, scalar * v.y};
}


template <typename T>
inline Vec2T<T> operator+(Vec2T<T> a, Vec2T<T> b) {
  return {a.x + b.x, a.y + b.y};
}


template <typename T>
inline Vec2T<T> operator-(Vec2T<T> a, Vec2T<T> b) {
  return {a.x - b.x, a.y - b.y};
}


template <typename T>
inline Vec2T<T> operator*(Vec2T<T> a, Vec2T<T> b) {
  return {a.x * b.x, a.y * b.y};
}


template <typename T>
inline Vec2T<T> operator/(Vec2T<T> a, Vec2T<T> b) {
  return {a.x / b.x, a.y / b.y};
}


template <typename T>
inline Vec2T<T>& operator*=(Vec2T<T>& a, T scalar) {
  a.x += scalar;
  a.y += scalar;
  return a;
}


template <typename T>
inline Vec2T<T>& operator/=(Vec2T<T>& a, T scalar) {
  a.x += scalar;
  a.y += scalar;
  return a;
}


template <typename T>
inline Vec2T<T>& operator+=(Vec2T<T>& a, Vec2T<T> b) {
  a.x += b.x;
  a.y += b.y;
  return a;
}


template <typename T>
inline Vec2T<T>& operator-=(Vec2T<T>& a, Vec2T<T> b) {
  a.x -= b.x;
  a.y -= b.y;
  return a;
}


using vec2 = Vec2T<float>;
using ivec2 = Vec2T<int>;


} // namespace vm
