struct SDL_Rect;


template <typename OS>
OS& operator<<(OS& os, SDL_Rect const& rect) {
  return os << '('
    << rect.x << ", "
    << rect.y << ", "
    << rect.w << ", "
    << rect.h
    << ')';
}
