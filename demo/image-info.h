#define IMAGE_INFO(surface) \
     "Bytes pp: " << int(surface->format->BytesPerPixel) << '\n' \
  << "Bits pp:  " << int(surface->format->BitsPerPixel) << '\n' \
  << "Pitch:    " << surface->pitch << '\n' \
  << "Masks:    " << surface->format->Rmask << ", " \
                  << surface->format->Gmask << ", " \
                  << surface->format->Bmask << ", " \
                  << surface->format->Amask
