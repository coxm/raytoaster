#include <cstdlib>
#include <string>


namespace demo {


inline bool
hasFlag(
  int const argc,
  char const* const* const argv,
  std::string const& flag
) {
  for (int i = 1; i < argc; ++i) {
    if (flag == argv[i]) {
      return true;
    }
  }
  return false;
}


inline bool
monoframe(int const argc, char const* const* const argv) {
  if (std::getenv("CI") != nullptr) {
    return true;
  }
  if (std::getenv("monoframe") != nullptr) {
    return true;
  }
  return hasFlag(argc, argv, "--monoframe");
}


} // namespace demo
