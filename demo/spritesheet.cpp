/**
 * @file demo/spritsheet.cpp
 *
 * Renders items from the game spritesheet.
 *
 * Options (all environment variables):
 * - gamedata: the game data path (defaults to the game's actual data);
 * - spritesheet: the spritesheet path (defaults to the game spritesheet).
 */
#include <cstdlib>
#include <cstring>
#include <limits>
#include <memory>
#include <string>
#include <iostream>
#include <algorithm>

#include <SDL2/SDL.h>

#include <game/data/init.h>
#include <game/data/Root.h>
#include <game/render/copy.h>
#include <game/render/deleters.h>
#include <game/render/loadSurface.h>

#include "./errors.h"
#include "./image-info.h"
#include "./cli.h"
#include "./rect.h"


int main(int const argc, char const* const* const argv) {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
    std::cerr << "SDL_Init failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlInit;
  }

  constexpr int windowWidth = 640;
  constexpr int windowHeight = 480;
  std::unique_ptr<SDL_Window, game::render::SDLWindowDeleter> window(
    SDL_CreateWindow(
      "Font rendering",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      windowWidth, windowHeight,
      SDL_WINDOW_SHOWN));
  if (!window) {
    std::cerr << "Failed to create window: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateWindow;
  }

  std::unique_ptr<SDL_Renderer, game::render::SDLRendererDeleter> renderer(
    SDL_CreateRenderer(
      window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
  if (!renderer) {
    std::cerr << "Failed to create renderer: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateRenderer;
  }

  constexpr auto PIXEL_FORMAT = SDL_PIXELFORMAT_RGBA8888;

  char const* spritesheetPath = std::getenv("spritesheet");
  if (!spritesheetPath) {
    spritesheetPath = DEFAULT_SPRITESHEET;
  }
  std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter> surface(
    game::render::loadFormattedSurface(
      spritesheetPath, SDL_PIXELFORMAT_RGBA8888, true));
  if (!surface) {
    std::cerr << "Failed to load spritesheet surface: " << SDL_GetError()
      << std::endl;
    return demo::errors::imgLoad;
  }
  if (surface->format->format != SDL_PIXELFORMAT_RGBA8888) {
    auto* converted = SDL_ConvertSurfaceFormat(surface.get(), PIXEL_FORMAT, 0);
    surface.reset(converted);
  }
  if (SDL_MUSTLOCK(surface.get()) && SDL_LockSurface(surface.get()) != 0) {
    std::cerr << "Failed to lock surface: " << SDL_GetError() << std::endl;
    return demo::errors::sdlLockSurface;
  }
  std::cout << "Image info:\n" << IMAGE_INFO(surface.get()) << std::endl;

  std::unique_ptr<SDL_Texture, game::render::SDLTextureDeleter> texture(
    SDL_CreateTexture(
      renderer.get(),
      PIXEL_FORMAT,
      SDL_TEXTUREACCESS_STREAMING,
      windowWidth,
      windowHeight));
  if (!texture) {
    std::cerr << "Failed to create streaming texture:" << SDL_GetError()
      << std::endl;
    return demo::errors::sdlCreateTexture;
  }
  {
    int texWidth, texHeight;
    int access;
    Uint32 format;
    SDL_QueryTexture(texture.get(), &format, &access, &texWidth, &texHeight);
    std::cout << "Texture dimensions: " << texWidth << 'x' << texHeight << '\n'
      << "Access: " << access << '\n'
      << "Format: " << format;
    if (format == SDL_PIXELFORMAT_RGBA8888) {
      std::cout << " (RGBA8888)";
    }
    std::cout << std::endl;
  }

  char const* gameDataPath = std::getenv("gamedata");
  if (gameDataPath == nullptr) {
    gameDataPath = DEFAULT_GAMEDATA;
  }
  auto gamedata = game::data::init(gameDataPath);
  auto framesBegin = std::begin(gamedata->spriteAtlas.frames);
  auto framesEnd = std::end(gamedata->spriteAtlas.frames);
  auto const numFrames = int(std::distance(framesBegin, framesEnd));
  int frameIndex = 0;

  SDL_Event event;
  int targetX = 0;
  int targetY = 0;
  bool looping = !demo::monoframe(argc, argv);
  bool didChange = true;
  do {
    SDL_Delay(33);
    while (SDL_PollEvent(&event) != 0) {
      switch (event.type) {
        case SDL_QUIT: looping = false; break;
        case SDL_KEYDOWN:
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
              looping = false;
              break;
            case SDLK_j:
              frameIndex = (numFrames + frameIndex - 1) % numFrames;
              didChange = true;
              break;
            case SDLK_k:
              frameIndex = (numFrames + frameIndex + 1) % numFrames;
              didChange = true;
              break;
            case SDLK_w:
              --targetY;
              didChange = true;
              break;
            case SDLK_s:
              ++targetY;
              didChange = true;
              break;
            case SDLK_a:
              --targetX;
              didChange = true;
              break;
            case SDLK_d:
              ++targetX;
              didChange = true;
              break;
          }
          break;
      }
    }

    if (didChange) {
      // The original spritesheet frame.
      SDL_Rect const& src = *(framesBegin + frameIndex);
      // The destination rect: clip the frame to fit the window.
      targetX = std::clamp(targetX, 0, windowWidth - src.w);
      targetY = std::clamp(targetY, 0, windowHeight - src.h);
      SDL_Rect dest{
        targetX,
        targetY,
        std::clamp(src.w, 0, windowWidth),
        std::clamp(src.h, 0, windowHeight)
      };
      // std::cout << "Copying " << src << " to " << dest << std::endl;
      game::render::copy(src, surface.get(), targetX, targetY, texture.get());
      didChange = false;

      SDL_RenderClear(renderer.get());
      SDL_RenderCopy(renderer.get(), texture.get(), &dest, &dest);
      SDL_RenderPresent(renderer.get());
    }

  } while (looping);

  return 0;
}
