/**
 * @file demo/foreground-render.cpp
 *
 * Renders walls from a scene using a game::render::raycastWalls.
 */
#include <memory>
#include <iostream>
#include <cstdlib>

#include <SDL2/SDL.h>

#include <game/data/StaticCamera.h>
#include <game/render/deleters.h>
#include <game/render/raycast.h>

#include "./errors.h"
#include "./cli.h"


int main(int const argc, char const* const* const argv) {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
    std::cerr << "SDL_Init failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlInit;
  }

  constexpr int windowWidth = 640;
  constexpr int windowHeight = 480;
  std::unique_ptr<SDL_Window, game::render::SDLWindowDeleter> window(
    SDL_CreateWindow(
      "Ray caster",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      windowWidth, windowHeight,
      SDL_WINDOW_SHOWN));
  if (!window) {
    std::cerr << "Failed to create window: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateWindow;
  }

  std::unique_ptr<SDL_Renderer, game::render::SDLRendererDeleter> renderer(
    SDL_CreateRenderer(
      window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
  if (!renderer) {
    std::cerr << "Failed to create renderer: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateRenderer;
  }

  constexpr auto PIXEL_FORMAT = SDL_PIXELFORMAT_RGBA8888;
  std::unique_ptr<SDL_Texture, game::render::SDLTextureDeleter> texture(
    SDL_CreateTexture(
      renderer.get(),
      PIXEL_FORMAT,
      SDL_TEXTUREACCESS_STREAMING,
      windowWidth,
      windowHeight));
  if (!texture) {
    std::cerr << "Failed to create streaming texture:" << SDL_GetError()
      << std::endl;
    return demo::errors::sdlCreateTexture;
  }

  constexpr int surfaceWidth = 32;
  constexpr int surfaceHeight = 32;
  constexpr int surfaceDepth = 32;
  std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter> surface(
    SDL_CreateRGBSurfaceWithFormat(
      0, surfaceWidth, surfaceHeight, surfaceDepth, PIXEL_FORMAT));
  if (!surface) {
    std::cerr << "Failed to create RGB surface: " << SDL_GetError()
      << std::endl;
    return demo::errors::sdlCreateRGBSurfaceWithFormat;
  }
  if (SDL_FillRect(surface.get(), nullptr, 0xff0000ff) != 0) {
    std::cerr << "Failed to fill rect: " << SDL_GetError() << std::endl;
    return demo::errors::sdlFillRect;
  }

  auto const findWall = [surface = surface.get()] (
    int x, int y,
    int* wallSurfaceWidth,
    SDL_Rect* frame
  ) -> Uint32 const* {
    int const ax = std::abs(x);
    int const ay = std::abs(y);
    bool const isWall =
      ax == 10 ||
      ay == 10 ||
      (ax != 0 && ax == ay);

    if (!isWall) {
      return nullptr;
    }

    *wallSurfaceWidth = surface->w;
    frame->x = 0;
    frame->y = 0;
    frame->w = surfaceWidth / 2;
    frame->h = surfaceWidth / 2;
    return static_cast<Uint32 const*>(surface->pixels)
      + frame->y * surface->w + frame->x;
  };

  game::data::StaticCamera const camera{
    vm::vec2{0, 0},
    vm::vec2{-1, 0},
    vm::vec2{0, 0.66f}
  };

  SDL_Event event;
  bool looping = !demo::monoframe(argc, argv);
  do {
    SDL_Delay(33);
    while (SDL_PollEvent(&event) != 0) {
      switch (event.type) {
        case SDL_QUIT: looping = false; break;
        case SDL_KEYDOWN:
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
              looping = false;
              break;
          }
          break;
      }
    }

    { // Render.
      game::render::TextureLock tex(texture.get(), nullptr);
      game::render::raycastWalls(
        findWall,
        [](auto&&...){},
        camera, tex,
        windowWidth, windowHeight);

      SDL_RenderClear(renderer.get());
      SDL_RenderCopy(renderer.get(), texture.get(), nullptr, nullptr);
      SDL_RenderPresent(renderer.get());
    }
  } while (looping);

  return 0;
}
