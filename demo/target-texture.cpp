/**
 * @file target-texture.cpp
 *
 * Render SDL_Renderer output to a target texture.
 */
#include <cstdlib>
#include <cstring>
#include <limits>
#include <memory>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <ios>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <game/render/deleters.h>

#include "./cli.h"
#include "./errors.h"
#include "./deleters.h"


int main(int const argc, char const* const* const argv) {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
    std::cerr << "SDL_Init failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlInit;
  }

  constexpr int windowWidth = 640;
  constexpr int windowHeight = 480;
  std::unique_ptr<SDL_Window, game::render::SDLWindowDeleter> window(
    SDL_CreateWindow(
      "Demo: render to target texture",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      windowWidth, windowHeight,
      SDL_WINDOW_SHOWN));
  if (!window) {
    std::cerr << "Failed to create window: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateWindow;
  }

  std::unique_ptr<SDL_Renderer, game::render::SDLRendererDeleter> renderer(
    SDL_CreateRenderer(
      window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
  if (!renderer) {
    std::cerr << "Failed to create renderer: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateRenderer;
  }
  {
    SDL_RendererInfo info;
    if (0 != SDL_GetRendererInfo(renderer.get(), &info)) {
      std::cerr << "Unable to get renderer info: " << SDL_GetError()
                << std::endl;
      return demo::errors::sdlGetRendererInfo;
    }
    if (0 == (info.flags & SDL_RENDERER_TARGETTEXTURE)) {
      std::cerr << "Renderer does not support rendering to textures"
                << std::endl;
      return demo::errors::other;
    }
  }

  std::unique_ptr<SDL_Texture, game::render::SDLTextureDeleter> texture(
    SDL_CreateTexture(
      renderer.get(),
      SDL_PIXELFORMAT_RGBA8888,
      SDL_TEXTUREACCESS_TARGET,
      windowWidth,
      windowHeight));
  if (!texture) {
    std::cerr << "Failed to create target texture:" << SDL_GetError()
      << std::endl;
    return demo::errors::sdlCreateTexture;
  }

  auto renderColour = [ren = renderer.get()] (Uint32 colour) {
    SDL_SetRenderDrawColor(
      ren,
      ( colour                >> 24u),
      ((colour & 0x00ff0000u) >> 16u),
      ((colour & 0x0000ff00u) >>  8u),
       (colour & 0x000000ffu)
    );
    SDL_RenderClear(ren);
    SDL_RenderPresent(ren);
  };

  Uint32 colour = 0xffffffffu;
  renderColour(colour);

  SDL_Event event;
  bool looping = !demo::monoframe(argc, argv);
  bool captured = false;
  do {
    Uint32 newColour = colour;
    SDL_Delay(33);
    while (SDL_PollEvent(&event) != 0) {
      switch (event.type) {
        case SDL_QUIT: looping = false; break;
        case SDL_KEYDOWN:
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
              looping = false;
              break;
            case SDLK_j:
              newColour = ((newColour | 1) >> 1) & ~1;
              break;
            case SDLK_k:
              if (colour < std::numeric_limits<Uint32>::max()) {
                newColour = (newColour << 1) | 1;
              }
              break;
            case SDLK_c:
              SDL_SetRenderTarget(renderer.get(), texture.get());
              captured = true;
              break;
            case SDLK_s:
              SDL_SetRenderTarget(renderer.get(), nullptr);
              {
                SDL_Rect const rect{
                  windowWidth / 2,
                  windowHeight / 2,
                  windowWidth / 4,
                  windowHeight / 4
                };
                SDL_RenderCopy(renderer.get(), texture.get(), &rect, &rect);
                SDL_RenderPresent(renderer.get());
              }
              captured = !captured;
              break;
          }
          break;
      }
    }

    if (newColour != colour) {
      colour = newColour;
      std::cout << "Colour: " << std::hex << std::setfill('0') << std::setw(8)
                << newColour << std::endl;
      renderColour(newColour);
    }
  } while (looping);

  return 0;
}
