extern "C" {
  void SDL_Quit();
  void IMG_Quit();
  void TTF_Quit();
}


namespace demo {


struct SDLQuitter { inline ~SDLQuitter() noexcept { SDL_Quit(); } };
struct TTFQuitter { inline ~TTFQuitter() noexcept { TTF_Quit(); } };
struct IMGQuitter { inline ~IMGQuitter() noexcept { IMG_Quit(); } };


} // namespace demo
