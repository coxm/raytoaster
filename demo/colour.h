#include <cstdint>
#include <cstdlib>
#include <limits>

#include <SDL2/SDL_pixels.h>

#include "game/render/colour.h"


template <typename OS>
OS& operator<<(OS& os, SDL_Colour colour) {
  return os << "rgba(" << int(colour.r) << ',' << int(colour.g) << ','
            << int(colour.b) << ',' << int(colour.a) << ')';
}


namespace demo {


SDL_Colour
colourOption(char const* envVarName, SDL_Colour defaultColour) {
  char const* value = std::getenv(envVarName);
  if (value != nullptr) {
    auto u = std::strtoul(value, nullptr, 16);
    if (u <= std::numeric_limits<Uint32>::max()) {
      return game::render::toColour(std::uint32_t(u));
    }
  }
  return defaultColour;
}


} // namespace demo
