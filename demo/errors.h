namespace demo::errors {


enum Error {
  sdlInit,
  sdlCreateWindow,
  sdlCreateRenderer,
  sdlGetRendererInfo,
  sdlCreateRGBSurface,
  sdlCreateRGBSurfaceWithFormat,
  sdlLockSurface,
  sdlFillRect,
  sdlCreateTexture,
  sdlCreateTextureFromSurface,
  ttfInit,
  ttfOpenFont,
  ttfRenderText,
  imgInit,
  imgLoad,
  other,
};


}
