#include <string>

#include <SDL2/SDL_timer.h>

#include <game/logging/log.h>


int main() {
  std::cout << "Now: " << game::logging::now() << std::endl;

  std::string const shrug = R"emoji(¯\_(ツ)_/¯)emoji";

  LOG_TRACE("Trace message " << shrug);
  LOG_DEBUG("Debug message " << shrug);
  LOG_INFO("Info message " << shrug);
  LOG_WARN("Warning message " << shrug);
  LOG_ERROR("Error message " << shrug);
  LOG_CRITICAL("Error message " << shrug);

  SDL_Delay(1234);
  std::cout << "Now: " << game::logging::now() << std::endl;
  return 0;
}
