/**
 * @file demo/renderer.cpp
 *
 * Simple demo that initialises SDL2 and creates a window and renderer.
 */
#include <memory>
#include <iostream>

#include <SDL2/SDL.h>

#include <game/render/deleters.h>

#include "./errors.h"
#include "./quitters.h"


int main() {
  if (int error = SDL_Init(SDL_INIT_VIDEO); error != 0) {
    std::cerr << "SDL_Init failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlInit;
  }
  demo::SDLQuitter quitSDL;

  std::unique_ptr<SDL_Window, game::render::SDLWindowDeleter> window(
    SDL_CreateWindow(
      "Demo: renderer",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      640, 480,
      SDL_WINDOW_SHOWN));
  if (!window) {
    std::cerr << "Window creation failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateWindow;
  }

  std::unique_ptr<SDL_Renderer, game::render::SDLRendererDeleter> renderer(
    SDL_CreateRenderer(
      window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
  if (!renderer) {
    std::cerr << "Renderer creation failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateRenderer;
  }

  SDL_SetRenderDrawColor(renderer.get(), 0, 0, 0, 0xff);
  SDL_RenderClear(renderer.get());
  SDL_RenderPresent(renderer.get());

  return 0;
}
