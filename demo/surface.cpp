/**
 * @file demo/surface.cpp
 *
 * Simple demo that creates surfaces with different SDL functions.
 */
#include <memory>
#include <iostream>

#include <SDL2/SDL.h>

#include <game/render/deleters.h>

#include "./errors.h"


int main() {
  if (int error = SDL_Init(SDL_INIT_VIDEO); error != 0) {
    std::cerr << "SDL_Init failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlInit;
  }
  std::atexit(SDL_Quit);

  std::unique_ptr<SDL_Window, game::render::SDLWindowDeleter> window(
    SDL_CreateWindow(
      "Demo: surface",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      640, 480,
      SDL_WINDOW_SHOWN));
  if (!window) {
    std::cerr << "Window creation failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateWindow;
  }

  std::unique_ptr<SDL_Renderer, game::render::SDLRendererDeleter> renderer(
    SDL_CreateRenderer(
      window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
  if (!renderer) {
    std::cerr << "Renderer creation failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateRenderer;
  }

  constexpr int flags = 0; // According to the docs, flags are unused.
  constexpr int width = 320;
  constexpr int height = 240;
  constexpr int depth = 32;
  constexpr int format = SDL_PIXELFORMAT_RGBA8888;
  std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter> surface(
    SDL_CreateRGBSurface(
      flags, width, height, depth,
      0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff));
  if (!surface) {
    std::cerr << "Failed to create RGB surface: " << SDL_GetError()
      << std::endl;
    return demo::errors::sdlCreateRGBSurface;
  }

  surface.reset(
    SDL_CreateRGBSurfaceWithFormat(flags, width, height, depth, format));
  if (!surface) {
    std::cerr << "Failed to create RGB surface with format: "
      << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateRGBSurfaceWithFormat;
  }

  return 0;
}
