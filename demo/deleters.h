#pragma once


struct SDL_Window;
struct SDL_Renderer;
struct SDL_Surface;
struct SDL_Texture;
struct _TTF_Font;


extern "C" {
  void SDL_DestroyWindow(SDL_Window*);
  void SDL_DestroyRenderer(SDL_Renderer*);
  void SDL_FreeSurface(SDL_Surface*);
  void SDL_DestroyTexture(SDL_Texture*);
  void SDL_FreeFormat(SDL_PixelFormat*);
}


#define DELETE_WITH(What, func) \
  struct What ## Deleter { \
    inline void operator()(What* ptr) const noexcept { func(ptr); } \
  }

namespace demo {
  DELETE_WITH(SDL_Window, SDL_DestroyWindow);
  DELETE_WITH(SDL_Renderer, SDL_DestroyRenderer);
  DELETE_WITH(SDL_Surface, SDL_FreeSurface);
  DELETE_WITH(SDL_Texture, SDL_DestroyTexture);
  DELETE_WITH(_TTF_Font, TTF_CloseFont);
}


#undef DELETE_WITH
