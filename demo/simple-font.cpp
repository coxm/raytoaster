/**
 * @file demo/simple-font.cpp
 *
 * Renders input text with the given font.
 *
 * Options (all environment variables):
 * - bg: background colour, in 0xRRGGBBAA hexadecimal format;
 * - fg: background colour, in 0xRRGGBBAA hexadecimal format;
 * - size: font pointsize;
 * - text: text to render.
 * - mode: "solid", "shaded" or "blended".
 */
#include <cstdlib>
#include <limits>
#include <memory>
#include <string>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <game/render/colour.h>
#include <game/render/deleters.h>
#include <game/render/ttf.h>

#include "./errors.h"
#include "./colour.h"
#include "./cli.h"


/**
 * @file demo/simple-font.cpp
 *
 * Render text to a surface, then create a texture and copy to the renderer.
 *
 * Options (all environment variables):
 * - bg: background colour, in 0xRRGGBBAA hexadecimal format;
 * - fg: background colour, in 0xRRGGBBAA hexadecimal format;
 * - font: the font file to load;
 * - size: font pointsize;
 * - text: text to render;
 * - mode: "solid", "shaded" or "blended".
 */
int main(int const argc, char const* const* const argv) {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
    std::cerr << "SDL_Init failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlInit;
  }
  std::atexit(SDL_Quit);

  std::unique_ptr<SDL_Window, game::render::SDLWindowDeleter> window(
    SDL_CreateWindow(
      "Demo: simple font",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      640, 480,
      SDL_WINDOW_SHOWN));
  if (!window) {
    std::cerr << "Failed to create window: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateWindow;
  }
  std::cout << "Initialised window!" << std::endl;

  std::unique_ptr<SDL_Renderer, game::render::SDLRendererDeleter> renderer(
    SDL_CreateRenderer(
      window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
  if (!renderer) {
    std::cerr << "Failed to create renderer: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateRenderer;
  }

  char const* fontpath = std::getenv("font");
  if (fontpath == nullptr) {
    fontpath = DEFAULT_FONT_PATH;
  }
  int pointSize = 14;
  if (char const* psize = std::getenv("size"); psize) {
    std::cout << "Specified size: '" << psize << "'\n";
    pointSize = std::atoi(psize);
    if (pointSize == 0) {
      pointSize = 14;
    }
  }
  std::cout << "Pointsize: " << pointSize << std::endl;

  if (TTF_Init() == -1) {
    std::cerr << "TTF init failed." << std::endl;
    return demo::errors::ttfInit;
  }
  std::atexit(TTF_Quit);

  std::unique_ptr<TTF_Font, game::render::TTFFontDeleter> font(
    TTF_OpenFont(fontpath, pointSize));
  if (!font) {
    std::cerr << "Failed to open font '" << fontpath << "': " << SDL_GetError()
      << std::endl;
    return demo::errors::ttfOpenFont;
  }

  auto const fgColour = demo::colourOption("fg", SDL_Colour{0xff, 0, 0, 0xff});
  std::cout << "Using fgColour: " << fgColour << std::endl;

  char const* text = std::getenv("text");
  if (!text) {
    text = "Hello";
  }

  std::string mode = "solid";
  if (char const* modevar = std::getenv("mode"); modevar != nullptr) {
    mode = modevar;
  }

  std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter> surface;
  if (mode == "blended") {
    surface.reset(TTF_RenderText_Blended(font.get(), text, fgColour));
  }
  else if (mode == "shaded") {
    auto const bgColour = demo::colourOption(
      "bg", SDL_Colour{0x33, 0x33, 0x33, 0xff});
    surface.reset(TTF_RenderText_Shaded(font.get(), text, fgColour, bgColour));
  }
  else {
    surface.reset(TTF_RenderText_Solid(font.get(), text, fgColour));
  }
  if (!surface) {
    std::cerr << "Failed to render text: " << SDL_GetError() << std::endl;
    return demo::errors::ttfRenderText;
  }

  std::unique_ptr<SDL_Texture, game::render::SDLTextureDeleter> texture(
    SDL_CreateTextureFromSurface(renderer.get(), surface.get()));
  if (!texture) {
    std::cerr << "Failed to create texture from surface:" << SDL_GetError()
      << std::endl;
    return demo::errors::sdlCreateTextureFromSurface;
  }
  int textureWidth, textureHeight;
  SDL_QueryTexture(
    texture.get(), nullptr, nullptr, &textureWidth, &textureHeight);
  std::cout << "Texture dimensions: " << textureWidth << 'x' << textureHeight
    << std::endl;

  SDL_RenderClear(renderer.get());
  SDL_RenderCopy(renderer.get(), texture.get(), nullptr, nullptr);
  SDL_RenderPresent(renderer.get());

  SDL_Event event;
  bool looping = !demo::monoframe(argc, argv);
  do {
    SDL_Delay(33);
    while (SDL_PollEvent(&event) != 0) {
      if (event.type == SDL_QUIT || (
            event.type == SDL_KEYDOWN &&
            event.key.keysym.sym == SDLK_ESCAPE
      )) {
        looping = false;
        break;
      }
    }
  } while (looping);

  return 0;
}
