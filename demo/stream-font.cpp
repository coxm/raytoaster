/**
 * @file demo/stream-font.cpp
 *
 * Render input text via a streaming texture.
 *
 * Options (all environment variables):
 * - bg: background colour, in 0xRRGGBBAA hexadecimal format;
 * - fg: background colour, in 0xRRGGBBAA hexadecimal format;
 * - font: the font file to load;
 * - size: font pointsize;
 * - text: text to render;
 * - mode: "solid", "shaded" or "blended".
 */
#include <cstdlib>
#include <limits>
#include <memory>
#include <string>
#include <iostream>
#include <algorithm>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <game/render/deleters.h>
#include <game/render/ttf.h>

#include "./colour.h"
#include "./errors.h"
#include "./cli.h"


int main(int const argc, char const* const* const argv) {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
    std::cerr << "SDL_Init failed: " << SDL_GetError() << std::endl;
    return demo::errors::sdlInit;
  }
  std::atexit(SDL_Quit);

  constexpr int windowWidth = 640;
  constexpr int windowHeight = 480;
  std::unique_ptr<SDL_Window, game::render::SDLWindowDeleter> window(
    SDL_CreateWindow(
      "Font rendering",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      windowWidth, windowHeight,
      SDL_WINDOW_SHOWN));
  if (!window) {
    std::cerr << "Failed to create window: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateWindow;
  }
  std::cout << "Initialised window!" << std::endl;

  std::unique_ptr<SDL_Renderer, game::render::SDLRendererDeleter> renderer(
    SDL_CreateRenderer(
      window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
  if (!renderer) {
    std::cerr << "Failed to create renderer: " << SDL_GetError() << std::endl;
    return demo::errors::sdlCreateRenderer;
  }

  char const* fontpath = std::getenv("font");
  if (fontpath == nullptr) {
    fontpath = DEFAULT_FONT_PATH;
  }
  int pointSize = 24;
  if (char const* psize = std::getenv("size"); psize) {
    std::cout << "Specified size: '" << psize << "'\n";
    pointSize = std::atoi(psize);
    if (pointSize == 0) {
      pointSize = 24;
    }
  }
  std::cout << "Pointsize: " << pointSize << std::endl;

  if (TTF_Init() == -1) {
    std::cerr << "TTF init failed." << std::endl;
    return demo::errors::ttfInit;
  }
  std::atexit(TTF_Quit);

  std::unique_ptr<TTF_Font, game::render::TTFFontDeleter> font(
    TTF_OpenFont(fontpath, pointSize));
  if (!font) {
    std::cerr << "Failed to open font '" << fontpath << "': " << SDL_GetError()
      << std::endl;
    return demo::errors::ttfOpenFont;
  }

  auto const fgColour = demo::colourOption("fg", SDL_Colour{0xff, 0, 0, 0xff});
  std::cout << "Using fgColour: " << fgColour << std::endl;

  char const* text = std::getenv("text");
  if (!text) {
    text = "Hello";
  }

  std::string mode = "blended";
  if (char const* modevar = std::getenv("mode"); modevar != nullptr) {
    mode = modevar;
  }

  std::unique_ptr<SDL_Surface, game::render::SDLSurfaceDeleter> surface;
  if (mode == "blended") {
    surface.reset(TTF_RenderText_Blended(font.get(), text, fgColour));
  }
  else if (mode == "shaded") {
    auto const bgColour = demo::colourOption(
      "bg", SDL_Colour{0x33, 0x33, 0x33, 0xff});
    surface.reset(TTF_RenderText_Shaded(font.get(), text, fgColour, bgColour));
  }
  else {
    surface.reset(TTF_RenderText_Solid(font.get(), text, fgColour));
  }
  if (!surface) {
    std::cerr << "Failed to render text: " << SDL_GetError() << std::endl;
    return demo::errors::ttfRenderText;
  }

  std::unique_ptr<SDL_Texture, game::render::SDLTextureDeleter> texture(
    SDL_CreateTextureFromSurface(renderer.get(), surface.get()));
  if (!texture) {
    std::cerr << "Failed to create texture from surface:" << SDL_GetError()
      << std::endl;
    return demo::errors::sdlCreateTextureFromSurface;
  }
  int textureWidth, textureHeight;
  SDL_QueryTexture(
    texture.get(), nullptr, nullptr, &textureWidth, &textureHeight);
  std::cout << "Texture dimensions: " << textureWidth << 'x' << textureHeight
    << std::endl;

  SDL_Event event;
  bool looping = !demo::monoframe(argc, argv);
  int x = 80;
  int y = 80;
  int up = 0, down = 0, left = 0, right = 0;
  int speed = 4;
  do {
    SDL_Delay(33);
    while (SDL_PollEvent(&event) != 0) {
      switch (event.type) {
        case SDL_QUIT: looping = false; break;
        case SDL_KEYDOWN:
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE: looping = false; break;
            case SDLK_k: up = speed; break;
            case SDLK_l: left = speed; break;
            case SDLK_j: down = speed; break;
            case SDLK_h: right = speed; break;
          }
          break;
        case SDL_KEYUP:
          switch (event.key.keysym.sym) {
            case SDLK_k: up = 0; break;
            case SDLK_l: left = 0; break;
            case SDLK_j: down = 0; break;
            case SDLK_h: right = 0; break;
          }
          break;
      }
    }
    x = std::clamp(x + right - left, 10, windowWidth);
    y = std::clamp(y + down - up, 10, windowHeight);

    SDL_RenderClear(renderer.get());
    SDL_Rect const dest{
      x, y,
      windowWidth - 2 * x, windowHeight - 2 * y
    };
    SDL_RenderCopy(renderer.get(), texture.get(), nullptr, &dest);
    SDL_RenderPresent(renderer.get());

  } while (looping);

  std::cout << "Success." << std::endl;
  return 0;
}
