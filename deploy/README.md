# @PROJECT_NAME_UPPER@
Thanks for downloading!

## Running the game
### Linux
Run `main` in this directory:

    ./main


### Windows
Run `main.exe` in this directory:

    main.exe

## Credits
This game relies on the following wonderful libraries.

-   [FreeType2](https://freetype.org)
-   [Cereal](http://uscilab.github.io/cereal/) (license included
    [here](./CEREAL_LICENSE.txt))
